#pragma once

#include <string>
#include "digimon.h"
#include "attribute.h"
#include "family.h"

using namespace std;

class Kabuterimon : public Digimon, private Attribute, private Family{
    public:
        Kabuterimon();
        ~Kabuterimon();
        string getInfo();
        Digimon* getEvolution();
    private:
        string *attacks;
        int numAttacks;
};