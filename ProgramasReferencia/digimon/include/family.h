#pragma once

#include <string>

using namespace std;

class Family{
    public:
        Family();
        Family(string name);
        string getFamily();
    private:
        string name;
};