#pragma once

#include <string>

using namespace std;

class Attribute{
    public:
        Attribute();
        Attribute(string name);
        ~Attribute();
        string getAttribute();
    private:
        string name;
};