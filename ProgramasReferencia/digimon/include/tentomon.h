#pragma once

#include <string>
#include "digimon.h"
#include "attribute.h"
#include "family.h"

using namespace std;

class Tentomon : public Digimon, private Attribute, private Family{
    public:
        Tentomon();
        ~Tentomon();
        string getInfo();
        Digimon* getEvolution();
    private:
        int numAttacks;
        string *attacks;
};