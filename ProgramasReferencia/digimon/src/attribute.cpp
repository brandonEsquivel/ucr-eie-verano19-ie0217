#include "attribute.h"

Attribute::Attribute() : name(" ")
{
}

Attribute::Attribute(string name) : name(name)
{
}

Attribute::~Attribute(){
    //nothing to do
}

string Attribute::getAttribute(){
    return this->name;
}