#include <cmath>
#include "point3d.h"

using namespace std;

Point3D::Point3D(){
    this->_x0 = 0;
    this->_y0 = 0;
    this->_z0 = 0;
}

Point3D::Point3D(float x, float y, float z){
    this->_x0 = x;
    this->_y0 = y;
    this->_z0 = z;
}

Point3D::~Point3D(){
    //do nothing
}

float Point3D::getX(){
    return this->_x0;
}

float Point3D::getY(){
    return this->_y0;
}

float Point3D::getZ(){
    return this->_z0;
}

float Point3D::getDistance(float x, float y, float z){
    float result = 0;

    result = sqrt(pow(x - this->_x0, 2) + pow(y - this->_y0, 2) + pow(y - this->_y0, 2));

    return result; 
}

float Point3D::getDistance(Point3D point){
    float result = 0;

    result = sqrt(pow(point.getX() - this->_x0, 2) + pow(point.getY() - this->_y0, 2) + pow(point.getZ() - this->_z0, 2));

    return result; 
}
