#include <iostream>
#include "complex.h"

using namespace std;

int main(){
    
    Complex c1(-0.0457, 0.9953);
    Complex c2(-0.1280, 0.9982);

    Complex c3 = c1 + c2;
    Complex c4 = c1 - c2;

    cout << c3 << endl;
    cout << c4 << endl;

    float real = c3;

    cout << real << endl;

    return 0;
}