#include <iostream>

using namespace std;

class Complex{
    private:
        float real;
        float imag;
        
    public:
        Complex();
        Complex(float real, float imag);
        Complex operator + (Complex const &obj);
        Complex operator - (Complex const &obj);
        operator float() const;
        friend ostream& operator << (ostream& os, Complex const &obj);
};