#include <iostream>

#include "calc.h"

using namespace std;

int main(){
    Calc<int> intCalc;
    Calc<double> doubleCalc;

    cout << intCalc.multiply(3, 5) << endl;
    cout << doubleCalc.multiply(3, 5) << endl;
}