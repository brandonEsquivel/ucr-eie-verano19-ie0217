#pragma once

template <class T> 

class Calc{
  public:
    T multiply(T x, T y);
    T add(T x, T y);
};

template <class T> T Calc<T>::multiply(T x, T y){
  return x*y;
}

template <class T> T Calc<T>::add(T x, T y){
  return x+y;
}