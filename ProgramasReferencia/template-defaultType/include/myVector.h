#pragma once

#include <string>

using namespace std;

template <typename T, int S>

class MyVector{
    private:
        int index;
        int size;
        T* data;

    public:
        MyVector();
        ~MyVector();
        T* getData();
        int getSize();
        string getInfo();
        void add(T value);
        void add(T value, int index);
        T get(int index); 
};

#include "myVector.tpp"