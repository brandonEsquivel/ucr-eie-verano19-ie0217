#include <iostream>
#include <string>

#include "myVector.h"

using namespace std;

int main(){
    MyVector<string, 5> vector;

    cout << vector.getSize() << endl;

    vector.add("An");
    vector.add("example");
    vector.add("of");
    vector.add("generic");
    vector.add("templates");

    cout << vector.getInfo();

    MyVector<double, 10> vector2;

    vector2.add(17.0);

    cout << vector2.get(0) << endl;

    vector2.add(15, 0);

    double *data = vector2.getData();

    cout << data[0] << endl;
}