#pragma once

template <typename T, int S> MyVector<T, S>::MyVector(){
    this->index = 0;
    this->size = S;
    this->data = new T[S];
}

template <typename T, int S> MyVector<T, S>::~MyVector(){
    //do nothing
}

template <typename T, int S> T* MyVector<T, S>::getData(){
    return data;
}

template <typename T, int S> int MyVector<T, S>::getSize(){
    return this->size;
}

template <typename T, int S> string MyVector<T, S>::getInfo(){
    string info = "";

    for(int i = 0; i < this->size; i++){
        info += this->data[i] + " ";
    }
    info += "\n";

    return info;
}

template <typename T, int S> void MyVector<T, S>::add(T value){
    this->data[this->index] = value;
    this->index = (this->index+1)%this->size;
}

template <typename T, int S> void MyVector<T, S>::add(T value, int index){
    if(index < this->index){
        this->data[index] = value;
    }
}

template <typename T, int S> T MyVector<T, S>::get(int index){
    return (index<this->size && index<this->index)? this->data[index]:0;
}

