// General includes
#include <iostream>
#include <vector>

// LEMON includes
#include <lemon/list_graph.h>

using namespace lemon;

int main(){
    // Bi-Directional graph
    ListGraph graph;

    // Graph nodes
    ListGraph::Node a = graph.addNode();
    ListGraph::Node b = graph.addNode();
    ListGraph::Node c = graph.addNode();
    ListGraph::Node d = graph.addNode();
    ListGraph::Node e = graph.addNode();
    ListGraph::Node f = graph.addNode();
    ListGraph::Node g = graph.addNode();
    ListGraph::Node h = graph.addNode();

    // Graph edges
    ListGraph::Edge ab = graph.addEdge(a, b);
    ListGraph::Edge ag = graph.addEdge(a, g);
    ListGraph::Edge bc = graph.addEdge(b, c);
    ListGraph::Edge bg = graph.addEdge(b, g);
    ListGraph::Edge cd = graph.addEdge(c, d);
    ListGraph::Edge cf = graph.addEdge(c, f);
    ListGraph::Edge ch = graph.addEdge(c, h);
    ListGraph::Edge de = graph.addEdge(d, e);
    ListGraph::Edge df = graph.addEdge(d, f);
    ListGraph::Edge ef = graph.addEdge(e, f);
    ListGraph::Edge fg = graph.addEdge(f, g);
    ListGraph::Edge fh = graph.addEdge(f, h);
    ListGraph::Edge gh = graph.addEdge(g, h);

    // Edges costs
    ListGraph::EdgeMap<int> cost(graph);
    cost[ab] = 4;
    cost[ag] = 8;
    cost[bc] = 8;
    cost[bg] = 11;
    cost[cd] = 7;
    cost[cf] = 4;
    cost[ch] = 2;
    cost[de] = 9;
    cost[df] = 14;
    cost[ef] = 2;
    cost[fg] = 1;
    cost[fh] = 6;
    cost[gh] = 7;

    // Traversing graph edges
    std::cout << "**Imprimiendo información de aristas**" << std::endl;
    for(ListGraph::EdgeIt it(graph); it != INVALID; ++it){
        std::cout << "Arista " << graph.id(it)  << " entre los nodos " << 
        graph.id(graph.u(it)) << " y " << graph.id(graph.v(it)) << 
        " tiene un costo de " << cost[it] << std::endl;
    }

    // Traversing graph nodes using an iterator
    std::cout << "**Imprimiendo nodos**" << std::endl;
    for(ListGraph::NodeIt it(graph); it != INVALID; ++it){
        std::cout << "Nodo " << graph.id(it) << std::endl;
    }

    // Obtaining neighbours from a given node
    std::cout << "**Imprimiendo cantidad de vecinos del nodo 0**" << std::endl;
    ListGraph::Node temp = graph.nodeFromId(0);
    std::vector<int> neighbours;
    int count = 0;
    for(ListGraph::EdgeIt it(graph); it != INVALID; ++it){
        if( graph.id(graph.u(it)) == graph.id(temp) ){
            neighbours.push_back(graph.id(graph.v(it)));
            count++;
        }
    }
    std::cout << "Nodo " << graph.id(temp) << " tiene " << count << " vecinos" << std::endl;
    std::cout << "Vecinos de 0 son: " << std::endl;
    for(int i = 0; i < neighbours.size(); i++){
        std::cout << "\t-" << neighbours[i] << std::endl;
    }

    // Put your methods calls here
    recorre
    exit(0);
}