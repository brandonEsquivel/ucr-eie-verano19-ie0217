\select@language {spanish}
\contentsline {chapter}{\numberline {1}\IeC {\'I}ndice de clases}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Lista de clases}{1}{section.1.1}
\contentsline {chapter}{\numberline {2}Indice de archivos}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Lista de archivos}{3}{section.2.1}
\contentsline {chapter}{\numberline {3}Documentaci\IeC {\'o}n de las clases}{5}{chapter.3}
\contentsline {section}{\numberline {3.1}Referencia de la Clase Fraccion}{5}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Documentaci\IeC {\'o}n del constructor y destructor}{5}{subsection.3.1.1}
\contentsline {subsubsection}{\numberline {3.1.1.1}Fraccion()\hspace {0.1cm}{\relax \fontsize {8}{9.5}\selectfont \abovedisplayskip 6\p@ plus2\p@ minus4\p@ \abovedisplayshortskip \z@ plus\p@ \belowdisplayshortskip 3\p@ plus\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 3\p@ plus\p@ minus\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \ttfamily [1/2]}}{5}{subsubsection.3.1.1.1}
\contentsline {subsubsection}{\numberline {3.1.1.2}Fraccion()\hspace {0.1cm}{\relax \fontsize {8}{9.5}\selectfont \abovedisplayskip 6\p@ plus2\p@ minus4\p@ \abovedisplayshortskip \z@ plus\p@ \belowdisplayshortskip 3\p@ plus\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 3\p@ plus\p@ minus\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \ttfamily [2/2]}}{6}{subsubsection.3.1.1.2}
\contentsline {subsection}{\numberline {3.1.2}Documentaci\IeC {\'o}n de las funciones miembro}{6}{subsection.3.1.2}
\contentsline {subsubsection}{\numberline {3.1.2.1}operator float()}{6}{subsubsection.3.1.2.1}
\contentsline {subsubsection}{\numberline {3.1.2.2}operator$\ast $()}{6}{subsubsection.3.1.2.2}
\contentsline {subsubsection}{\numberline {3.1.2.3}operator+()}{6}{subsubsection.3.1.2.3}
\contentsline {subsubsection}{\numberline {3.1.2.4}operator-\/()}{6}{subsubsection.3.1.2.4}
\contentsline {subsubsection}{\numberline {3.1.2.5}operator/()}{6}{subsubsection.3.1.2.5}
\contentsline {subsection}{\numberline {3.1.3}Documentaci\IeC {\'o}n de las funciones relacionadas y clases amigas}{6}{subsection.3.1.3}
\contentsline {subsubsection}{\numberline {3.1.3.1}operator$<$$<$}{7}{subsubsection.3.1.3.1}
\contentsline {subsubsection}{\numberline {3.1.3.2}operator$>$$>$}{7}{subsubsection.3.1.3.2}
\contentsline {section}{\numberline {3.2}Referencia de la Clase Polinomio}{7}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Documentaci\IeC {\'o}n del constructor y destructor}{7}{subsection.3.2.1}
\contentsline {subsubsection}{\numberline {3.2.1.1}Polinomio()\hspace {0.1cm}{\relax \fontsize {8}{9.5}\selectfont \abovedisplayskip 6\p@ plus2\p@ minus4\p@ \abovedisplayshortskip \z@ plus\p@ \belowdisplayshortskip 3\p@ plus\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 3\p@ plus\p@ minus\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \ttfamily [1/2]}}{7}{subsubsection.3.2.1.1}
\contentsline {subsubsection}{\numberline {3.2.1.2}Polinomio()\hspace {0.1cm}{\relax \fontsize {8}{9.5}\selectfont \abovedisplayskip 6\p@ plus2\p@ minus4\p@ \abovedisplayshortskip \z@ plus\p@ \belowdisplayshortskip 3\p@ plus\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 3\p@ plus\p@ minus\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \ttfamily [2/2]}}{8}{subsubsection.3.2.1.2}
\contentsline {subsection}{\numberline {3.2.2}Documentaci\IeC {\'o}n de las funciones miembro}{8}{subsection.3.2.2}
\contentsline {subsubsection}{\numberline {3.2.2.1}operator$\ast $()}{8}{subsubsection.3.2.2.1}
\contentsline {subsubsection}{\numberline {3.2.2.2}operator+()}{8}{subsubsection.3.2.2.2}
\contentsline {subsubsection}{\numberline {3.2.2.3}operator-\/()}{8}{subsubsection.3.2.2.3}
\contentsline {subsubsection}{\numberline {3.2.2.4}operator/()}{8}{subsubsection.3.2.2.4}
\contentsline {subsection}{\numberline {3.2.3}Documentaci\IeC {\'o}n de las funciones relacionadas y clases amigas}{8}{subsection.3.2.3}
\contentsline {subsubsection}{\numberline {3.2.3.1}operator$<$$<$}{8}{subsubsection.3.2.3.1}
\contentsline {subsubsection}{\numberline {3.2.3.2}operator$>$$>$}{9}{subsubsection.3.2.3.2}
\contentsline {chapter}{\numberline {4}Documentaci\IeC {\'o}n de archivos}{11}{chapter.4}
\contentsline {section}{\numberline {4.1}Referencia del Archivo fraccion.\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}h}{11}{section.4.1}
\contentsline {section}{\numberline {4.2}Referencia del Archivo polinomio.\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}h}{11}{section.4.2}
\contentsline {chapter}{\IeC {\'I}ndice}{13}{section*.10}
