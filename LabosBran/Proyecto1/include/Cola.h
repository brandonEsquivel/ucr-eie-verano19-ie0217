#pragma once

// INCLUDES

#include "Nodo.h"

//DEFINES

using namespace std;

/** @brief Implementacion de la clase Cola simple
 * @author Brandon Esquivel
 * @date February 2020 */
class Cola{

    private:
        string nombreCola;/**Nombre de la cola*/
        nodo* final;/** enlace puntero al elemento final de la cola*/
        int size;/**Tamano de la cola*/
    public:

    /** Constructor default
     * @brief Default Constructor */
    Cola();

    /** Constructor Custom
     * @brief Custom Constructor */
    Cola(int i);

     /** Constructor Custom
     * @brief Custom Constructor */
    Cola(string nombre, int i);
    
     /** Destructor Default
     * @brief Default Destructor */
    ~Cola();

    /** Metodo extraer elemento de la Cola. Se extrae del inicio de la cola. Primer elemento
     * @brief Metodo extraer elemento de la Cola. Se extrae del inicio de la cola. Primer elemento*/
    void deQueue(string &aux);

    /** Metodo Agregar elemento a la Cola. Se agrega al final de la cola
     * @brief Metodo agregar elemento a la Cola. Se agrega al final de la cola*/
    void enQueue(int i);

    /** Obtiene el valor tamaño actual de la Cola
     * @brief Obtiene el valor tamaño actual de la Cola
     * @return Int tamaño de la Cola.*/
    int getSize();

    /** Obtiene el nombre de la Cola
     * @brief Obtiene el nombre de la Cola
     * @return String nombre de la Cola.*/
    string getNombre();

    /** Metodo que devuelve booleano indicando si la Cola esta vacia o no.
     * @brief Metodo que devuelve booleano indicando si la Cola esta vacia o no
     * @return Bool Cola vacia o no.*/
    bool Vacia();

    /** metodo de utilidad para crear un nuevo nodo
     * @brief Metodo de utilidad para crear un nuevo nodo
     * @return Nodo* nuevo nodo creado*/
    Nodo* CrearNodo(int i);

    /** Metodo de utilidad para eliminar un nodo
     * @brief Metodo de utilidad para eliminar un nodo*/    
    void DestruirNodo(Nodo* nodo);

    /** metodo de utilidad para imprimir el contenido actual de la cola
     * @brief Metodo de utilidad para imprimir el contenido actual de la cola*/
    void PrintQueue();

};


