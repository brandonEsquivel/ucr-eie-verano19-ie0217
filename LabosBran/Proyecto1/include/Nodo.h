// Pragma once
#pragma once

// INCLUDES
#include "header.h"

// Defines
using namespace std;

/** @brief Implementacion de estructura Nodo, que forma parte de las estructura lista, simplemente enlazada
 * @author Brandon Esquivel
 * @date February 2020 */
struct Nodo {
    char dato;/**Dato a almacenar en el nodo(Para la pila stack)*/
    int data;/**Dato a almacenar en el nodo(para la cola queue) */
    Nodo* siguiente;/**Enlace al siguiente elemento del nodo, para la Cola, queue*/
    Nodo* Abajo;/**Enlace al elemento inferior en la pila(stack). */
};
typedef struct Nodo nodo;/**Typedef para facilidad */