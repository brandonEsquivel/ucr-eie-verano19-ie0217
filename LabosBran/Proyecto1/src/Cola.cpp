/*  Cola.cpp - Autor: Brandon Esquivel Molina - B52571 - EIE UCR
    Implementacion de la clase Cola.h
    de uso academico unicamente.    */

//INCLUDES
#include "../include/Cola.h"


Cola::Cola(){
    this->size = 0;
    this->final = NULL;
    cout << "\nCreando Cola..."<<endl;
}

Cola::Cola(string nombre, int i){
    this->nombreCola = nombre;
    this->size = 0;
    this->final = CrearNodo(i);
    cout << "\nCreando Cola..."<<endl;
}

Cola::Cola(int i){
    this->size = 0;
    this->final = CrearNodo(i);
    cout << "\nCreando Cola..."<<endl;
}

Cola::~Cola(){
    if(this->Vacia()==true){
        // do nothing
    }else{
        while (this->final)
        {
            Nodo* ptr = this->final->siguiente;
            delete this->final;
            this->final = ptr;
        }
    cout << "\nCola eliminada con exito." << endl;
    }
}

Nodo* Cola::CrearNodo(int i){
    Nodo* nodo = new Nodo;
    nodo->data = i;
    nodo->siguiente = NULL;     
    return nodo;
}

void Cola::DestruirNodo(Nodo* nodo){
    delete nodo;
}

bool Cola::Vacia(){
    return (this->size==0 || this->final==NULL);
    }

string Cola::getNombre(){
    return this->nombreCola;
}

int Cola::getSize(){
    return this->size;
}

void Cola::enQueue(int i){
    Nodo* nodo = CrearNodo(i);
    if(this->Vacia()==true){
        cout << "Pushing"<<endl;
        this->final = nodo;
        this->size++;    
    }
    else{
        nodo->siguiente = this->final;
        this->final = nodo;
        this->size++;
    }
}

void Cola::PrintQueue(){
    if(Vacia()==true){
        cout <<"\nCola vacia, nada que imprimir.\n";
    }
    else{
        cout << "Imprimiendo cola: \n"<<endl;
        Nodo *ptr = this->final;
        cout << ptr->data;
        while(ptr->siguiente){
            ptr = ptr->siguiente;
            cout << ptr->data;
        }
    }
}

void Cola::deQueue(string &aux){
    if(Vacia()==true){
        cout << "\nCola vacia."<<endl;
    }
    else
    {
        Nodo *ptr = this->final;
        if(ptr->siguiente==NULL){
            cout <<"\nUltimo elemento:";
            aux += (ptr->data);
            cout<<"\nSacando a \"" << ptr->data << "\" de la cola."<<endl;
            delete ptr;
            this->final= NULL;
            this->size--;
        }
        else{
            while(ptr->siguiente->siguiente){
                ptr = ptr->siguiente;
            }
            aux += (ptr->siguiente->data);
            cout<<"\nSacando a \"" << ptr->siguiente->data << "\" de la cola."<<endl;
            delete ptr->siguiente;
            ptr->siguiente = NULL;
            this->size--;
        }
    }
}