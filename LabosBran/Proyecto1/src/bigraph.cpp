//INCLUDES
#include "../include/Cola.h"

// DEFINES
vector<bool> mark;
mark = vector<bool>(graph.V, false);
// LEMON includes


using namespace lemon;

int main(){
    // Bi-Directional graph
    ListGraph graph;

    // Graph nodes
    ListGraph::Node a = graph.addNode();
    ListGraph::Node b = graph.addNode();
    ListGraph::Node c = graph.addNode();
    ListGraph::Node d = graph.addNode();
    ListGraph::Node e = graph.addNode();
    ListGraph::Node f = graph.addNode();
    ListGraph::Node g = graph.addNode();
    ListGraph::Node h = graph.addNode();

    // Graph edges
    ListGraph::Edge ab = graph.addEdge(a, b);
    ListGraph::Edge ag = graph.addEdge(a, g);
    ListGraph::Edge bc = graph.addEdge(b, c);
    ListGraph::Edge bg = graph.addEdge(b, g);
    ListGraph::Edge cd = graph.addEdge(c, d);
    ListGraph::Edge cf = graph.addEdge(c, f);
    ListGraph::Edge ch = graph.addEdge(c, h);
    ListGraph::Edge de = graph.addEdge(d, e);
    ListGraph::Edge df = graph.addEdge(d, f);
    ListGraph::Edge ef = graph.addEdge(e, f);
    ListGraph::Edge fg = graph.addEdge(f, g);
    ListGraph::Edge fh = graph.addEdge(f, h);
    ListGraph::Edge gh = graph.addEdge(g, h);

    // Edges costs
    ListGraph::EdgeMap<int> cost(graph);
    cost[ab] = 4;
    cost[ag] = 8;
    cost[bc] = 8;
    cost[bg] = 11;
    cost[cd] = 7;
    cost[cf] = 4;
    cost[ch] = 2;
    cost[de] = 9;
    cost[df] = 14;
    cost[ef] = 2;
    cost[fg] = 1;
    cost[fh] = 6;
    cost[gh] = 7;

    // Traversing graph edges
    std::cout << "**Imprimiendo información de aristas**" << std::endl;
    for(ListGraph::EdgeIt it(graph); it != INVALID; ++it){
        std::cout << "Arista " << graph.id(it)  << " entre los nodos " << 
        graph.id(graph.u(it)) << " y " << graph.id(graph.v(it)) << 
        " tiene un costo de " << cost[it] << std::endl;
    }

    // Traversing graph nodes using an iterator
    std::cout << "**Imprimiendo nodos**" << std::endl;
    for(ListGraph::NodeIt it(graph); it != INVALID; ++it){
        std::cout << "Nodo " << graph.id(it) << std::endl;
    }

    // Obtaining neighbours from a given node
    std::cout << "**Imprimiendo cantidad de vecinos del nodo 0**" << std::endl;
    ListGraph::Node temp = graph.nodeFromId(0);
    std::vector<int> neighbours;
    int count = 0;
    for(ListGraph::EdgeIt it(graph); it != INVALID; ++it){
        if( graph.id(graph.u(it)) == graph.id(temp) ){
            neighbours.push_back(graph.id(graph.v(it)));
            count++;
        }
    }
    std::cout << "Nodo " << graph.id(temp) << " tiene " << count << " vecinos" << std::endl;
    std::cout << "Vecinos de 0 son: " << std::endl;
    for(int i = 0; i < neighbours.size(); i++){
        std::cout << "\t-" << neighbours[i] << std::endl;
    }

    // Put your methods calls here
    recorrer_graph(graph);

    exit(0);
}







void recorrer_graph(ListGraph &graph){
std::vector<int> noVisitados;
Cola cola;
// recorrer nodos y agregarlos a lista de noVisitados
for(ListGraph::NodeIt it(graph); it != INVALID; ++it){
    noVisitados.push_back(graph.id(it));
}

for (int j = 0; j < noVisitados.size(); j++){
    if(cola.Vacia()==false)
    cola.enQueue(noVisitados[j]);                   // agregando a la cola
    noVisitados.erase(noVisitados.begin()+j);       // quitando de noVisitados, es decir marcando como visitado
    
} 



}



// Declaraciones en el archivo .h
const int INF = 1000000;
int cn; //cantidad de nodos
vector< vector > ady; //matriz de adyacencia

// Devuelve la matriz de adyacencia del arbol minimo.
vector< vector > Grafo :: prim(){

// uso una copia de ady porque necesito eliminar columnas
vector< vector > adyacencia = this->ady;
vector< vector > arbol(cn);
vector markedLines;
vector :: iterator itVec;

// Inicializo las distancias del arbol en INF.
for(int i = 0; i < cn; i++) arbol[i] = vector (cn, INF);

int padre = 0;
int hijo = 0;
while(markedLines.size() + 1 < cn){ padre = hijo;
// Marco la fila y elimino la columna del nodo padre. markedLines.push_back(padre); 
for(int i = 0; i < cn; i++) adyacencia[i][padre] = INF;
 
// Encuentro la menor distancia entre las filas marcadas. 
// El nodo padre es la linea marcada y el nodo hijo es la columna del minimo. 
int min = INF; for(itVec = markedLines.begin(); itVec != markedLines.end(); itVec++) for(int i = 0; i < cn; i++) if(min > adyacencia[*itVec][i]){
min = adyacencia[*itVec][i];
padre = *itVec;
hijo = i;
}

arbol[padre][hijo] = min;
arbol[hijo][padre] = min;
}
return arbol;
}





