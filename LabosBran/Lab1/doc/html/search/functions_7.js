var searchData=
[
  ['setcarcel',['setCarcel',['../classjugador.html#a9552a7f34b6b26cead7914b31e77a8d2',1,'jugador']]],
  ['setcartas',['setCartas',['../classjugador.html#a099a5d287b83d5a00f92fa1fed01c0af',1,'jugador']]],
  ['setcasilla',['setCasilla',['../classTablero.html#a3f44082f9ccad3d929173d0c5aa5ab4e',1,'Tablero']]],
  ['setcomprado',['setComprado',['../classcarta.html#a220a2e22b5020bcbfdc3ebf62b500c8f',1,'carta']]],
  ['setcontadorcarcel',['setContadorCarcel',['../classjugador.html#a6e7d02b0f84920fd68a087faff6b8d27',1,'jugador']]],
  ['setjugador',['setJugador',['../classTablero.html#ac0f8c79edb4873b24bbed6fdd5e8ad6e',1,'Tablero']]],
  ['setjugasiguiente',['setJugaSiguiente',['../classTablero.html#ab70f77e4e4cd1f071239b963d2fe868f',1,'Tablero']]],
  ['setnombre',['setNombre',['../classjugador.html#a3dffe407f56c869a3fb33a28f5da89b3',1,'jugador']]],
  ['setpostablero',['setPosTablero',['../classjugador.html#a47eec9b1f2f204b52d380919dce05f4f',1,'jugador']]],
  ['setpropiedad',['setPropiedad',['../classjugador.html#a0f51eabb70b47afe2e27f60be14797ec',1,'jugador']]],
  ['setpropietario',['setPropietario',['../classcarta.html#af4d91713ba5382345943cba62403ed71',1,'carta']]],
  ['setsaldo',['setSaldo',['../classjugador.html#a2c5e9904a5faac39b5412d235f39802e',1,'jugador']]],
  ['setsalir',['setSalir',['../classTablero.html#a6ca76109dddd1cd8463db48aa8a0a22b',1,'Tablero']]]
];
