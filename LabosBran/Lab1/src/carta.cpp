// Clase cartas/ Propiedades del monopoly - Brandon Esquivel

// INCLUDES
#include "carta.h"


// DEFINES

using namespace std;



//metodos get generales
string carta::getNombre(){
return this->nombre;

}

string carta::getColor(){
return this->color;

}

int carta::getPrecio(){
    return this->precio;

}

int carta::getPosTablero(){
return this->posTablero;

}

int carta::getAlquiler(){
return this->alquiler;
}

bool carta::getTipo(){
return this->libre;

}
bool carta::getComprado(){
    return this->comprado;
    }

string carta::getPropietario(){
return this->propietario;
}

// metodos set

void carta::setComprado(){
    this->comprado = true;
    }

int carta::hipotecar(){
    this->comprado = false;
    return this->hipoteca;
}

void carta::setPropietario(string dueno){
    this->propietario = dueno;
}




//metodo de creacion
carta::carta(string nombre, string color, int precio, int alquiler, int hipoteca, int posTablero, bool comprado, bool libre){

this->nombre = nombre;
this->color = color;
this->precio = precio;
this->alquiler = alquiler;
this->hipoteca = hipoteca;
this->posTablero = posTablero;
this->comprado = comprado;
this->libre = libre;
}

//metodo de creacion
carta::carta(){
this->nombre = "GOKU";
this->color = "anaranjado";
this->precio = 2000;
this->alquiler = 100;
this->hipoteca = 200;
this->posTablero = 1;
this->comprado = false;
this->libre = true;
this->propietario="none";
}

carta::~carta(){
    //Do nothing
}
