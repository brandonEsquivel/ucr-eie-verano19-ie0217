

//INCLUDES

//#include "C:\Users\Brn-Hisoka\Documents\Utemporal\Algoritmos\ucr-eie-verano19-ie0217\LabosBran\Lab1\include\tablero.h"


#include "tablero.h"

//DEFINES

using namespace std;



int main() {

//vector por defecto de propiedades
string propiedadesDefault[10] = {"none","none","none","none","none","none","none","none","none","none"};
string jugadores[5] = {"none", "none", "none", "none","none"};
// saldo inicial de los jugadores
int saldo_inicial = 800;//getNombre()
int numJugadores;
bool fin = false;


// inicio menu del juego
cout << "\tBienvenido al juego Monopoly shinobi"<<endl;
cout<<endl<<"\t Consideraciones"<<endl;
cout<<" x: salir del juego"<<endl;
cout<<" i: mostrar información del jugador"<<endl; 



//creacion de jugadores


cout<< "Ingrese numero de jugadores: 2-3-4: ";
cin >> numJugadores;

    if(numJugadores>4){
        cout << "\n\n\t Numero incorrecto de jugadors. Elija 2,3 o 4." << endl;
}
    else{
        int i;                                                      // Creando vector de jugadores
        for(i=1; i<numJugadores+1; i++){

            cout << "\n Jugador " << i << " Ingrese su nombre: ";
            cin >> jugadores[i];

            }



//creando cartas/propiedades del juego y el tablero


Tablero tablero(numJugadores);             //Se ingresa numero de jugadores

carta go("go","blanco",0,200,0,0,false,true);
tablero.setCasilla(go);

carta kushina("kushina","rojo",140,14,110,1,false,false);
tablero.setCasilla(kushina);

carta libre2("libre2","blanco",0,0,0,2,false,true);
tablero.setCasilla(libre2);

carta boruto("boruto","rojo",140,14,110,3,false,false);
tablero.setCasilla(libre2);

carta naruto("naruto","rojo",160,16,130,4,false,false);
tablero.setCasilla(libre2);

carta libre5("libre5","blanco",0,0,0,5,false,true);
tablero.setCasilla(libre5);

carta hiruzen("hiruzen","amarillo",160,16,130,6,false,false);
tablero.setCasilla(hiruzen);

carta libre7("libre7","blanco",0,0,0,7,false,true);
tablero.setCasilla(libre7);

carta kakashi("kakashi","amarillo",160,16,130,8,false,false);
tablero.setCasilla(kakashi);

carta minato("minato","amarillo",170,17,140,9,false,false);
tablero.setCasilla(minato);

carta libre10("libre10","blanco",0,0,0,10,false,true);
tablero.setCasilla(libre10);

carta tsunade("tsunade","naranja",170,17,140,11,false,false);
tablero.setCasilla(tsunade);

carta jiraiya("jiraiya","naranja",170,17,140,12,false,false);
tablero.setCasilla(jiraiya);

carta libre13("libre13","blanco",0,0,0,13,false,true);
tablero.setCasilla(libre13);

carta orochimaru("orochimaru","naranja",180,18,150,14,false,false);
tablero.setCasilla(orochimaru);

carta libre15("libre15","blanco",0,0,0,15,false,true);
tablero.setCasilla(libre15);

carta hashirama("hashirama","verde",180,18,150,16,false,false);
tablero.setCasilla(hashirama);

carta tobirama("tobirama","verde",180,18,150,17,false,false);
tablero.setCasilla(tobirama);

carta IRcarcel("IRcarcel","blanca",0,0,0,18,false,true);
tablero.setCasilla(IRcarcel);

carta butsuma("butsuma","verde",190,19,160,19,false,false);
tablero.setCasilla(butsuma);

carta libre20("libre20","blanco",0,0,0,20,false,true);
tablero.setCasilla(libre20);

carta hanabi("hanabi","celeste",190,19,160,21,false,false);
tablero.setCasilla(hanabi);

carta libre22("libre22","blanco",0,0,0,22,false,true);
tablero.setCasilla(libre22);

carta hinata("hinata","celeste",190,19,160,23,false,false);
tablero.setCasilla(hinata);

carta libre24("libre24","blanco",0,0,0,24,false,true);
tablero.setCasilla(libre24);

carta neji("neji","celeste",200,20,170,25,false,false);
tablero.setCasilla(neji);

carta libre26("libre26","blanco",0,0,0,26,false,true);
tablero.setCasilla(libre26);

carta carcel("carcel","blanco",0,0,0,27,false,true);
tablero.setCasilla(carcel);

carta hiashi("hiashi","celeste",210,21,180,28,false,false);
tablero.setCasilla(hiashi);

carta libre29("libre29","blanco",0,0,0,29,false,true);
tablero.setCasilla(libre29);

carta sasuke("sasuke","azul",210,21,180,30,false,false);
tablero.setCasilla(sasuke);

carta libre31("libre31","blanco",0,0,0,31,false,true);
tablero.setCasilla(libre31);

carta shisui("shisui","azul",210,21,180,32,false,false);
tablero.setCasilla(shisui);

carta libre33("libre33","blanco",0,0,0,33,false,true);
tablero.setCasilla(libre33);

carta itachi("itachi","azul",220,22,190,34,false,false);
tablero.setCasilla(itachi);

carta impuestos("impuestos","blanco",0,200,0,35,false,true);
tablero.setCasilla(impuestos);

// Declarando jugadores
jugador jug1(jugadores[1], propiedadesDefault, saldo_inicial, 0, 0, false);
jugador jug2(jugadores[2], propiedadesDefault, saldo_inicial, 0, 0, false);
jugador jug3(jugadores[3], propiedadesDefault, saldo_inicial, 0, 0, false);
jugador jug4(jugadores[4], propiedadesDefault, saldo_inicial, 0, 0, false);

// Seteando jugadores activos en el tablero

switch(numJugadores){
case 2: {
    tablero.setJugador(jug1);
    tablero.setJugador(jug2);
    break;
}
case 3: {
    tablero.setJugador(jug1);
    tablero.setJugador(jug2);
    tablero.setJugador(jug3);
    break;
}
case 4: {
    tablero.setJugador(jug1);
    tablero.setJugador(jug2);
    tablero.setJugador(jug3);
    tablero.setJugador(jug4);
    break;
}

default:
    cout << "error" << endl;
    break;

}


//tablero.getJugadoresGuar();



// INICIO DEL JUEGO

while(fin!=true){

 fin = tablero.play();




}



















}        // fin else manejo de numero de jugadores


return 0;
}




