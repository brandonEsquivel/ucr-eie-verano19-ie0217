#pragma once

//INCLUDES
//#include "jugador.h"
//--------------

#include "carta.h"
#include "jugador.h"

//--------------



using namespace std;

/** @brief Implementacion de clase jugadores/jugadores juego del monopoly

    Implementation de clase Tablero para el juego monopoly shinobi
    @author Brandon Esquivel
    @date January 2019
    */

class Tablero{
  private:
    carta cartas[36]; /**<Vector de referencia a cartas del tablero*/
    jugador jugadores[5]; /**<vector de referencia  a jugadores del tablero*/
    int num_juga;/**<Numero de jugadores activos*/
    int cantidadJ;/**<cantidad de jugadores, contador del vector jugadores*/
    int cantidadC; /**<cantidad de cartas en el tablero, contador*/
    int num_juga_sig;/**Jugador que continua jugando*/
    bool salir;/**Guarda condicion de salida*/

  public:
    /** Default constructor. */
    Tablero();

        /** Custom constructor. Inicializa al jugador
            @param num_jug - numero de jugadores activos a definir
            */
        Tablero(int num_jug);

        /** Setea un jugador en el tablero
            @param jugador - jugador en la partida
            */
        void setJugador(jugador per);

        /** Setea una casilla en el tablero.
            @param carta - espacio en el tablero carta
            */
        void setCasilla(carta car);


        /** metodo de manejo del juego por turnos, devuelve la condicion de fin de juego
            @param fin de juego - bandera que determina el fin del juego
            */
        bool play();//************************

	/** Muestra  nombre de todos los jugadores guardados
            @return nombre de los jugadores guardados
            */
	void getJugadoresGuar();

	/** Controla el indice del jugador proximo a jugar
        */
        void setJugaSiguiente();

	/** Tiene acceso al indice del jugador a tirar los dados
            @return retorna el indice del jugador a jugar
            */
	int getJugaSiguiente();

	/** Se encarga de verificar si el jugador esta en carcel,
	de tirar los dados, de llamar funciones para mostrar casilla 
	en que cayo y de cambiar indice del proximo jugador. 
        */
	void moverJugador();

	/** Muestra informacion basica de casilla del tablero
            @return nombre y precio de casilla.
            */
	void getCasilla(int num);
      
	/** Encargada del proceso de compra de la propiedad
            @param num: indice de la posicion de la casilla en el vector.
            */
	void Compra(int num);
	
	
	/** Encargada de buscar cual jugador tiene una carta.
	@param nomCar: nombre de la carta a consultar
            */	
	void pagaAlquiler(string nomCar);

	/** Modifica condicion de salida.
	@param valor: contiene false para seguir jugando o true para salir
            */	
	void setSalir(bool valor);
	
	/** Muestra condicion de salida.
	@return retorna condicion de salida
            */	
	bool getSalir();

        /** Default destructor */
        ~Tablero();
};
