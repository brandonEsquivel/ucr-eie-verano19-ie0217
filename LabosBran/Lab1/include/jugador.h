#pragma once

//INCLUDES
#include "carta.h"



//DEFINES
using namespace std;

/** @brief Implementacion de clase jugadores/jugadores juego del monopoly

    Implementation de clase jugador para el jeugo monopoly shinobi
    @author Brandon Esquivel
    @date January 2019
    */

class jugador{
    private:
        string nombre;/**<Nombre del jugador*/
        string propiedades[10];/**<Vector de propiedades del dueno*/
        int saldo;/**<dinero restante*/
        int contadorCarcel, posTablero;/**<valores numericos del jugador*/
        bool carcel;/**<Indicador de estado de la propiedad*/
	carta cartasJ[16];
	int conCartas;

    public:
        /** Default constructor. */
        jugador();

        /** Custom constructor. Inicializa al jugador
            @param nombre - Nombre del jugador
            @param propiedades - propiedades adquiridas por el jugador
            @param posTablero - indicador de la posicion en el tablero
            @param carcel - indicador de estado en carcel
            @param contadorCarcel - contador de turnos para salir de carcel
            */
        jugador(string nombre, string propiedades[10], int saldo, int posTablero, int contadorCarcel, bool carcel);


        /** Default destructor */
        ~jugador();


        // Definiciones metodos set/get generales

        // gets del jugador

        /** Get nombre del j ugador
            @return nombre del jugador
            */
        string getNombre();

         /** Get propiedades del jugador
            @return propiedades del jugador
            */
        void getPropiedades();


        /** Get posicion en el tablero del juagdor
            @return posTablero del jugador
            */
        int getPosTablero();


        /** Get saldo del jugador
            @return saldo
            */
        int getSaldo();

        /** Get indicador bool carcel del jugador
            @return carcel
            */
        bool getCarcel();

        /** Get indicador bool Contador carcel del jugador
            @return contadorCarcel
            */
        int getContadorCarcel();

        /** Get informacion del jugador imprime todos los parametros actuales del jugador
            @return informacion
            */
        void getInformacion();

        // Definiciones set del jugador

        /** agrega una propiedad cuando se compra
            @param propiedades - vector de propiedades adquiridas
            */
        void setPropiedad(carta c);

        /** Setea cuando hay un cambio en el saldo de un jugador por compra o pago.
            @param saldo - dinero restante del jugador
            */
        void setSaldo(int saldo);


        /** Setea el indicador carcel cuando un jugador cae en ir carcel
            @param carcel - indicador de carcel del jugador
            */
        void setCarcel();


        /** cambia el nombre del jugador
            @param nombre - nombre del jugador
            */
        void setNombre(string nom);

        /** cambia el contador de turnos en carcel del jugador
            @param contadorCarcel - indicador de contadorCarcel del jugador
            */
        int setContadorCarcel();

        /** cambia la posicion del jugador en el tablero
            @param posTablero - indicador de la posicion del jugador
            */
        void setPosTablero(int pos);
	

        /** Guarda las propiedades que compra jugador
            @param car - Objeto de tipo carta.
            */
	void setCartas(carta car);


        /** Busca propiedad en el vector de cartas del jugador
            @param nom - nombre de carta a buscar.
	    @return  El precio del alquier. 
            */
	int buscarCarta(string nom);



	

};
