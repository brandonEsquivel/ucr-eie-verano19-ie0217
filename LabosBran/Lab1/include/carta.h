#pragma once

//INCLUDES
#include "header.h"
#include <string>
#include <iostream>
#include <string>
//#include <conio.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

//DEFINES
using namespace std;

/** @brief Implementacion de cartas/propiedades del juego Monopoly.

    Implementacion de cartas con su color, precio, alquiler hipoteca y otros atributos.
    @author Brandon Esquivel
    @date January 2019
    */

class carta{
    private:
        string nombre;/**<Nombre de la carta*/
        string color;/**<Color de la carta*/
        string propietario;/**<dueno de la carta*/
        int precio, alquiler, hipoteca, posTablero;/**<valores numericos de la carta*/
        bool comprado;/**<Indicador de estado de la propiedad*/
        bool libre;/**<Indicador del tipo de carta: libre o propiedad*/

    public:
        /** Default constructor. */
        carta();

        /** Custom constructor. Inicializa la carta no propiedad
            @param nombre - nombre de carta
            @param alquiler - precio a cobrar al caer
            */
        carta(string nombre,int alquiler);


        /** Custom constructor. Inicializa carta propiedad
            @param nombre - Nombre de la propiedad
            @param color - Color de la propiedad
            @param propietario - dueno de la propiedad
            @param precio - Precio de la propiedad
            @param alquiler - Precio a cobrar al caer
            @param hipoteca - valor de venta al banco (devolvel)
            @param posTablero - indica la pocision en el tablero de la propiedad/carta
            @param comprado - indicador de estado de la propiedad
            @param libre - indicador de carta tipo libre
            */
        carta(string nombre, string color, int precio, int alquiler, int hipoteca, int posTablero, bool comprado, bool libre);


        /** Default destructor */
        ~carta();


        // Definiciones metodos set/get generales


        /** Get nombre de la carta
            @return nombre de la carta
            */
        string getNombre();

         /** Get color de la carta
            @return color de la carta
            */
        string getColor();


        /** Get posicion en el tablero de la carta
            @return posTablero de la carta
            */
        int getPosTablero();


        /** Get propietario de la carta
            @return propietario de la carta
            */
        string getPropietario();

        /** Get alquiler de la carta
            @return alquiler
            */
        int getAlquiler();

        /** Get tipo de la carta, si es propiedad/ impuesto o libre
            @return alquiler
            */
        bool getTipo();

         /** Crea las cartas del juego en memoria
            */



        // Definiciones metodos para compra/venta de propiedades

        /** Setea cuando se compra una propiedad
            @param comprado - bool indicador de estado
            */
        void setComprado();

        /** Setea cuando se hipoteca una propiedad y devuelve el precio de hipoteca
            @param hipoteca - valor de venta al banco (devolvel)
            @param comprado - bool indicador de estado
            */
        int hipotecar();


        /** Setea el nuevo propietario cuando se compra una propiedad
            @param propietario - dueno de la propiedad
            */
        void setPropietario(string propietario);


        /** obtiene el precio de una propiedad
            @param precio - valor de la propiedad
            */
        int getPrecio();

        /** obtiene el indicador de comprado de una propiedad
            @param comprado - indicador comprado de la propiedad
            */
        bool getComprado();

};
