

// INCLUDES
#include "../include/header.h"

// DEFINES

using namespace std;

    //metodos
// set y get umbral
void contarPalabras::setUmbral(int u){
    this->umbral = u;
}

int contarPalabras::getUmbral(){
    return this->umbral;
}


int contarPalabras::getCantidadPalabras(){
return this->cantidadPalabras;
}


int contarPalabras::getCantidadPalabrasUmbral(){
return this->cantidadPalabrasUmbral;
}



int contarPalabras::contar(char nombre[]){

    f = fopen(nombre,"r");                                                  // Abriendo archivo en modo lectura
                                                    
    if (f == NULL) {                                                        // Comprobando que se abrio bien
        cout << "No se pudo abrir el archivo.\n"<< endl;
        exit(EXIT_FAILURE);
    }

    fscanf(f, "%s", palabra_actual);                                        // Se escanea la primer palabra y se guarda.
    cantidadPalabras++;                                                     // contador de palabras no repetidas
    palabras.push_back(palabra_actual);                                     // Agregando palabra inicial
    while(!feof(f)){                                                        // Mientras no sea el fin del archivo, escanea palabra por palabra y la almacena en palabra_actual
        bool agregar = true;                                                // Bandera de comprobacion si la palabra esta repetida                               
        fscanf(f, "%s", palabra_actual);                                    // Escanea palabra por palabra y la almacena en palabra_actual
        for( size_t i = 0; i < palabras.size(); ++i)                        // Recorre el vector verificando si la palabra ya esta guardada
        {                                            
            if(this->palabras[i]==this->palabra_actual){                    // SI LA PALABRA ESTA REPETIDA, SE LA SALTA
                agregar = false;
                break;
            } 
        }
        if(agregar==true){                                                  // Agrega cada palabra NO REPETIDA en una posicion del vector palabras y aumenta el contador de palabas
            this->palabras.push_back(this->palabra_actual);
            this->cantidadPalabras++;
        }      
    }               // fin while
    fclose(f);                                                              // Cerrando archivo
    cout << "Se han contado las palabras. Total de palabras usadas NO REPETIDAS: " << this->cantidadPalabras << endl;   
    return this->cantidadPalabras;
}


void contarPalabras::guardarPalabras(char nombre[]){

cout << "\nIniciando conteo sobre umbral..." << endl;  

 f = fopen(nombre,"r");                                                         // Abriendo archivo en modo lectura
    if (f == NULL) {                                                            // Comprobando que se abrió bien
        cout << "No se pudo abrir el archivo.\n"<< endl;
        exit(EXIT_FAILURE);
    }

    for (size_t i = 0; i < palabras.size(); i++){                                 // Se recorre el vector de palabras guardadas
        n=0;
        while(!feof(f)){                                                          // Mientras no sea el fin del archivo escanea palabra por palabra
            fscanf(f, "%s", palabra_actual);                                      // Recorre el vector hasta el numero de palabras contadas NO repetidas y aumenta su contador
            if (palabras[i]==palabra_actual){                                    // Si la palabra leida concuerda con la analizada actualmente del vector, se suma el contador
                n++;
            }
        }
        rewind(f);                                                                  // Reestableciendo Puntero del archivo abierto para nueva iteracion de lectura
        if(n>=umbral){                                                              // Si el numero de repeticiones de la palabra es mayor que el umbral, se agrega al vector de palabras sobre umbral para sustituir.
            cout << "\nLa palabra " << palabras[i] << " se repite mas del umbral( " << n << " veces) se sustituira."<< endl;
            palabrasUmbral.push_back(palabras[i]);
            cantidadPalabrasUmbral++;                                                // Aumentando contador de palabras a sustituir
        }
    }

    
    
    cout << "\nSe han terminado de contar y guardar las palabras repetidas por encima del umbral definido. El total es: " << cantidadPalabrasUmbral << endl;
    fclose(f);                                                                       // Cerrando archivo abierto
}   



void contarPalabras::crearTabla(){
    cout << "Creando tabla in.tab ... " << endl;                                        // Encabezado

    ofstream tabla;                                                                     // Abriendo archivo .tab para escribir tabla
    tabla.open("in.tab");   
                                                                                        // Recorriendo vector con palabras a sustituir                                        
    for(int j=0; j<cantidadPalabrasUmbral; j++){
        tabla   <<  palabrasUmbral[j] << "\t\t@" << j <<   endl;                        // Apend
    }
    cout << "\nFinalizado. " << endl;
    tabla.close();                                                                      // Cerrando archivo


}


void contarPalabras::sustituir(char nombre[]){
    cout << "\nSe procede a realizar la compresion por sustitucion..." << endl;                // Encabezado
    
    char caracter;                                                              // Variable auxiliar para guardar caracter.
    string add = "";                                                            // Variable temporal para almacenar 
    f = fopen(nombre,"r");                                                      // Abriendo copia para sustituir
    if (f == NULL) {                                                            // Comprobando que se abrio bien
        cout << "No se pudo abrir el archivo.\n"<< endl;
        exit(EXIT_FAILURE);
    }
    

    // Ahora se procede a crear un string con la informacion del archivo a sustituir, luego se sustituyen las palabras sobre Umbral.
    // Hay dos formas de hacerlo: leyendo palabra por palabra o caracter por caracter
    // La primera obvia los saltos de linea por lo que escribe toda la informacion comprimida  en una sola linea
    // La Segunda permite tomar en cuneta estos saltos de linea, pero sustituye coincidencias dentro de palabras.
    // Como lo que se desea es comprimir el archivo, a mayor sustitucion mayor compresion, ademas se mantiene el formato, se utiliza la segunda opcion
    
    // 1 - FORMA PALABRA POR PALABRA 
/*
    fscanf(f, "%s", palabra_actual);                                              // Escaneando primer palabra 
    while(!feof(f)){                                                              // Mientras no sea el fin del archivo escanea palabra por palabra 
        add = add + " " + palabra_actual;                                         // Concatenando cadenas
        fscanf(f, "%s", palabra_actual);                                          // Escaneando palabra
    } 
  */  


    // 2 - Forma Caracter por caracter - Note que se utiliza una referencia a la variable auxiliar caracter

    fscanf(f, "%c", &caracter);                                              // Escaneando primer caracter (solucion error de ultimo caracter)
    while(!feof(f)){                                                         // Mientras no sea el fin del archivo escanea palabra por palabra 
        add = add + caracter;                                                // Concatenando cadenas
        fscanf(f, "%c", &caracter);                                          // Escaneando caracter
    } 


    
    for(int j=0; j<cantidadPalabrasUmbral; j++){                             // Realizando sustitucion
            int pos = add.find(palabrasUmbral[j]);                           // Se obtiene la posicion de la palabra a sustituir en el string
            stringstream ss;                                                 // Se crea objeto stringstream para cast
            ss << j;                                                         // Agregando valor de iterador para sustitucion con @j -> @1, @2... etc
            string sust = "@"+ ss.str();                                     // Cast
            while(pos!= -1){                                                 // Pos devuelve -1 cuando termina
                add.replace(pos, palabrasUmbral[j].size(), sust);            // Reemplazando palabra por codigo
                pos = add.find(palabrasUmbral[j], pos + sust.size());        // Solucion para que continue sustituyendo luego de la actual sustitucion (Sino se embucla)
            }
    }          
    fclose(f);                                                                // Cerrando archivo
    
        // Se creo el string sustituido, ahora se incluye en in.rep

        ofstream cp;                                                          // Abriendo archivo in.rep para realizar sustitucion
        cp.open("in.rep");                                                     

        for(size_t j = 0; j<add.size(); j++){                                 // Recorriendo string add comprimido caracter por caracter para escribirlo en el archivo de salida                 
            caracter = add.at(j);                                             // Obteniedo caracter
            cp << caracter;                                                   // Escribiendo en archivo de salida
        }
        cp.close();                                                           // Cerrando archivo

     cout << "\nSutitucion Completada. Archivo in.rep comprimido creado." << endl; 

    //HASTA AQUI OK! Se ha logrado el objetivo del laboratorio
    // Conocimientos adquiridos -> EXC
    // Manejo de tiempo -> terminado 35 horas antes de la entrega



}








//metodo de creacion - Inicializa vectores
contarPalabras::contarPalabras(int umbral){
    this->umbral=umbral;
    this->cantidadPalabrasUmbral=0;
    this->cantidadPalabras=0;
    this->n=0;

}

contarPalabras::~contarPalabras(){
    //Do nothing
}
