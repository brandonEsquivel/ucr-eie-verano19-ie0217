#pragma once

// INCLUDES

#include "header.h"
#include "Nodo.h"

//DEFINES
using namespace std;

/** @brief Implementacion de estructura Lista simplemente enlazada, para un mejor panorama de programacion ante Pila y Cola.
 * @author Brandon Esquivel
 * @date February 2020 */
typedef struct Lista{
    string nombreLista;/**Nombre de la lista*/
    Nodo* head;/**Puntero a la cabeza de la lista*/
    int size;/**Tamano de la lista*/
} Lista;