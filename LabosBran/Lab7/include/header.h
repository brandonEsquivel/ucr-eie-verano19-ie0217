/** 
 * @authors Brandon Esquivel, Yasser Wagon, Reinier Camacho
 * @date Febrero 2020
 * @warning UCR EIE IE217 III-19
 * @mainpage  Laboratorio 7: Colas y Pilas en C++
 * @brief Implementacion del Laboratorio 7 del curso IE0217 Estructuras abstractas y algoritmos para ingenieria.
 * @section intro_sec Introduccion
 * En el presente codigo se muestra la solucion implementada del Laboratorio 7 del curso IE0217 Estructuras abstractas y algoritmos para ingenieria, de la escuela de Ingenieria Electrica , EIE, UCR.
 * El objetivo de este Laboratorio es brindarle al estudiante la capacidad de modelar e implementar las estructuras colas y pilas (Stack y Queue) en C++ por medio de listas enlazadas simples. 
 * @section compile_sec Compilacion y ejecucion
 * Ejecute el comando make en la carpeta raiz, luego, para correr ejecute make run o bien ./bin/main
 * @section Documentation_sec Documentacion
 * Aparte de este HTLM podra encontrar un pdf en doc/latex/refman.pdf latex con toda la informacion detallada. Si no encuentra el pdf, utilize el comando make en esa carpeta para generarlo.
 * @section about_sec Sobre el programa
 * Se Implementan las estructuras de datos abstractos Stack y Queue utilizando POO en C++, por medio de una lista simplemente enlazada.
 * Se Implementa una funcion en C++ que veriﬁca si una hilera de caracteres tiene los parentesis balanceados, Utilizando una pila.
 * Se Implementa una funcion en C++ que veriﬁca si una hilera de caracteres es un palindromo. Utilizando una cola.
 * @section concepts_sec Resumen Teorico
 * @subsection estruct_sec Estructuras de Datos
 * Las estructuras de datos se construyen con clases autorreferenriadas, que contienen miembros que se conocen como enlaces y que apuntan a objetos del mismo tipo de la clase.
 * Las clases autorreferenciadas permiten que muchos objetos se enlacen entre si para formar pilas, colas, listas y arboles.
 * La asignacion dinamica de memoria reserva un bloque de bytes en memoria para almacenar un objeto durante la ejecucion de un programa.
 * Una lista enlazada es una coleccion lineal de objetos de clases autorreferenciadas.
 * Una lista enlazada es una estructura de datos dinamica: la longitud de la lista aumenta o disminuye segun sea necesario.
 * Las listas enlazadas pueden continuar creciendo hasta que se agote la memoria.
 * @subsection List_sec Listas, Pilas, Colas y arboles
 * Las listas enlazadas cuentan con un mecanismo para insertar y eliminar datos mediante la manipulacion de apuntadores.
 * Una lista de un solo enlace empieza con un apuntador al primer nodo y cada nodo contiene un apuntador al siguiente nodo “en secuencia”. Esta lista termina con un nodo cuyo miembro apuntador es 0. Una lista simplemente enlazada debe recorrerse en una sola direccion.
 * Una lista circular simplemente enlazada empieza con un apuntador al primer nodo, y cada nodo contiene un apuntador al siguiente nodo. El apuntador en el ultimo nodo apunta al primer nodo, cerrando asi el “circulo".
 * Una lista doblemente enlazada permite recorridos tanto hacia adelante como hacia atras. Cada nodo cuenta con un apuntador hacia adelante al siguiente nodo en la lista en direcdon de avance y con un apuntador hacia atras al siguiente nodo en la lista en direccion de retroceso.
 * En una lista circular doblemente enlazada el apuntador hada adelante del ultimo nodo apunta al primer nodo, y el apuntador hacia atras del primer nodo apunta al ultimo nodo, cerrando asi el “circulo”.
 * Las pilas y las colas son versiones restringidas de listas enlazadas.
 * En una pila, los nodos se agregan o se quitan solo de su parte superior. Por esta razon, una pila se conoce como una estructura de datos del tipo “ultimo en entrar, primero en salir (UFO)”.
 * Al miembro de enlace en el ultimo nodo de la pila se le asigna el valor nulo (cero) para indicar el fondo de la pila.
 * Las operaciones primarias utilizadas para manipular una pila son push y pop. La operadon p u sh crea un nuevo nodo y lo coloca en la parte superior de la pila. La operadon pop saca un nodo de la pane superior de la pila, elimina la memoria asignada a ese nodo y devuelve el valor que se saco.
 * En una estructura de dalos tipo cola, los nodos se quitan de la parte inicial y se agregan en la parte final. Por esta razon, una cola se conoce como estructura de datos dd tipo F1FO (primero en entrar, primero en salir). Las operaciones para agregar y quitar se conocen como en Queue 
 * Los arboles son estructuras de datos bidimensionales que requieren de dos o mas enlaces por nodo.
 * Los arboles binarios contienen dos enlaces por nodo.
 * El nodo raiz es el primer nodo en el arbol.
 * Cada uno de los apuntadores en el nodo raiz hace referenda a un hijo. El hijo izquierdo es el primer nodo en el subarbol izquierdo y el hijo derecho es el primer nodo en el subarbol derecho. Los hijos de un nodo se conocen como hermanos. Cualquier nodo de un arbol que no tiene hijos se conoce como nodo hoja
 * Un arbol de busqueda binaria tiene la caracteristica de que el valor en el hijo izquierdo de un nodo es menor que el valor en su nodo padre, y el valor en el hijo derecho de un nodo es mayor o igual que el valor en su nodo padre. Si no existen valores de datos duplicados, el valor en el hijo derecho es mayor que el valor en su nodo padre.
 * Un recorrido inorden de un arbol binario realiza un recorrido inorden dd subarbol izquierdo, procesa el valor dd nodo raiz y despues realiza un recorrido inorden del subarbol derecho. El valor en un nodo no se procesa sino hasta que se procesan los valores en d subarbol izquierdo. 
 * Un recorrido preorden procesa el valor en el nodo raiz, realiza un recorrido preorden del subarbol izquierdo y despues realiza un recorrido preordcn del subarbol derecho. El valor en cada nodo se procesa a medida que este se va encontrando.
 * Un recorrido postorden realiza un recorrido postorden del subarbol izquierdo, realiza un recorrido postorden del subarbol derecho y despues procesa el valor en el nodo raiz. El valor en cada nodo no se procesa sino hasta que se procesan todos los valores en sus subarboles.
 * **/ 

//PRAGMA ONCE
#pragma once

//INCLUDES
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <string>