// PRAGMA ONCE
#pragma once

//INCLUDES
#include "header.h"

// INCLUDES

/** @brief Implementacion de las funciones de prueba para los ejercicios con Pila y Cola.
 * @author Brandon Esquivel
 * @date February 2020 */

    /** Funcion de interfaz con el usuario para probar si una cadena de parentesis esta o no balanceada, usando una estructura pila (stack).
     * @brief Funcion de interfaz con el usuario para probar si una cadena de parentesis esta o no balanceada, usando una estructura pila (stack). */
    void Balanceados();


    /** Funcion de interfaz con el usuario para probar si una cadena de parentesis esta o no balanceada, usando una estructura Cola (Queue)
     * @brief Funcion de interfaz con el usuario para probar si una cadena de parentesis esta o no balanceada, usando una estructura Cola (Queue).*/
    void Palindromos();