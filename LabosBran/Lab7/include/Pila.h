#pragma once

// INCLUDES

#include "Nodo.h"

//DEFINES

using namespace std;




/** @brief Implementacion de la clase Pila simple
 * @author Brandon Esquivel
 * @date February 2020 */
class Pila{

    private:
        string nombrePila;/**Nombre asignado a la Pila*/
        nodo* ultimo;/**Ultimo elemento en la pila(cuspide).*/
        int size;/**Tamaño de la pila*/

    public:

    /** Constructor default
     * @brief Default Constructor */
    Pila();

    /** Constructor Custom
     * @brief Custom Constructor */
    Pila(char i);

     /** Constructor Custom
     * @brief Custom Constructor */
    Pila(string nombre, char i);

    /** Destructor Default
     * @brief Default Destructor */
    ~Pila();

    /** Metodo extraer elemento de la pila.
     * @brief Metodo extraer elemento de la pila.*/
    void Pop();

    /** Metodo Agregar elemento a la pila.
     * @brief Metodo agregar elemento a la pila.*/
    void Push();

    /** Obtiene el valor tamaño actual de la pila
     * @brief Obtiene el valor tamaño actual de la pila
     * @return Int tamaño de la pila.*/
    int getSize();

    /** Obtiene el nombre de la pila
     * @brief Obtiene el nombre de la pila
     * @return string nombre de la pila.*/
    string getNombre();

    /** Metodo que devuelve booleano indicando si la pila esta vacia o no.
     * @brief Metodo que devuelve booleano indicando si la pila esta vacia o no
     * @return bool pila vacia o no.*/
    bool Vacia();

    /** metodo de utilidad para crear un nuevo nodo
     * @brief metodo de utilidad para crear un nuevo nodo
     * @return Nodo* nuevo nodo creado*/
    Nodo* CrearNodo(char i);

    /** metodo de utilidad para eliminar un nodo
     * @brief metodo de utilidad para eliminar un nodo*/    
    void DestruirNodo(Nodo* nodo);

};

