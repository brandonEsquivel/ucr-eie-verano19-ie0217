var indexSectionsWithContent =
{
  0: "abcdefghlnpsv~",
  1: "clnp",
  2: "cfhlnp",
  3: "bcdegpv~",
  4: "adhns",
  5: "ln",
  6: "l"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Pages"
};

