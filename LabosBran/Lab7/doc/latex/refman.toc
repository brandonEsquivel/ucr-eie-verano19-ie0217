\contentsline {chapter}{\numberline {1}Laboratorio 7\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}: Colas y Pilas en C++}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Introduccion}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Compilacion y ejecucion}{1}{section.1.2}
\contentsline {section}{\numberline {1.3}Documentacion}{1}{section.1.3}
\contentsline {section}{\numberline {1.4}Sobre el programa}{2}{section.1.4}
\contentsline {section}{\numberline {1.5}Resumen Teorico}{2}{section.1.5}
\contentsline {subsection}{\numberline {1.5.1}de Datos}{2}{subsection.1.5.1}
\contentsline {subsection}{\numberline {1.5.2}, Pilas, Colas y arboles}{2}{subsection.1.5.2}
\contentsline {chapter}{\numberline {2}Class Index}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Class List}{3}{section.2.1}
\contentsline {chapter}{\numberline {3}File Index}{5}{chapter.3}
\contentsline {section}{\numberline {3.1}File List}{5}{section.3.1}
\contentsline {chapter}{\numberline {4}Class Documentation}{7}{chapter.4}
\contentsline {section}{\numberline {4.1}Cola Class Reference}{7}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Detailed Description}{8}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Constructor \& Destructor Documentation}{8}{subsection.4.1.2}
\contentsline {subsubsection}{\numberline {4.1.2.1}Cola()\hspace {0.1cm}{\relax \fontsize {8}{9.5}\selectfont \abovedisplayskip 6\p@ plus2\p@ minus4\p@ \abovedisplayshortskip \z@ plus\p@ \belowdisplayshortskip 3\p@ plus\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 3\p@ plus\p@ minus\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \ttfamily [1/3]}}{8}{subsubsection.4.1.2.1}
\contentsline {subsubsection}{\numberline {4.1.2.2}Cola()\hspace {0.1cm}{\relax \fontsize {8}{9.5}\selectfont \abovedisplayskip 6\p@ plus2\p@ minus4\p@ \abovedisplayshortskip \z@ plus\p@ \belowdisplayshortskip 3\p@ plus\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 3\p@ plus\p@ minus\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \ttfamily [2/3]}}{8}{subsubsection.4.1.2.2}
\contentsline {subsubsection}{\numberline {4.1.2.3}Cola()\hspace {0.1cm}{\relax \fontsize {8}{9.5}\selectfont \abovedisplayskip 6\p@ plus2\p@ minus4\p@ \abovedisplayshortskip \z@ plus\p@ \belowdisplayshortskip 3\p@ plus\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 3\p@ plus\p@ minus\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \ttfamily [3/3]}}{8}{subsubsection.4.1.2.3}
\contentsline {subsubsection}{\numberline {4.1.2.4}$\sim $\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Cola()}{8}{subsubsection.4.1.2.4}
\contentsline {subsection}{\numberline {4.1.3}Member Function Documentation}{9}{subsection.4.1.3}
\contentsline {subsubsection}{\numberline {4.1.3.1}Crear\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Nodo()}{9}{subsubsection.4.1.3.1}
\contentsline {subsubsection}{\numberline {4.1.3.2}de\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Queue()}{9}{subsubsection.4.1.3.2}
\contentsline {subsubsection}{\numberline {4.1.3.3}Destruir\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Nodo()}{9}{subsubsection.4.1.3.3}
\contentsline {subsubsection}{\numberline {4.1.3.4}en\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Queue()}{9}{subsubsection.4.1.3.4}
\contentsline {subsubsection}{\numberline {4.1.3.5}get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Nombre()}{10}{subsubsection.4.1.3.5}
\contentsline {subsubsection}{\numberline {4.1.3.6}get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Size()}{10}{subsubsection.4.1.3.6}
\contentsline {subsubsection}{\numberline {4.1.3.7}Print\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Queue()}{10}{subsubsection.4.1.3.7}
\contentsline {subsubsection}{\numberline {4.1.3.8}Vacia()}{10}{subsubsection.4.1.3.8}
\contentsline {section}{\numberline {4.2}Lista Struct Reference}{11}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Detailed Description}{11}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Member Data Documentation}{11}{subsection.4.2.2}
\contentsline {subsubsection}{\numberline {4.2.2.1}head}{11}{subsubsection.4.2.2.1}
\contentsline {subsubsection}{\numberline {4.2.2.2}nombre\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Lista}{11}{subsubsection.4.2.2.2}
\contentsline {subsubsection}{\numberline {4.2.2.3}size}{12}{subsubsection.4.2.2.3}
\contentsline {section}{\numberline {4.3}Nodo Struct Reference}{12}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Detailed Description}{12}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Member Data Documentation}{12}{subsection.4.3.2}
\contentsline {subsubsection}{\numberline {4.3.2.1}Abajo}{12}{subsubsection.4.3.2.1}
\contentsline {subsubsection}{\numberline {4.3.2.2}data}{13}{subsubsection.4.3.2.2}
\contentsline {subsubsection}{\numberline {4.3.2.3}dato}{13}{subsubsection.4.3.2.3}
\contentsline {subsubsection}{\numberline {4.3.2.4}siguiente}{13}{subsubsection.4.3.2.4}
\contentsline {section}{\numberline {4.4}Pila Class Reference}{13}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}Detailed Description}{14}{subsection.4.4.1}
\contentsline {subsection}{\numberline {4.4.2}Constructor \& Destructor Documentation}{14}{subsection.4.4.2}
\contentsline {subsubsection}{\numberline {4.4.2.1}Pila()\hspace {0.1cm}{\relax \fontsize {8}{9.5}\selectfont \abovedisplayskip 6\p@ plus2\p@ minus4\p@ \abovedisplayshortskip \z@ plus\p@ \belowdisplayshortskip 3\p@ plus\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 3\p@ plus\p@ minus\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \ttfamily [1/3]}}{14}{subsubsection.4.4.2.1}
\contentsline {subsubsection}{\numberline {4.4.2.2}Pila()\hspace {0.1cm}{\relax \fontsize {8}{9.5}\selectfont \abovedisplayskip 6\p@ plus2\p@ minus4\p@ \abovedisplayshortskip \z@ plus\p@ \belowdisplayshortskip 3\p@ plus\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 3\p@ plus\p@ minus\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \ttfamily [2/3]}}{14}{subsubsection.4.4.2.2}
\contentsline {subsubsection}{\numberline {4.4.2.3}Pila()\hspace {0.1cm}{\relax \fontsize {8}{9.5}\selectfont \abovedisplayskip 6\p@ plus2\p@ minus4\p@ \abovedisplayshortskip \z@ plus\p@ \belowdisplayshortskip 3\p@ plus\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 3\p@ plus\p@ minus\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \ttfamily [3/3]}}{14}{subsubsection.4.4.2.3}
\contentsline {subsubsection}{\numberline {4.4.2.4}$\sim $\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Pila()}{14}{subsubsection.4.4.2.4}
\contentsline {subsection}{\numberline {4.4.3}Member Function Documentation}{15}{subsection.4.4.3}
\contentsline {subsubsection}{\numberline {4.4.3.1}Crear\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Nodo()}{15}{subsubsection.4.4.3.1}
\contentsline {subsubsection}{\numberline {4.4.3.2}Destruir\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Nodo()}{15}{subsubsection.4.4.3.2}
\contentsline {subsubsection}{\numberline {4.4.3.3}get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Nombre()}{15}{subsubsection.4.4.3.3}
\contentsline {subsubsection}{\numberline {4.4.3.4}get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Size()}{15}{subsubsection.4.4.3.4}
\contentsline {subsubsection}{\numberline {4.4.3.5}Pop()}{16}{subsubsection.4.4.3.5}
\contentsline {subsubsection}{\numberline {4.4.3.6}Push()}{16}{subsubsection.4.4.3.6}
\contentsline {subsubsection}{\numberline {4.4.3.7}Vacia()}{16}{subsubsection.4.4.3.7}
\contentsline {chapter}{\numberline {5}File Documentation}{17}{chapter.5}
\contentsline {section}{\numberline {5.1}Cola.\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}h File Reference}{17}{section.5.1}
\contentsline {section}{\numberline {5.2}Funciones.\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}h File Reference}{17}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Function Documentation}{17}{subsection.5.2.1}
\contentsline {subsubsection}{\numberline {5.2.1.1}Balanceados()}{17}{subsubsection.5.2.1.1}
\contentsline {subsubsection}{\numberline {5.2.1.2}Palindromos()}{18}{subsubsection.5.2.1.2}
\contentsline {section}{\numberline {5.3}header.\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}h File Reference}{18}{section.5.3}
\contentsline {section}{\numberline {5.4}Lista.\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}h File Reference}{18}{section.5.4}
\contentsline {subsection}{\numberline {5.4.1}Typedef Documentation}{18}{subsection.5.4.1}
\contentsline {subsubsection}{\numberline {5.4.1.1}Lista}{19}{subsubsection.5.4.1.1}
\contentsline {section}{\numberline {5.5}Nodo.\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}h File Reference}{19}{section.5.5}
\contentsline {subsection}{\numberline {5.5.1}Typedef Documentation}{19}{subsection.5.5.1}
\contentsline {subsubsection}{\numberline {5.5.1.1}nodo}{19}{subsubsection.5.5.1.1}
\contentsline {section}{\numberline {5.6}Pila.\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}h File Reference}{19}{section.5.6}
