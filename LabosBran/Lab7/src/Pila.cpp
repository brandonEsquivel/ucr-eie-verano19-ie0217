/*  Pila.cpp - Autor: Brandon Esquivel Molina - B52571 - EIE UCR
    Implementacion de la clase Pila.h
    de uso academico unicamente.    */


//INCLUDES

#include "../include/Pila.h"


Pila::Pila(){
    this->size = 0;
    this->ultimo = NULL;
    cout << "\nCreando Pila..."<<endl;
}

Pila::Pila(string nombre, char i){
    this->nombrePila = nombre;
    this->size = 0;
    this->ultimo = CrearNodo(i);
    cout << "\nCreando Pila..."<<endl;
}

Pila::Pila(char i){
    this->size = 0;
    this->ultimo = CrearNodo(i);
    cout << "\nCreando Pila..."<<endl;
}

Pila::~Pila(){
    if(this->Vacia()==true){
        // do nothing
    }
    else{
        while (this->ultimo)
        {
            Nodo* ptr = this->ultimo->Abajo;
            delete this->ultimo;
            this->ultimo = ptr;
        }
        cout << "\nPila eliminada con exito." << endl;
    }
}

Nodo* Pila::CrearNodo(char i){
    Nodo* nodo = new Nodo;
    nodo->dato = i;
    nodo->Abajo = NULL;     
    return nodo;
}

void Pila::DestruirNodo(Nodo* nodo){
    delete nodo;
}

bool Pila::Vacia(){
    return (this->ultimo==NULL || this->size==0);
}

string Pila::getNombre(){
    return this->nombrePila;
}

int Pila::getSize(){
    return this->size;
}

void Pila::Push(){
    Nodo* nodo = CrearNodo('(');
    if(this->Vacia()){
        this->ultimo = nodo;
        this->size++;    
    }
    else{
        nodo->Abajo = this->ultimo;
        this->ultimo = nodo;
        this->size++;
    }
}

void Pila::Pop(){
    if(this->Vacia()){
        cout << "\nLa pila esta vacia"<< endl;
    }else{
        Nodo* ptr = this->ultimo->Abajo;
        delete this->ultimo;
        this->ultimo = ptr;
        this->size--;
    }
}