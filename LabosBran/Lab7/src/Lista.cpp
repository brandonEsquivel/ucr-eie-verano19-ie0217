//INCLUDES

#include "../include/Lista.h"


Nodo* CrearNodo(char i){

    Nodo* nodo = new Nodo;
    nodo->dato = i;
    nodo->siguiente = NULL;    
    return nodo;

}

void DestruirNodo(Nodo* nodo){
    delete nodo;
}

void InsertarPrincipio(Lista* lista, char i){

    Nodo* nodo = CrearNodo(i);
    nodo->siguiente = lista->head;
    lista->head = nodo;
}

void InsertarFinal(Lista* lista, char i){

    Nodo* nodo = CrearNodo(i);
    if(lista->head == NULL){

        lista->head = nodo;
    }else{
        Nodo* punteroAux = lista->head;
        while(punteroAux->siguiente){

          punteroAux = punteroAux->siguiente;
        }
    punteroAux->siguiente = nodo;

    }
}

void InsertarDespues(int n, Lista* lista, char i){
    Nodo* nodo = CrearNodo(i);
    if(lista->head == NULL){
        lista->head = nodo;
    }
    else{
        Nodo* punteroAux = lista->head;
        int pos = 0;
        while (pos < n && punteroAux->siguiente)
        {
            punteroAux = punteroAux->siguiente;
            pos++;
        }
        
        nodo->siguiente = punteroAux->siguiente;
        punteroAux->siguiente = nodo;

    }


}