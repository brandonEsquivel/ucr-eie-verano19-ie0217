//INCLUDES
#include "../include/Funciones.h"

//Defines

using namespace std;
void Palindromos();
void Balanceados();

int main(){                                                                                         // Inicio del main
string n;                                                                                           // variable de seleccion del usuario
cout << "\nBienvenid@.\n Que desea hacer?"<<endl;set:                                               // Menu principal y etiqueta de control de flujo
cout << "a. Probar si un string es palindromo."<<endl;
cout << "b. Probar si una cadena de parentesis esta o no balanceada."<<endl;
cout << "s. Salir"<<endl;
cin >> n;                                                                                            // Obteniendo eleccion
switch (n[0])                                                                                        // Casos
{
case 'a':                                                                                            // Probar funcion palindromos, llama a la funcion 2
    { Palindromos();}
    break;
case 'A':
    { Palindromos();}
    break;

case 'b':                                                                                            // Probar funcion balanceados, llama a funcion 1
    { Balanceados();}
    break;
case 'B':
    { Balanceados();}
    break;

case 's':                                                                                            // Salir
    break;

case 'S':
    break;

default:                                                                                            // Manejo de opcion
    cout << "\nIngrese una opcion valida, por favor."<< endl;
    goto set;                                                                                       // GOTO etiqueta, inicio del main
}

return 0;                                                                                           // fin del main
}