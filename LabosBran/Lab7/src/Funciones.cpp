//INCLUDES
#include "../include/Funciones.h"
#include "../include/Pila.h"
#include "../include/Cola.h"

//Implementacion de dos funciones de prueba para el laboratorio 7 del Curso Estructuras abstractas y Algoritmos para ingenieria, IE0217 , de EIE, UCR



void Balanceados(){                                         // Funcion de interfaz con el usuario para probar si una cadena de parentesis esta o no balanceada, usando una estructura pila (stack).
    start:                                                                                          // etiqueta de control de flujo del programa
    string cadena;                                                                                  // variable auxiliar
    Pila P;                                                                                         // declarando nueva Pila
    bool balanceado = true;                                                                         // Booleano de control de conclusion
    cout << "\nIngrese cadena de parentesis: ";                                                      
    cin >> cadena;                                                                                  // Guardando entrada
    cout << "\nTamaño de la cadena: "<< cadena.size()<<endl;                                         
    for(int i = 0; i<(int)cadena.size(); i++ ){                                                     // recorriendo cadena de entrada para acceder a cada elemento char del string
        if(cadena[i]!='(' && cadena[i] !=')' ){                                                     // Comprobando que sea un parentesis
            // cout <<"\nNo es un parentesis, saltando caracter\n";
        }
        else{
                        if(cadena[i] == '('){                                                       // Comprobando que sea parentesis de apertura
                P.Push();                                                                           // ingresando parentesis de apertura a la pila
        }
            else{                                                                                   // Si no es parentesis de apertura:

                if(P.Vacia()){                                                                      // y la pila esta vacia, entonces returna false, ya que la combinacion es desbalanceada.
                    balanceado = false;
                }
                else{
                    P.Pop();                                                                        // Si la pila no esta vacia, entonces hay que sacar un parentesis de cierre, ya que se encontro su par complementario
                }
            }
        }
        }
    if(balanceado && P.Vacia() ){                                                                   // Luego de recorrer, si la pila esta vacia y se concluyo balanceado, entonces se retorna true(esta balanceada)
        cout << "\nLa cadena tiene parentesis balanceados."<< endl;
    }else{                                                                                          // Si no implica que quedo algun parentesis sin pareja, por lo que esta desbalanceado.
        cout << "\nLa cadena tiene parentesis desbalanceados."<< endl;
    }
    cout << "Desea ingresar otra cadena? (y/n)";                                                     // Formalidad de menu
    cin >> cadena;
    if(cadena == "y" || cadena == "yes"){
    goto start;
}
// FIN
}

void Palindromos(){                                                                             // Funcion de interfaz con el usuario para probar si una cadena de parentesis esta o no balanceada, usando una estructura Cola (Queue).
    start:                                                                                      // etiqueta de control de flujo del programa
    string cadena;                                                                              // Variable auxiliar
    Cola cola("cola palindroma");                                                               // Declarando nueva cola con nombre cola Palindroma.
    string auxiliar;                                                                            // Variable auxiliar
    cout << "\nIngrese cadena: ";
    cin >> cadena;                                                                              // recibiendo entrada
    if(cadena.size()==1){                                                                       // Caso en que la cadena de entrada sea un solo caracter
        cout << "La cadena es solo un caracter."<<endl;
    }else{                                                                                      // Si es de mas de un caracter, se analiza.
        cout << "\nTamaño de la cadena: "<< cadena.size()<<endl;                                // Imprimiendo tamano
        for(int i = (int)cadena.size()-1; i>=0; i--){                                           // Se recorre la cadena de entrada en sentido contrario, para agregarlas a la cola.
            string aux(1,(cadena[i]));                                                          // Al tomar un char de la cadena, se debe convertir a string, ya que el diseno de la cola es con string, se usa el constructor de std::string que crea un string con n veces el caracter que se le pasa(en este caso el obtenido de la cadena, una vez), logrando asi tener un string igual al char
            cola.enQueue(aux);                                                                  // Agregando a la cola cada elemento
        }
        while (cola.Vacia()==false){                                                            // Mientras la cola no este vacia, se procede a sacar los elementos(deQueue)
            cola.deQueue(auxiliar);                                                             // Sacando elementos de la cola y guardando datos en string auxiliar
        }
        if(auxiliar==cadena){                                                                   // Conclusion: si al sacar la cadena en sentido contrario, esta es igual a la cadena normal, entonces es palindromo.
            cout << "\nLa cadena es un palindromo."<<endl;

        }else{
            cout << "\nLa cadena no es un palindromo"<<endl;                                    // Sino no.
        }
    }
    cout << "Desea ingresar otra cadena? (y/n)";                                                // Formalidad de menu
    cin >> cadena;
    if(cadena == "y" || cadena == "yes"){
        goto start;                                                                                 // Goto etiqueta de inicio de la funcion.
    }
}