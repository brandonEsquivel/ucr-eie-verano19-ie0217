#include <iostream>
using namespace std;

template<typename T>
class Polinomio{
	private:
/* tres constantes para ax²+bx+c y las otras para polinomio de grado 4*/
		T a;
		T b;
		T c;
		T d;
		T e;
	public:
			 /** Constructor,
 		* @brief Constructor por defecto.
		 */	
		Polinomio();
		 /** Destructor,
 		* @brief Destructor por defecto
		 */		
		~Polinomio( );
		

/** @brief Costructor, lo inicializa el usuario
 * @param a: coeficiente  que acompaña a la x²
 * @param b: coeficiente  que acompaña a la x
 * @param c: coeficiente solitario
 */		Polinomio(T a, T b, T c );


/** @brief Realiza la suma de dos fracciones, meadiante el operador +
 * @param Fraccion const @obj: resive la referencia de un objeto de tipo Fraccion
 */		void operator + (Polinomio const &obj);

/** @brief Realiza la resta  de dos fracciones, meadiante el operador -
 * @param Fraccion const @obj: resive la referencia de un objeto de tipo Fraccion
 */		void operator - (Polinomio const &obj);
		
/** @brief Realiza la multiplicación de dos fracciones, meadiante el operador *
 * @param Fraccion const @obj: resive la referencia de un objeto de tipo Fraccion
 */		void operator * (Polinomio const &obj);

/** @brief Realiza la divición de dos fracciones, meadiante el operador /
 * @param Fraccion const @obj: resive la referencia de un objeto de tipo Fraccion
 */		void operator / (Polinomio const &obj);

	/** @brief Función que sobrecarga operador de asignación.
* @param rhs parámetro constante de tipo Fracción que se pasa por referencia.
*/
 		Polinomio& operator=(const Polinomio &rhs);

  /** @brief Función para mostrar una fracción.
  */	void print();

};


//DEFAULT CONSTRUCTOR
template<typename T>
Polinomio<T>::Polinomio(){
	this->a=0;
	this->b=0;
	this->c=0;
	this->d=0;
	this->e=0;

}

template<typename T>
Polinomio<T>::~Polinomio(){


}
//CUSTOM CONSTRUCTOR
template<typename T>
Polinomio<T>::Polinomio(T a,T b, T c){
	this->a=a;
	this->b=b;
	this->c=c;
	this->d=0;
	this->e=0;
}

//SOBRECARGA DEL OPERADOR + PARA POLINOMIOS DE GRADO DOS
template<typename T>
void Polinomio<T>::operator + (Polinomio const &obj){
	
	this->a=(this->a + obj.a);
	this->b=(this->b + obj.b);
	this->c=(this->c + obj.c);
	this->d=0;
	this->e=0;

}

//SOBRECARGA DEL OPERADOR - PARA POLINOMIOS DE GRADO DOS
template<typename T>
void Polinomio<T>::operator - (Polinomio const &obj){
	
	this->a=(this->a - obj.a);
	this->b=(this->b - obj.b);
	this->c=(this->c - obj.c);
	this->d=0;
	this->e=0;
	
}

//SOBRECARGA DEL OPERADOR * PARA POLINOMIO DE GRADO DOS RESULTANDO GRADO CUATRO
template<typename T>
void Polinomio<T>::operator * (Polinomio const &obj){
	
	this->e=this->a *obj.a;
	this->d=this->a*obj.b+this->b*obj.a;
	this->a=this->a*obj.c + this->b*obj.b + this->c*obj.a;
	this->b=this->b*obj.c + this->c*obj.b;
	this->c=this->c*obj.c;
}

template<typename T>
void Polinomio<T>::operator / (Polinomio const &obj){
	
	T d[3]={this->a,this->b,this->c};
	T di[3]={obj.a,obj.b,obj.c};
	T r[3]={0,0,0};

	if(this->a==0 && obj.a!=0){
	this->a=0.0000001;
	this->b=0.0000001;
	this->c=0.0000001;
	this->d=0.0000001;
	this->e=0.0000001;
	}	//NO SE PUEDE HACER DIVISION
	
	else{//Se puede hacer la division
	T c1;
	if(obj.a!=0){//Si se ingresa divisor de grado 2
		c1=d[0]/di[0];
		this->c=c1;
		for(int j=0;j<3;j++){
			r[j]=c1*di[j];
			r[j]=d[j]-r[j];
		}

	}
	else{
		c1=d[0]/di[1];
		this->b=c1;
		for(int j=0;j<3;j++){
			r[j]=c1*di[j];
			r[j]=d[j]-r[j];
		}
		c1=r[1]/di[1];//
		this->c=c1;
		for(int j=1;j<3;j++){
			r[j]=c1*di[j];
			r[j]=d[j]-r[j];
		}

	

	}
	}

}

template<typename T>
void Polinomio<T>::print(){
	string x[5]={"x⁴","x³","x²","x",""};
	T res[5]={this->e,this->d,this->a,this->b,this->c};	//Division
	int bandera=0;//Para saber cual x es la primera.
	for(int j=0;j<5;j++){

		if(res[j]!=0){//Si coeficiente es 0 no lo imprima.
			
			if(res[j]<0){
				cout<<res[j]<<x[j];
			}else{
				
				if(bandera==0){
					cout<<res[j]<<x[j];
					bandera=1;
				
				}
				else{
					cout<<" + "<< res[j]<<x[j];
					
				
				}
			}
			
		}
	}
	cout<<endl;
		
}
template<typename T>
Polinomio<T>& Polinomio<T>::operator=(const Polinomio &rhs){

	this->a=rhs.a;
	this->b=rhs.b;
	this->c=rhs.c;
}
	
