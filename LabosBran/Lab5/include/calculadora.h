#include<iostream>

using namespace std;
template<typename T>
class Calculadora{
    
    public:
    /** @brief Constructor por defecto.*/
        Calculadora();

        /** @brief Constructor por defecto.*/
        ~Calculadora();

        /** 
         * @brief Método suma, resive dos  parametros por referencia y retorna un dato.
         * @param dato1,  primer parámetro por referencia.
         * @param dato2, segundo parámetro por referencia pero es constante.
         * @return retorna la suma de ambos parámetros.
        */
        T add(T &dato1, const T &dato2);

                /** 
         * @brief Método resta, resive dos  parametros por referencia y retorna un dato.
         * @param dato1,  primer parámetro por referencia.
         * @param dato2, segundo parámetro por referencia pero es constante.
         * @return retorna la resta de ambos parámetros.
        */
        T sub(T &dato1, const T &dato2);

                /** 
         * @brief Método multiplicación, resive dos  parametros por referencia y retorna un dato.
         * @param dato1,  primer parámetro por referencia.
         * @param dato2, segundo parámetro por referencia pero es constante.
         * @return retorna la multiplicación de ambos parámetros.
        */
        T mul(T &dato1, const T &dato2);

        /** 
         * @brief Método división, resive dos  parametros por referencia y retorna un dato.
         * @param dato1,  primer parámetro por referencia.
         * @param dato2, segundo parámetro por referencia pero es constante.
         * @return retorna la división de ambos parámetros.
        */
        T div(T &dato1, const T &dato2);

        /** Método print, imprime el dato que se le pase como parámetro.
         * @param dato1, recibe un dato como parámetro.
         * */
        void print(T &dato1);

};

template <typename T>
Calculadora<T>::Calculadora(){}

template <typename T>
Calculadora<T>::~Calculadora(){}

template <typename T>
T Calculadora<T>::add(T &dato1, const T &dato2){
    dato1=dato1+dato2; 
    return dato1;   
}


template <typename T>
T Calculadora<T>::sub(T &dato1, const T &dato2){
  
    dato1=dato1-dato2; 

    return dato1;   
}
template <typename T>
T Calculadora<T>::mul(T &dato1, const T &dato2){
    dato1=dato1*dato2; 
    return dato1;   
}
template <typename T>
T Calculadora<T>::div(T &dato1, const T &dato2){
    dato1=dato1/dato2; 
    return dato1;   
}
template <typename T>
void Calculadora<T>::print(T &dato1){
    cout<<dato1<<endl;
}

