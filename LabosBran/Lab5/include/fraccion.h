#include <iostream>
using namespace std;

/**
 * @brief Esta platilla se encarga de realizar operaciones básicas con fracciones.   
  */
template<typename T>
class Fraccion{
	private:
		T num;
		T den;
	public:
		/** @brief Constructor por defecto. */
		Fraccion();

/** @brief Constructor que lo inicializa los atributos el usuario.
 * @param num numerador
 * @param den denominador
 */    Fraccion(T num, T den);

/** @brief Realiza la suma de dos fracciones, meadiante el operador + y no retorna nada.
 * @param Fraccion const @obj: resive la referencia de un objeto de tipo Fraccion
 */    void operator + (Fraccion const &obj);

/** @brief Realiza la resta  de dos fracciones, meadiante el operador - y no retorna nada
 * @param Fraccion const @obj: resive la referencia de un objeto de tipo Fraccion
 */void operator - (Fraccion const &obj);

/** @brief Realiza la multiplicación de dos fracciones, meadiante el operador * y no retorna nada
 * @param Fraccion const @obj: resive la referencia de un objeto de tipo Fraccion
*/void operator * (Fraccion const &obj);

/** @brief Realiza la división de dos fracciones, meadiante el operador /
 * @param Fraccion const @obj: resive la referencia de un objeto de tipo Fraccion
*/ void operator / (Fraccion const &obj);

/** @brief Función que sobrecarga operador de salida o de imprimir
 * @param os, parametro de tipo ostream que se pasa por referencia.
 * @param obj, parámetro constante de tipo Fracción que se pasa por referencia.
*/


 /** @brief Función para mostrar una fracción.
  * */
 void print();

/** @brief Función que sobrecarga operador de asignación.
* @param rhs parámetro constante de tipo Fracción que se pasa por referencia.
*/
	Fraccion<T>& operator=(const Fraccion &obj);

/******************************************************************
Fraccion& operator=(const Fraccion &rhs)



/**Función que sobrecarga operador de entrada 
 */ //friend istream& operator >> (istream& in, Fraccion  &obj);
 
};

template <typename T>
Fraccion<T>::Fraccion(){
	this->num=0;
	this->den=1;
}
template <typename T>
Fraccion<T>::Fraccion(T num,T den){
	this->num=num;
	this->den=den;
}
template <typename T>
void Fraccion<T>::operator + (Fraccion const &obj){
	
	this->num =(this->num * obj.den) + (this->den * obj.num);
	this->den=(this->den * obj.den);

}
template <typename T>
void Fraccion<T>::operator - (Fraccion const &obj){
	
	this->num=(this->num * obj.den) - (this->den * obj.num);
	this->den=(this->den * obj.den);

}

template <typename T>
void Fraccion<T>::operator * (Fraccion const &obj){
	
	this->num=(this->num * obj.num);
	this->den=(this->den * obj.den);

}

template <typename T>
void Fraccion<T>::operator / (Fraccion const &obj){
	
	this->num=(this->num / obj.num);
	this->den =(this->den / obj.den);

}


template <typename T>
void  Fraccion<T>::print(){
	cout<<this->num<<"/"<<this->den<<endl;
}
template <typename T>
Fraccion<T>& Fraccion<T>::operator=(const Fraccion<T> &rhs){
	Fraccion result;
	(this->num=rhs.num);
	(this->den = rhs.den);
	//result.den=rhs.den;
	//result.num=rhs;
	return *this;

}


