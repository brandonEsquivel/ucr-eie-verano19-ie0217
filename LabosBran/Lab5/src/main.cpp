//INCLUDES

#include "../include/calculadora.h"
#include "../include/fraccion.h"
#include "../include/polinomio.h"
#include<iostream>
using namespace std;

int main(){
    //Calculadora<int> cal1();
    Calculadora<int> cal;
    int dato1=12;
    int dato2=24;
    int res;  //Para el resultado
    cout<<"La suma de "<<res <<" + "<<dato2<< " = ";
    res=cal.add(dato1,dato2);    
    cal.print(res);

    cout<<"El resultado de la suma: "<<res <<" - "<<dato2<<" = ";
    res=cal.sub(res,dato2);    
    cal.print(res);

    cout<<"El resultado de la resta: "<<res <<" * "<<dato2<<" = ";
    res=cal.mul(res,dato2);    
    cal.print(res);

    cout<<"El resultado de la multiplicacion: "<<res <<" / "<<dato2<<" = ";
    res=cal.div(res,dato2); 
    cal.print(res);

    /*********************************/
    cout<<"\n";
    Calculadora<float> cal2;
    float dato3=12.4;
    float dato4=24.7;
    float res1;  //Para el resultado

    res1=cal2.add(dato3,dato4);
    cout<<"La suma de "<<dato3 <<" + "<<dato4<< " = ";
    cal2.print(res1);

    cout<<"El resultado de la suma: "<<res1 <<" - "<<dato4<<" = ";
    res1=cal2.sub(res1,dato4);    
    cal2.print(res1);

    cout<<"El resultado de la resta: "<<res1 <<" * "<<dato4<<" = ";
    res1=cal2.mul(res1,dato4);    
    cal2.print(res1);

    cout<<"El resultado de la multiplicacion: "<<res1 <<" / "<<dato4<<" = ";
    res1=cal2.div(res1,dato4); 
    cal2.print(res1);

    Fraccion<int> f1(1,2);
    Fraccion<int> f2(3,4);
    Fraccion<int> f5;

    cout<<"\n ******Fraccion  int ***** \n ";    

    cout<<endl<<" La fraccion 1 inicial es: ";
    f1.print();

	cout<<endl<<" La fraccion 2 es: ";
    f2.print();
	cout<<endl;

    f5=f2;
    cout<<"La fraccion f5 es igual a: ";
    f5.print();
   
    f1 + f2;
    cout<<"La suma es: ";
    f1.print();

    cout<<"f1 nuevo es:  ";
    f1.print();
	f1 - f2;
    cout<<endl<<"La resta es: ";
    f1.print();


    cout<<"f1 nuevo es:  ";
    f1.print();
	f1 * f2;
    cout<<endl<<"La multiplicacion: ";
    f1.print();

    cout<<"f1 nuevo es:  ";
    f1.print();
	f1 / f2;
	cout<<endl<<"La division:";
	f1.print();	



 	

    /*******************************/
      Fraccion<float> f3(1.2,2.3);
    Fraccion<float> f4(3.4,4.5);

    cout<<"\n ******Fraccion  float  ***** \n ";    

    cout<<endl<<" La fraccion 1 inicial es: ";
    f3.print();

	cout<<endl<<" La fraccion 2 es: ";
    f4.print();
	cout<<endl;
   
    f3 + f4;
    cout<<"La suma es: ";
    f3.print();

    cout<<"f1 nuevo es:  ";
    f3.print();
	f3 - f4;
    cout<<endl<<"La resta es: ";
    f3.print();


    cout<<"f1 nuevo es:  ";
    f3.print();
	f3 * f4;
    cout<<endl<<"La multiplicacion: ";
    f3.print();

    cout<<"f1 nuevo es:  ";
    f3.print();
	f3 / f4;
	cout<<endl<<"La division:";
	f3.print();		
			
		/**********************************************/
	cout<<"\n ******Polinomio ***** \n "; 
	Polinomio<int> p1(1,2,3);
    Polinomio<int> p2(2,3,4);
    Polinomio<int> p5;

    cout<<"El polinomio 1 es: ";
    p1.print();
    cout<<endl;

    cout<<"El polinomio 2 es: ";
    p2.print();
    cout<<endl;

    cout<<"Asignacion"<<endl;
    cout<<"f5 = ";p5.print();
    
    p5=p2;
    cout<<"f5 = f2 "<<endl;
	cout<<"El polinomio f5 es: "; p5.print();
    cout<<endl;

	p1 + p2;
    cout<<endl<<"La suma es: "; p1.print();

    cout<<"P1 nuevo: "; p1.print();
	p1 - p2;
    cout<<endl<<"La resta es: ";p1.print();

    cout<<"P1 nuevo: "; p1.print();
	p1 * p2;
    cout<<endl<<"La multiplicacion: "; p1.print();

    cout<<"P1 nuevo: "; p1.print();
	p1 / p2;
    cout<<endl<<"La division: ";p1.print();
	
					
            		
		
	
			
			
			
		
	



   
/*
    int c=0;
	bool salir =false;
	while(salir!=true){
		cout<<endl<<"***********"<<endl;
		cout<<" Opcion:"<<endl;///
		cout<<"1 Fracción"<<endl;
		cout<<"2 Polinomio"<<endl;
		cout<<"3 Salir"<<endl;
		cin >>c;
	switch(c){
		case 1:
			{
			Fraccion f1;
			cin >>f1; 
			cout<<endl<<" La fraccion 1 que ingresó es: "<<f1<<endl;

			Fraccion f2;
			cin >>f2; 

			cout<<endl<<" La fraccion 2 que ingresó es: "<<f2<<endl;
	
			Fraccion f3=f1 + f2;
			Fraccion f4=f1 - f2;

			Fraccion f5=f1 * f2;
			Fraccion f6=f1 / f2;
	
			cout<<endl<<"La suma es:"<<endl;
			cout<<f3<<endl;
			cout<<endl<<"La resta es: "<<endl;
			cout<<f4<<endl;
	
			cout<<endl<<"La multiplicación: "<<endl;
			cout<<f5<<endl;
			cout<<endl<<"La division:"<<endl;
			cout<<f6<<endl;
			}
	
			break;
		case 2:
	
			{


			cout<<endl<<" ********Polinomio*******"<<endl;	
			Polinomio p1;
			cin >>p1; 
			cout<<endl<<" El polinomio 1 que ingresó es: "<<p1<<endl;
	
			Polinomio p2;
			cin >>p2; 
			cout<<endl<<" El polinomio 2 que ingresó es: "<<p2<<endl;	
			Polinomio p3=p1 + p2;
			Polinomio p4=p1 - p2;

			Polinomio p5=p1 * p2;
			Polinomio p6=p1 / p2;
	
			cout<<endl<<"La suma es:"<<endl;
			cout<<p3<<endl;
			cout<<endl<<"La resta es: "<<endl;
			cout<<p4<<endl;
	
			cout<<endl<<"La multiplicación: "<<endl;
			cout<<p5<<endl;
			cout<<endl<<"La division:"<<endl;
			cout<<p6<<endl;
			}
			break;
		case 3: 
			salir =true;
	}
	}*/

 
    return 0;
}