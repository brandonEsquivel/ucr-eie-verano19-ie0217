var searchData=
[
  ['operator_2a',['operator*',['../classFraccion.html#a166d8cff649ccb79d66db493d3ae595c',1,'Fraccion::operator*()'],['../classPolinomio.html#a1e60577c344cd82cfb7b2dd311027111',1,'Polinomio::operator*()']]],
  ['operator_2b',['operator+',['../classFraccion.html#ad649ab236590874419d754cf64b9c51a',1,'Fraccion::operator+()'],['../classPolinomio.html#ac3d7ab6bdc592f66a5d14f89f44cfcc4',1,'Polinomio::operator+()']]],
  ['operator_2d',['operator-',['../classFraccion.html#a941c11defce0cdb7d5c6736bad8d9fdf',1,'Fraccion::operator-()'],['../classPolinomio.html#aea95db50e5ce38ddfac07c0964d54f9e',1,'Polinomio::operator-()']]],
  ['operator_2f',['operator/',['../classFraccion.html#a9880f42fea3a932ce736bb51e1e4aa44',1,'Fraccion::operator/()'],['../classPolinomio.html#a3729c686fc869a6d424cbafc3c5929fe',1,'Polinomio::operator/()']]],
  ['operator_3d',['operator=',['../classFraccion.html#a78d25d16f89642d94e12529cbf85d72e',1,'Fraccion::operator=()'],['../classPolinomio.html#add1d4ce5ee7d016d62531498b6b0fd09',1,'Polinomio::operator=()']]]
];
