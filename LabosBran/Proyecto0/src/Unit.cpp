
//INCLUDES

#include "../include/Unit.h"

//DEFINES
using namespace std;

//Metodos

Unit::Unit(){
    
}

Unit::~Unit(){
  
}

int Unit::getATK(){
    return this->attack;
}

int Unit::getDEF(){
    return this->defense;
}

int Unit::getEXP(){
    return this->experience;
}

int Unit::getHP(){
    return this->hitPoints;
}

int Unit::getHPM(){
    return this->maxHitPoints;
}

int Unit::getID(){
    return this->id;
}

int Unit::getLevel(){
    return this->level;
}

int Unit::getMovement(){
    return this->movement;
}

string Unit::getNAME(){
    return this->name;
}

int Unit::getPosX(){
    return this->posX;
}

int Unit::getPosY(){
    return this->posY;
}

int Unit::getRange(){
    return this->range;
}

char Unit::getType(){
    return this->type;
}

string Unit::getJug(){
    return this->jugador;
}

// SETS ****************

void Unit::setATK(int atkk){
    this->attack = atkk;
}

void Unit::setDEF(int deff){
    this->defense = deff;
}

void Unit::setEXP(int expp, char m){
    if (m =='m')
    {cout << "Subiendo experiencia a " << this->getNAME()<<endl;
        this->experience++;
        this->subirNivel();
    }
    else{
    this->experience = expp;
}
}

bool Unit::subirNivel(){
    if(this->experience >= this->level*2){
        this->level++;
        cout << this->getNAME() << " ( " << this->getType() << " )  Subio al Nivel " << this->level << "!!" << endl;
        this->attack = this->attack*1.25;
        this->defense = this->defense*1.25;
        this->experience = 0;
        return true;
    }
    else
    {
        return false;
    }
    
}

void Unit::setHP(int hp){
    this->hitPoints = hp;
}

void Unit::lessHP(int hp){
    this->hitPoints = this->hitPoints - hp;
}

void Unit::setHPM(int HPm){
    this->maxHitPoints = HPm;
}

void Unit::setID(int id){
    this->id = id;
}

void Unit::setLevel(int lv){
    this->level = lv;
}

void Unit::setMovement(int mv){
    this->movement = mv;
}

void Unit::setNAME(string n){
    this->name = n;
}

void Unit::setPosX(int x){
    this->posX = x;
}

void Unit::setPosY(int y){
    this->posY = y;
}

void Unit::setRange(int rg){
    this->range = rg;
}

void Unit::setType(char ty){
    this->type = ty;
}

void Unit::setJug(string nombre){
    this->jugador = nombre;
}

bool Unit::Attack(Unit *ref, int x, int y){
    return true;
}

bool Unit::Move(Cell *ref, int x, int y){
     return true;
 }