//INCLUDES
#include "../include/Field.h"

//DEFINES
using namespace std;

//Metodos

//Constructor
Field::Field(){
    this->cows=10;
    this->rows=9;
}

//Destructor
Field::~Field(){

}
//Guarda celda en vector;
void Field::tabInicial(){
    cout<<"Creando Tablero..."<<endl;
    int cow=getCow();                                               // Obteniendo columnas y filas del tablero
    int row=getRow();
    for(int i=0;i<cow;i++){
        for(int j=0;j<row;j++){
           if(i<1){                                          // Comprobar borde superior
               playfield[i][j]=new Cell(false);
           }
           else if(i==(getCow()-1)){                         // Comprobar borde inferior
               playfield[i][j]=new Cell(false);
           }
           //Extemos
           else if((j==0 && i!=0) ||((i!=(getCow()-1) && j==(getRow()-1) ))){
              playfield[i][j]=new Cell(false);
            }
            //Caso contrario
            else {
                playfield[i][j]=new Cell(true); 
            }
        }
    }
    cout<<"\n \n";
}

//Retorna el valor de cows.
int Field::getCow(){
    return  this->cows;
}

//Retorna el valor de rows.
int Field::getRow(){
    return   this->rows;
}

void Field::printTab(){
    for(int i=0;i<getCow();i++){
        for(int j=0;j<getRow();j++){ 
            if(playfield[i][j]->getPassable()==false){                                                   // Si es pasable
                cout<<"\t X ";
            }
            else{
                if(playfield[i][j]->getOcuped()==false){ 
                    cout<<"\t"<<i<<j;}
                else{
                    cout<<"\t"<<playfield[i][j]->getArmy()->getNAME()<<"("<<playfield[i][j]->getArmy()->getJug()<<")";
                }
            }              
        }
        cout<<"\n";
    }
}

Cell* Field::getCell(int cow, int row){
    return playfield[cow][row];
}

void Field::Guardar(){
    fstream archivo("Juego.txt");
    if(!archivo.is_open()){// si no esta abierto, se abre
         archivo.open("Juego.txt",ios::out);
         //escribir
         archivo.close();
    }
}

bool Field::Cargar(){
    return false;
}

void Field::RefreshCelda(Field &field,int x,int y){
    field.playfield[x][y] = new Cell(true);
}