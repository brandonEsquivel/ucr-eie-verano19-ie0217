/* Implementacion de la clase Player.h, que contiene la informacion de cada jugador.
Autor: Brandon Esquivel Molina, Reinier Camacho.
UCR, EIE
*/

//INCLUDES

#include "../include/Player.h"
#include "../include/Field.h"

//DEFINES
using namespace std;
class Field;                                                              // declaracion adelantada de Field

//METODOS

Player::Player(){                                                        // Constructor
  this->setGuardar(false);
}

Player::~Player(){                                                      // Destructor
  // do nothing
}

Player::Player(string nam){                                            // Custom constructor
  this->name = nam;
  this->setGuardar(false);
}

Unit* Player::getArmy(int pos){                                        // autoexplicado
  return army[pos]; 
}

string Player::getNombre(){                                            // autoexplicado
  return this->name;
}

void Player::setNombre(string b){                                     // autoexplicado
  this->name = b;
}

void Player::colocarUnits(Field &field){                             // Metodo para colocar las unidades en el campo de batalla
  int x;                                                             // Variables auxiliares                                          
  int y;
  for (int j = 0; j < (int)army.size(); j++)                         // Recorrer vector de unidades para establecerlas.
  {
    x=army[j]->getPosX();                                            // Se obtiene la posicion X,Y de la unidad
    y=army[j]->getPosY();
    field.getCell(x,y)->setOcuped(true);                             // Seteando parametros de la celda
    field.getCell(x,y)->setArmy(army[j]);
  }
  field.printTab();                                                  // Imprimiendo tablero para mayor claridad
}

void Player::Mover(Field &field){                                    // Metodo para mover las unidades, por turno
  cout<<"----------------------------------------Mover----------------------------------------"<<endl;
  char n;                                                           // Variables auxiliares 
  int x, y;
  string t,XS,XC;
  bool salir=false;
            
  //INICIANDO RECORRIDO POR TODAS LAS UNIDADES DEL JUGADOR
   
  for(int j=0;j<(int)army.size();j++){                                // recorriendo vector de unidades del jugador
    field.printTab();                                                 // Imprimiendo tablero para mayor claridad
    moverr:                                                           // Cada vez que se llama una unidad, se muestra el tablero.
    cout<<"Unit "<<army[j]->getNAME() <<"("<<j<<")"<<endl;            // Imprimiendo nombre y numero de la unidad
    cout<<"mover ?"<<endl;                                            // Inicia seleccion de movimiento
    cin>>t;                                                           // Dato de entrada, seleccion del usuario
    n=t[0];                                                           // Variable de eleccion del usuario
    switch (n){
      case 'q':
        cout << "Desea salir y/o guardar? (yn/yy/n)";                 // Comprobacion de salir
        cin >>t;                                                      // Dato de entrada, seleccion del usuario
        if(t=="yn"){
          cout<<"\nSaliendo del juego\n";                             // Salir sin guardar, con exit(1)
          exit(1);
        }
        if(t=="yy"){                                                  // Salir y guardar partida
          this->setGuardar(true);                                     // Set guardar como true para llamada posterior a guardar
          break;
        }
        else{
          goto moverr;                                                // Goto etiqueta de control de flujo. 
        }
      case 'n':                                                       // Caso (n)o mover, saliendo.
        break; 
      case 'y':   {                                                   // Caso (y)es, preguntar coordenadas de movimiento
        movewhere:                                                    // Etiqueta de control de salto del programa
        while(salir==false){
          cout<<"Donde? "<<endl;                                      // obteniendo coordenadas de movimiento
          cout<< "x:  ";
          cin >> XS;
          if(XS == "-1"){                                             // Corriguiendo pequeno Bug
            goto verificarY;
          }
          XC = XS[0];                                                 // Obteniendo primer char del string de entrada para comprobar que sea valido
          if(isalpha(XS[0])==0){                                      // Es Numero-> valido
              x = stoi(XC, nullptr, 10);
              salir=true;
          }
          else{
            cout<<"\nVuelva a ingresar NUMERO de coordenada X."<<"\n";
            salir=false;             
          }
        }
        salir =false;                                                  // Mismo procedimiento con la coordenada Y
        while(salir==false){
          verificarY:
          cout << "y: ";
          cin >> XS;
          if(XS=="-1"){ 
            goto menosmenos;
          }
          XC = XS[0];
          if(isalpha(XS[0])==0){
            y = stoi(XC, nullptr, 10);
            salir=true;
          }
          else{
            cout<<"\nVuelva a ingresar NUMERO de coordenada Y."<<"\n";
            salir=false;                
          }
        }
        salir =false;
        if(x == -1 && y==-1){      
          menosmenos:                                                                                                 // Comprobando si se ingresa caso especial (-1,-1) para dejar la unidad quieta
          cout << " se eligio (-1,-1) dejando la unidad en la misma posicion..." << endl;
          break;                                                                                                      // saliendo
        }
        else if((x < 1 || x > field.getRow()) || (y < 1 || y > field.getCow()) ){                                     // Comprobar validez sobre el tablero
          cout << "No se ingresaron coordenadas del tablero validas, por favor verifique." << endl; 
          goto movewhere;                                                                                             // Regresando a ingresar coordeadas de movimiento
        }
        else{                                                                                                         // Obteniedo posicion Y actual 
          bool mover = army[j]->Move(field.getCell(army[j]->getPosX(), army[j]->getPosY()), x,y);                     // Estoy obteniendo una celda del vector de celdas de la clase Field en la que esta la unit actualmente y llamo a move que devuelve true o false si puede o no moverse */ 
          if(mover==true){                                                                                            // si move devuelve true, si se puede mover
            if(field.getCell(x,y)->getOcuped()==false ){                                                              // Revisan si esta ocupada o no
              field.RefreshCelda(field, army[j]->getPosX(), army[j]->getPosY());                                      // Refrescando celda
              army[j]->setPosX(x);                                                                                    // Redefiniendo coordenadas de posicion de la Unidad recien movida
              army[j]->setPosY(y);
              field.getCell(x,y)->setArmy(army[j]);                                                                   // Colocando unidad en la celda
              field.getCell(x,y)->setOcuped(true);                                                                    // Indicando que la nueva celda esta ocupada 
              break;
            }
            else{
              cout<<"La posicion esta ocupada por una unidad. Por favor verifique."<<endl;
              goto movewhere;                                                               // Volviendo a ingresar coodenadas
            } 
          }
          else{                                                                             // si Move devuelve False
            cout<<"La unidad no puede desplazarse a esa posicion. Por favor verifique."<<endl;
            goto movewhere;                                                                 // Volviendo a ingresar coodenadas
          }
        }
      }
    default:                                                                               // Caso default, regresa a elegir cordenadas 
      cout<<"La unidad no puede desplazarse a esa posicion. Por favor verifique. Si no tiene movimientos disponibles, indique (-1,-1)."<<endl;
      goto movewhere;
    }                                                                                        // fin scope de la estrutura switch
  }
}

void Player::Atacar(Field &field){                                                           // Metodo para Atacar con las unidades, por turno
  cout<<"---------------------------------------Atacar---------------------------------------"<<endl;
  char n;                                                                                   // Variables auxiliares
  int x;
  int y;
  string t,XS,XC;
  bool salir = false;          
//INICIANDO RECORRIDO POR TODAS LAS UNIDADES DEL JUGADOR
  for(int j=0;j<(int)army.size();j++){ 
    atackk:                                                                             // Recorriendo vector de unidades del jugador
    field.printTab();                                                                   // Cada vez que se llama una unidad, se muestra el tablero.
    cout<<"Unit "<<army[j]->getNAME() <<"("<<j<<")"<<endl;                              // Identificando unidad
    cout<<"Attack ?"<<endl;
    cin>>t;                                                                             // Dato de entrada
    n=t[0];                                                                             // Tomando primer caracter del string de entrada 
    switch (n){
      case 'q':
        cout << "Desea salir y/o guardar? (yn/yy/n)";
        cin >>t;
        if(t=="yn"){
          cout<<"\nSaliendo del juego\n";
          exit(1);
        }
        if(t=="yy"){
        this->setGuardar(true);
        break;
        }
        else{
        goto atackk;
        }                                                            // Saliendo si elige (n)o
              
      case 'n':                                                      // Saliendo si elige (n)o
        break; 
      case 'y':{                                                     // Preguntando a donde para verificar
        attackwhere:                                                 // Etiqueta de control de flujo del programa
        while(salir==false){
        cout<<"Donde? "<<endl;                                       // obteniendo coordenadas de movimiento
        cout<< "x:  ";
        cin >> XS;                                                   // Entrada, mismo procedimiento que en move
        if(XS == "-1"){                                              // Caso especial para salir de la instancia de eleccion
          goto verificarY;
        }
        XC=XS[0];
        if(isalpha(XS[0])==0){                                        // si es Numero
          x = stoi(XC, nullptr, 10);
          salir=true;  
        }
        else{
          cout<<"\nVuelva a ingresar NUMERO de coordenada X."<<"\n";
          salir=false;             
        }
      }
        salir =false;
        while(salir==false){                                                      // Mismo procedimiento
          verificarY:
          cout << "y: ";
          cin >> XS;
          if(XS=="-1"){
            goto menosmenos;
          }
          XC=XS[0];
          if(isalpha(XS[0])==0){                                                    // Verificar que es Numero
            y = stoi(XC, nullptr, 10);
            salir=true;
          }
          else{
            cout<<"\nVuelva a ingresar NUMERO de coordenada Y."<<"\n";
            salir=false;                
          }
        }
        salir =false;
        if(x == -1 && y==-1){   
          menosmenos:                                                                         // Comprobando si se ingresa caso especial (-1,-1) para dejar la unidad quieta
          cout << "Se eligio (-1,-1) dejando la unidad en la misma posicion..." << endl;
          break;                                                                              // saliendo
        }
        if(x < 0 || x > field.getRow() || y < 0 || y > field.getCow() ){                      // Comprobar validez sobre el tablero
          cout << "No se ingresaron coordenadas del tablero validas, por favor verifique." << endl; 
          goto attackwhere;                                                                   // Regresando a ingresar coordenadas de Ataque
        }
        if(field.getCell(x,y)->getOcuped()==false){
          cout <<"No hay blanco para atacar en esa posicion. Verifique."<< endl;
          goto attackwhere;
        }
        bool atacar = army[j]->Attack(army[j], x, y);                                         // Llamando a metodo ATTACK de la Unidad. Ingresando unidad misma como puntero
        if(atacar==true){                                                                     // si Attack devuelve true, si se puede atacar                               
          field.getCell(x,y)->getArmy()->lessHP( army[j]->getATK() - field.getCell(x,y)->getArmy()->getDEF() );         // attack - def y eso se mete en lessHP de la unidad atacada, disminuyendo su HP
          if( field.getCell(x,y)->getArmy()->getHP() <= 0){                                                             // Comprobando si la unidad atacada HA MUERTO!
            cout<< "La unidad " << army[j]->getNAME() << " ha vencido a la unidad "<< field.getCell(x,y)->getArmy()->getNAME() << "!!!!!" << endl;
            army[j]->setEXP(1,'m');                                                                                     // Sumando experiencia ganada, Incluye llamado a metodo para revisar experiencia y subir de nivel. Implementado  en clase Unit
            field.RefreshCelda(field, x, y);                                                                            // Refrescando celda                                                                                                                                           
            break;
          } 
          break;
        } 
        else{                                                                                   // si Attack devuelve False, osea no es posible atacar en esa posicion
          cout<<"La unidad no puede atacar a esa posicion. Por favor verifique."<<endl;
          goto attackwhere;                                                       // Volviendo a ingresar coodenadas
        }
      }
      default:{
        cout <<"\nEntrada incorrecta. Verifique.\n";
        goto attackwhere;
      }
    }
  }
}

bool Player::createArmy(int lancer ,int caval ,int arch, Field &field){       // Crea las unidades y las almacena en el vector de unidades del jugador
  int con=0;                                                                  // Contador de caballos, lanceros y de arqueros para ingresar: 0,1,2,,,,
  int sum=lancer+caval+arch;                                                  // Suma total de costos por unidades
  if (sum<=Player::maxCost){                                              // Condicion aprobada para crear unidades                                                    // Inicalizar vector
    string nombre;                                                        // Variables auxiliares para la creacion de unidades                                                              
    for (int j=0;j<lancer;j++){                                           // Se recorre la cantidad propuesta de cada unidad creandolas
      cout <<" \n Ingrese nombre lancero (" <<j<<"): " << endl;           // Lanceros
      cin >> nombre;                                                      // var auxiliar
      army.push_back(new Lancero(nombre, this->getNombre()));             // Custom constructor con nombre
      Player::ColocarUnit(j, field);                                      // Llamado anidado a colocar unidades  
    }    
    for (int j=lancer;j<(lancer+caval);j++){
      cout <<" \n Ingrese nombre Caballo (" <<con<<"): " << endl;         // Caballos
      cin >> nombre;                                                      // var auxiliar
      army.push_back(new Caballo(nombre, this->getNombre()));             // Custom constructor con nombre 
      Player::ColocarUnit(j, field);                                      // Llamado anidado a colocar unidades     
      con++;                                                              // Aumenta contador de caballos
    }
    con=0;                                                                // Se resetea del contador
    for (int j=(lancer+caval);j<sum;j++){
      cout <<"\n Ingrese nombre Arquero(" <<con<<"):\n " << endl;         // Arqueros
      cin >> nombre;                                                      // var auxiliar
      army.push_back(new Arquero(nombre, this->getNombre()));             // Custom constructor con nombre
      Player::ColocarUnit(j, field);                                      // Llamado anidado a colocar unidades
      con++;
    }
    return true;                                                            // Creacion de Army completada
  }
  else{                                                                     // Condicion incorrecta
    cout<< "Sobrepasó número máximo de unidades: " << Player::maxCost << "\nReingrese y verifique\n" << endl; 
    return false;                                                         // Indica que no se crearon correctamente las unidades    
  }
}

void Player::ColocarUnit(int j, Field &field){                                 // Ingresar unidades en una poscision del tablero
  field.printTab();                                                            // Muestra el tablero para saber donde colocar las unidades.
  int x;                                                                       // Variables auxiliares para la colocacion de unidades
  int y;
  bool salir = false;
  string nombre,XS,YS,XC,YC;
  xy:                                                                          // Etiqueta de control de flujo del programa
  while(salir==false){
    cout << "Donde colocar a " << army[j]->getNAME() << "?" <<endl;      
    cout << "x: "<<endl;                                                       // Ingresar coordenadas
    cin>>XS;
    XC=XS[0];                                                                  // Verifica si es numero, mismo procedimiento
    if(isalpha(XS[0])==0){//Numero
      cout << XC<<endl;
      x = stoi(XC, nullptr, 10);
      salir=true;
    }
    else{
      cout<<"\nVuelva a ingresar NUMERO de COORDENADA X."<<"\n";
      salir=false;                 
    }
  }
  salir =false;                                                                 // Mismo procedimiento con Y
  while(salir==false){
    cout << "y: "<<endl;
    cin>>YS;
    YC=YS[0];
    if(isalpha(YS[0])==0){                                                      // Verifica si es Numero
      cout << YC<<endl;
      y = stoi(YC, nullptr, 10);
      salir=true;
    }
    else{
      cout<<"\nVuelva a ingresar NUMERO de COORDENADA Y."<<"\n";
      salir=false;                 
    }
  }
  salir = false;
  if(x < 0 || y < 0 || x > field.getRow() || y > field.getCow() ){                     // OUT OF BOUNDS
    cout << "Coordenadas invalidas, reingrese" << endl;
    goto xy;                                                                          // Retornar a etiqueta
  }
  else{                                                                 // Primer nivel de comprobacion
    if( field.getCell(x,y)->getPassable()==true ){                       // Segundo nivel de comprobacion
      if(field.getCell(x,y)->getOcuped()==false){                        // tercer nivel de comprobacion
        cout << "Posicion correcta. Colocando Unidad"<< endl;
        army[j]->setPosX(x);                                           // Seteando posicion de la unidad
        army[j]->setPosY(y);
        field.getCell(x,y)->setOcuped(true);                             // Seteando parametros de la celda
        field.getCell(x,y)->setArmy(army[j]); 
      }
      else{
        cout << "Coordenadas invalidas, reingrese" << endl;           // error de coordenadas, volviendo a etiqueta
        goto xy;
      } 
    }
    else{
      cout << "Coordenadas invalidas, reingrese" << endl;
      goto xy;
    }
  }
}

bool Player::play(Field &field){                                                                  // Mantiene la logica del juego por turnos.
  string nom;
  cout << "Turno de " << this->name<<endl;                                
  for(int unsigned j=0;j<army.size();j++){                                                           // Recorriendo vector de unidades comprobando su salud (HP)
    if(army[j]->getHP() <= 0 ){
      cout << "Eliminando  bajas :C " << endl;    
      cout << "Adios " << army[j]->getNAME() << "..." <<endl;
      field.RefreshCelda(field, army[j]->getPosX(), army[j]->getPosY());                                   //  redefiniendo valores de la celda                    
      army.erase(army.begin()+j);                                                                   // Eliminado Unidad
      j--;                                                                                          // Corrigiendo caso de extremo y evitar segm fault  
    }
  }    
  if(army.size()==0){ 
    return true; //PARA TERMINAR JUEGO
  }
  else{
    whattodo:
    cout << "Qué desea hacer?"<<endl;
    cout << "\np: mover y atacar. \t i: info unid. \t u: info unidad espcif \t q: Guardar y Salir"<<endl;
    char n;
    string X;
    cin >> X;
    n =X[0];
    switch (n){
      case 'p':{
        Mover(field);
        if(this->getGuardar()==true){
          break;
        }
        Atacar(field);
        break;
      }
      case 'i':{
        MostrarInfoTodasUnidades(field);
        goto whattodo;
      }   
    case 'u':{
      string name;                                                  // Lleva el mismo flujo que los menus anteriores, solo que llama a su respectiva funcion
      cout << "Ingrese nombre de la unidad a buscar: "<<endl;
      cin >> name;
      MostrarInfoUnidad(field,name);
      goto whattodo;
    }
    case 'q':{                                                 // Caso para quitar el juego en funcionamiento
      cout<<"¿Deseas SALIR del juego y guardar?  (n/y)";
      cin >> X;
      n =X[0]; 
      switch(n){
        case 'n':                                             // si no quiere salir del juego.
          goto whattodo;
        case 'y':{
          setGuardar(true);
          goto salswit;
        }
        break;
      }
    }  
    default:
      cout << "No ingreso opcion validad. Verifique."<<endl;
      goto whattodo;
    }
    salswit:                                             // Etiqueta para salir
    return false;
  }
}

void Player::MostrarInfoTodasUnidades(Field &field){
  cout << "*************** ESTADO ACTUAL DE UNIDADES ***************" << endl;
  for(int j=0;j<(int)army.size();j++){
    cout << "**** Unidad: "<<army[j]->getNAME()<< "("<<army[j]->getType()<<")*******"<<endl;
    cout << "**** Salud actual: "<< army[j]->getHP()<< "          *******"<<endl;
    cout << "**** Nivel: "<< army[j]->getLevel()<< " Posicion: "<<army[j]->getPosX()<<","<<army[j]->getPosY()<<"***"<<endl;           
  }
}

void Player::MostrarInfoUnidad(Field &field, string name){
  bool find = false;
  for(int j=0;j<(int)army.size();j++){
    if(army[j]->getNAME()==name){
      cout << "**** Unidad: "<<army[j]->getNAME()<< "("<<army[j]->getType()<<")*******"<<endl;
      cout << "**** Salud actual: "<< army[j]->getHP()<< "          *******"<<endl;
      cout << "**** Nivel: "<< army[j]->getLevel()<< " Posicion: "<<army[j]->getPosX()<<","<<army[j]->getPosY()<<"***"<<endl;           
      find = true;
      break;
    }
  }
  if(find!=true){
    cout << "No se encontró la Unidad. Verifique el nombre"<<endl;
  }
}

vector<Unit*> Player::getArmy2(){
  return (army);
}

bool Player::getGuardar(){
  return guardar;
}

void Player::setGuardar(bool guardar1){
  this->guardar=guardar1;
}

void Player::setVector(vector<Unit*> ref){
  this->army=ref;
}