//INCLUDES

#include "../include/UI.h"


//DEFINES

using namespace std;
void Juego();

int main(){
    //INICIALIZANDO ELEMENTOS
    Field tablero;                                      // INICIALIZANDO TABLERO
    Player hellbrain;                                   // INICIALIZANDO Jugador 1
    Player Nostra;                                      // INICIALIZANDO Jugador 2
    hellbrain.setGuardar(false);
    Nostra.setGuardar(false);
    bool val;
   
    tablero.tabInicial();                                 // Iniciando tablero
    UI Partidas;                                          // Iniciando objeto UI de control
    string info;
    cout<<"\nBinevenid@!.\nVer información General del juego (y/n) \n";
    cin>>info;
    if(info[0]=='y'){
       Partidas.MostrarInfoJuego(); 
    }
    val = Partidas.MenuPrincipal(tablero, hellbrain, Nostra);                            // Menu principal - devuelve true si se cargo un juego o false si se va iniciar uno nuevo
    if(val!=true){
        Partidas.NuevaPartida(tablero, hellbrain, Nostra);
    }
    Partidas.Juego(tablero, hellbrain, Nostra);
    cout << "\n\n\tFinalizando..."<<endl;
    return 0;
}