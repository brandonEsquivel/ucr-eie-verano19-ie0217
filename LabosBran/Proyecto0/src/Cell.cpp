//INCLUDES

#include "../include/Cell.h"

//DEFINES
using namespace std;

//Metodos

// Constructor
Cell::Cell(){
    this->ocupada=false;
}

// Constructor Custom
Cell::Cell(bool pass){
    this->passable = pass;
    this->ocupada=false;
}

//Destructor
Cell::~Cell(){
    //Do nothing
}

//Set passable
void Cell::setPassable(bool p){
    this->passable = p;
    this->ocupada=false;
}

//Get passable
bool Cell::getPassable(){
    return this->passable;
}

void Cell::setArmy(Unit *unidad){
    this->army = unidad;
}

Unit* Cell::getArmy(){
    return this->army;
}

void Cell::setOcuped(bool oc){
    this->ocupada = oc;
}
 
bool Cell::getOcuped(){
    return this->ocupada;
}

void Cell::printCellInfo(){
    bool pass = this->getPassable();
if(pass==true){
    Unit *unidad = this->getArmy();
    cout <<"\n******Info de la celda *******"<<endl;
    cout <<"*                            *"<<endl;
    cout <<"* Flanqueble: Si              *"<<endl;
    cout <<"* Unidad: "<<unidad->getType()<<"           *"<<endl;
    cout <<"* Jugador: "<<unidad->getJug()<<"    *"<<endl;
    cout <<"******************************"<<endl;
    }
else{
    cout <<"\n******Info de la celda *******"<<endl;
    cout <<"*                            *"<<endl;
    cout <<"* Flanqueble: NO             *"<<endl;
    cout <<"* Unidad: NULL               *"<<endl;
    cout <<"******************************"<<endl;
    }
}