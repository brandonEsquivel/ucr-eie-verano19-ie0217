//INCLUDES

#include "../include/Lancero.h"

//DEFINES
using namespace std;

//Metodos

Lancero::Lancero(){
//Constructor
this->setHPM(20);
this->setHP(20);
this->setATK(7);
this->setDEF(7);
this->setEXP(0,'s');
this->setID(0);
this->setLevel(1);
this->setMovement(1);
this->setNAME("NULL");
this->setRange(1);
this->setType('L');
}

Lancero::Lancero(string name, string jugador){
//Constructor
this->setHPM(20);
this->setHP(20);
this->setATK(7);
this->setDEF(7);
this->setEXP(0,'s');
this->setID(0);
this->setLevel(1);
this->setMovement(1);
this->setNAME("NULL");
this->setRange(1);
this->setType('L');
this->setNAME(name);
this->setJug(jugador);
}

int Lancero::getCost(){
    return cost;
}

Lancero::~Lancero(){
}

bool Lancero::Move(Cell *ref, int x, int y){
    //Revisando Posibilidades de avance
    int mover = ref->getArmy()->getMovement();        // Obtengo cantidad posible de movimiento
    int posx = ref->getArmy()->getPosX();             // Obtengo posiciones (x,y) actuales del unit
    int posy = ref->getArmy()->getPosY();
    cout << "Capacidad de movimiento: " << mover << endl;
    // posibles movimientos prohibidos del lancero
    if(     (   (x == posx + mover || x==posx-mover)    && y==posy )    || (    (y == posy+mover || y==posy-mover)   && x ==posx )  ){
       cout<<"Ubicacion correcta.."<<endl;
        return true;                                 // Si es una coordenada valida
    }
    else{   
        return false;
    }   
}

bool Lancero::Attack(Unit *ref, int x, int y){
    int alcance = ref->getRange();           // Obtengo rango de alcance del unit
    int posx = ref->getPosX();               // Obtengo posiciones (x,y) actuales del unit
    int posy = ref->getPosY();
    if( ( (x==posx+alcance || x==posx-alcance) &&  y==posy) ||  ( (y==posy+alcance || y==posy-alcance) &&  x==posx)){
        cout << "Rango de ataque Correcto"<< endl;
        return true;                        // Si es una coordenada valida
    }
    else{
       cout << "Rango de ataque No Adecuado."<< endl;
       return false;                       // Si no
    }
}