//INCLUDES
#include "../include/UI.h"

//DEFINES
using namespace std;

//Metodos

// Constructor
UI::UI(){}
// Destructor
UI::~UI(){}

void UI::guardarJ(Player ref, Player ref2){
    string nombreTXT;
    cout<< "Ingrese nombre de la partida a guardar: "<<endl;
    cin >> nombreTXT;
    nombreTXT = nombreTXT+".txt";
    ofstream archivo, archivoPartidas;
    archivo.open(nombreTXT,ios::out);                               // se sobreescribe
    archivoPartidas.open("Partidas.txt", ios::out | ios::app);
    if(archivo.fail() || archivoPartidas.fail()){                                             //   error al abrir o crear el archivo
    cout << "No se pudo Guardar la Partida."<< endl;
    exit(1);                                                                            // salir del programa
    }
    else{
        archivoPartidas<<nombreTXT<<"\n";
        archivoPartidas.close();
        vector<Unit*> army;                                         // vector de tipo Unidad que me guarda todas las unidades de jugador.
        string name, name2;                                         // name2 es del jugador*/
        int ATK,DEF,EXP,HPM,ID,Level,Movement,PosX,PosY,Range,HP;
        char Type;
        army=ref.getArmy2();                                             //vector con todas las Unidades del jugador.
        name2=ref.getNombre();
        for(int unsigned j=0;j<army.size();j++){            
            name=army[j]->getNAME();
            Type=army[j]->getType();
            ATK=army[j]->getATK();
            DEF=army[j]->getDEF();
            EXP=army[j]->getEXP();
            HPM=army[j]->getHPM();
            ID=army[j]->getID();
            Level=army[j]->getLevel();
            Movement=army[j]->getMovement();
            PosX=army[j]->getPosX();
            PosY=army[j]->getPosY();
            Range=army[j]->getRange();
            HP=army[j]->getHP();
            cout<<"Se guarda: "<<" Nombre "<<name2<<" Type "<<Type<<" PosX "<<PosX<<" PosY "<<PosY<<endl;// igreso correcto de un char\n";
            archivo<<name2<<" "<<name<<" "<<Type<<" "<<ATK<<" "<<DEF<<" "<<EXP<<" "<<HPM<<" "<<ID<<" "<<Level<<" "<<Movement<<" "<<PosX<<" "<<PosY<<" "<<Range<<" "<<HP<<"\n";
        }

        army=ref2.getArmy2();                                                            //vector con todas las Unidades del jugador.
        name2=ref2.getNombre();
        for(int unsigned j=0;j<army.size();j++){            
            name=army[j]->getNAME();
            Type=army[j]->getType();
            ATK=army[j]->getATK();
            DEF=army[j]->getDEF();
            EXP=army[j]->getEXP();
            HPM=army[j]->getHPM();
            ID=army[j]->getID();
            Level=army[j]->getLevel();
            Movement=army[j]->getMovement();
            PosX=army[j]->getPosX();
            PosY=army[j]->getPosY();
            Range=army[j]->getRange();
            HP=army[j]->getHP();
            cout<<"Se guarda: "<<" Nombre "<<name2<<" Type "<<Type<<" PosX "<<PosX<<" PosY "<<PosY<<endl;// igreso correcto de un char\n";
            archivo<<name2<<" "<<name<<" "<<Type<<" "<<ATK<<" "<<DEF<<" "<<EXP<<" "<<HPM<<" "<<ID<<" "<<Level<<" "<<Movement<<" "<<PosX<<" "<<PosY<<" "<<Range<<" "<<HP<<"\n";
        }
        archivo.close();
    }   
}


bool UI::ListaPartidas(){
 ifstream archivo;
 ifstream archivoVer;
        archivo.open("Partidas.txt",ios::in);                                //abrimos archivo
        if(archivo.fail()){                                                 //error al abrir o crear el archivo
        cout<<"NO SE PUDO ABRIR ARCHIVO DE PARTIDAS"<<endl;
        return false;
        exit(1);                                                            // salir del programa
        }
        else
        {   string nombre;
            archivo>>nombre;
            cout<<"\n";
            while(!archivo.eof()){
             archivoVer.open(nombre,ios::in);                                //abrimos archivo
             if(!archivoVer.fail()){                                         //error al abrir o crear el archivo
                cout << nombre;
                cout<<"\n";                                                                                       
        }else
        {
            cout <<nombre <<" Partida eliminada."<<endl;
        }
            archivo >> nombre;
            }
            archivoVer.close();
            archivo.close();
            return true;
        }
}



bool UI::cargar(Field &field, string nombrePartida){
        string aux;
        ifstream archivo;
        archivo.open(nombrePartida,ios::in);                                    //abrimos archivo
        if(archivo.fail()){                                                     //error al abrir o crear el archivo
        return false;
        exit(1);                                                                // salir del programa
        }
        else
        {
            int j1=0;
            int j2=0;
            string name,name2;
            int ATK,DEF,EXP,HPM,ID,Level,Movement,PosX,PosY,Range,HP;
            char Type;
            archivo>>name2;                                         // nombre del jugador.
            aux=name2;
            while(!archivo.eof()){                                  // mientras no sea el final del archivo

                if(name2==aux){
                    this->nomPer1=name2;
                    archivo>>name;
                    archivo>>Type;
                    cout<<name2<<"---"<<name<<"-----"<<Type<<endl;
                    if(Type=='L'){
                        army1.push_back(new Lancero(name,name2));
                    }
                    if(Type=='C'){
                        army1.push_back(new Caballo(name,name2));
                    }
                    if(Type=='A'){
                        army1.push_back(new Arquero(name,name2));
                    }              
                    archivo>>ATK;
                    army1[j1]->setATK(ATK);
                    archivo>>DEF;
                    army1[j1]->setDEF(DEF);
                    archivo>>EXP;
                    army1[j1]->setEXP(EXP,'s');
                    archivo>>HPM;
                    army1[j1]->setHPM(HPM);
                    archivo>>ID;
                    army1[j1]->setID(ID);
                    archivo>>Level;
                    army1[j1]->setLevel(Level);
                    archivo>>Movement;
                    army1[j1]->setMovement(Movement);
                    archivo>>PosX;
                    army1[j1]->setPosX(PosX);
                    archivo>>PosY;
                    army1[j1]->setPosY(PosY);
                    archivo>>Range;
                    army1[j1]->setRange(Range);
                    archivo>>HP;
                    army1[j1]->setHP(HP);
                    archivo>>name2;                                               // para que siga leyendo.    
                    field.getCell(PosX,PosY)->setOcuped(true);                    // Seteando parametros de la celda
                    field.getCell(PosX,PosY)->setArmy(army1[j1]);
                    j1++;
                }
                else{
                    this->nomPer2=name2;
                    archivo>>name;
                    archivo>>Type;
                    cout<<name2<<"---"<<name<<"-----"<<Type<<endl;
                    if(Type=='L'){
                        army2.push_back(new Lancero(name,name2));
                    }
                    if(Type=='C'){
                        army2.push_back(new Caballo(name,name2));
                    }
                    if(Type=='A'){
                        army2.push_back(new Arquero(name,name2));
                    }
                    archivo>>ATK;
                    army2[j2]->setATK(ATK);
                    archivo>>DEF;
                    army2[j2]->setDEF(DEF);
                    archivo>>EXP;
                    army2[j2]->setEXP(EXP,'s'); //REVISAR FUNCIONAMIENTO
                    archivo>>HPM;
                    army2[j2]->setHPM(HPM);
                    archivo>>ID;
                    army2[j2]->setID(ID);
                    archivo>>Level;
                    army2[j2]->setLevel(Level);
                    archivo>>Movement;
                    army2[j2]->setMovement(Movement);
                    archivo>>PosX;
                    army2[j2]->setPosX(PosX);
                    archivo>>PosY;
                    army2[j2]->setPosY(PosY);
                    archivo>>Range;
                    army2[j2]->setRange(Range);
                    archivo>>HP;
                    army2[j2]->setHP(HP);
                    archivo>>name2;                                              // para que siga leyendo.
                    field.getCell(PosX,PosY)->setOcuped(true);                   // Seteando parametros de la celda
                    field.getCell(PosX,PosY)->setArmy(army2[j2]);
                    j2++;
                }
                
            }
            archivo.close();
            return true;
        }    
}

vector<Unit*> UI::getArmy1(){
    return army1;
}

vector<Unit*> UI::getArmy2(){
    return army2;
}

string UI::getPer1(){
    return this->nomPer1;
}

string UI::getPer2(){
    return this->nomPer2;
}


bool UI::MenuPrincipal(Field &tablero, Player &hellbrain, Player &Nostra){
string n;                                                                // Var aux
cargar:                                                                  // Etiqueta de control de flujo del programa 
bool cargar=this->ListaPartidas();                                       // Obtener indicador si es posible cargar una partida, es decir si existe un archivo de partida creado. 
    if(cargar==true){                                                    // hay algun archivo de partida por cargar
        string nombrePartida;                                            // var aux       
        cout << "\nHay partida(as) Guardadas. Desea cargar alguna? (y/n)"<<endl;
        cin >> n;
         switch (n[0])
        {
            case 'y':
                cout << "Ingrese Nombre de la partida (partida.txt): ";
                cin >>nombrePartida;
                cout << "\nBuscando Partida..."<< endl;
                   cargar = this->cargar(tablero, nombrePartida);
                   if(cargar==false){
                       cout << "\nNo se encontró la partida con ese Nombre. Verifique."<<endl;
                        goto cargar;
                   }
                   else{
                        cout<<"\nCargando Partida...  \n";
                        hellbrain.setNombre(this->getPer1());           //Retorna y crea persona con nombre del jugador guardado.
                        hellbrain.setVector(this->getArmy1());
                        hellbrain.colocarUnits(tablero);
                        cout<<"\nSE COMPLETÓ  DE CARGAR JUGADOR 1\n";

                        //cout<<"SE COMPLETÓ VECTOR 2 \n";
                        Nostra.setNombre(this->getPer2());//Retorna y crea persona con nombre del jugador guardado.
                        Nostra.setVector(this->getArmy2());
                        Nostra.colocarUnits(tablero);
                        cout<<"\nSE COMPLETÓ  DE CARGAR JUGADOR 2\n";
                        return true;
                     }
                case 'n': 
                    return false;
                default: 
                    
                    return false;
                    }
    }
    else{
        return false;
    }
}

void UI::NuevaPartida(Field &tablero, Player &hellbrain, Player &Nostra){
    string jug1;                                                         // VARIABLES AUXILIARES
    string jug2;
    int L,C,A,contaux;
    char LC,CC,AC;
    string LS,CS,AS;
    int count=28;
    bool salir =false;
    cout << "----------------NUEVA PARTIDA--------------------"<<endl; 
    cout << "****************INICIO DEL JUEGO!!***************"<<endl;                         // Iniciando menu del juego
    cout << "Jugador 1 ingrese su nombre: "<<endl;
    cin >> jug1;
    cout << "\n Jugador 2 ingrese su nombre: "<<endl;
    cin >> jug2;
    hellbrain.setNombre(jug1);                                                     // Creando juador 1 
    Nostra.setNombre(jug2);                                                        // Creando juador 2
    vector<Player*> jugadores = {&hellbrain,&Nostra};                              // vector de juagdores
    for( int j=0; j<(int)jugadores.size();j++){                                    // Recorrido de creacion de unidades - Nuevo juego
    verificar:
    count=28;
    L=0,
    C=0;
    A=0;
    contaux=0;
    cout <<"\n"<< jugadores[j]->getNombre() << " ---> Ingrese las unidades que desea: (L,C,A) recuerde no sobrepasar el maximo de costo: 28" << endl;
    LR:
    while(salir==false){
        cout<<"\nCantidad de Lanceros"<<endl;
        cout << "Lanceros - Costo: 3"<< endl;
        cin >> LS;
        LC=LS[0];
        //Verifica si es numero
        if(isalpha(LC)==0){//Numero
            L = stoi(LS, nullptr, 10);
            salir=true;
            
        
        }
        else{
            cout<<"\nVuelva a ingresar NUMERO de unidades de Lanceros."<<"\n";
            salir=false;          
        }
    }
    salir =false;
    contaux=count; //ejemplo 1;
    contaux=contaux-abs(L*3);
    if(contaux<0){
        cout<<"\nNo se puede esa cantidad "<<L; goto LR;
    }  
    count = count - abs(L*3);
    cout << "Puntos restantes: " << count << endl;
    CR: 
    while(salir==false){
            cout << "\nCantidad de Caballos"<< endl;
            cout << "Caballos - Costo: 5"<< endl;
            cin >> CS;
            CC=CS[0];
            if(isalpha(CC)==0){//Numero
                C = stoi(CS, nullptr, 10);
                salir=true;
            }
            else
            {      
                cout<<"\nVuelva a ingresar NUMERO de unidades de Caballo\n";
                salir=false;
            }
        }
    salir =false;
    contaux=count;
    contaux=contaux-abs(C*5);
    if(contaux<0){cout<<"\nNo se puede esa cantidad "<<C; goto CR;}
    count = count - abs(C*5);
    cout << "\nPuntos restantes: " << count << endl;
    AR:
    while(salir==false){
           cout << "\nCantidad de Arqueros "<<  endl;
            cout << "Arqueros - Costo: 4"<< endl;
            cin >> AS;
            AC=AS[0];
            if(isalpha(AC)==0){                                                                     // Mismo procedimiento de las otras clases, verificar Numero
                A = stoi(AS, nullptr, 10);
                salir=true;
            }
            else{      
                cout<<"\nVuelva a ingresar NUMERO de unidades de Arquero\n";
                salir=false;
            }
        }
        salir=false;
        contaux=count; //ejemplo 1;
        contaux=contaux-abs(A*4);
        if(contaux<0){
            cout<<"\nNo se puede esa cantidad "<<A; goto AR;
        }
        count = count - abs(A*4);
        cout << " \nPuntos restantes: " << count << endl;
        if(jugadores[j]->createArmy(L,C,A,tablero)==false){
            goto verificar;
        }
    }
}

void UI::Juego(Field &tablero,Player &hellbrain, Player &Nostra){
    bool Fin = false;
    while (Fin!=true ) {
        Fin = hellbrain.play(tablero);
        if(hellbrain.getGuardar()==true){
            this->guardarJ(hellbrain,Nostra);
            Fin=true;
        }
        if(Fin==false){//Si jugador anterior no quiso salir
            Fin = Nostra.play(tablero);
            if(Nostra.getGuardar()==true){
                this->guardarJ(Nostra,hellbrain);  
                Fin=true;            
            }
        }
    }
    cout <<"\nFinal del juego."<<endl;
}

void UI::MostrarInfoJuego(){

cout << "\n---------------Informacion del juego: ------------"<<endl;
cout << "El juego posee tres unidades  \n";
cout<<"Las x representan los lugares en que se pueden mover y atacar\n";
cout<<"\n \n Lancero (L)\n";
cout<<"Posibles movimientos.\n";
cout<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|\n";
cout<<"|"<<"_"<<"|"<<"_"<<"|"<<"X"<<"|"<<"_"<<"|"<<"_"<<"|\n";
cout<<"|"<<"_"<<"|"<<"X"<<"|"<<"L"<<"|"<<"X"<<"|"<<"_"<<"|\n";
cout<<"|"<<"_"<<"|"<<"_"<<"|"<<"X"<<"|"<<"_"<<"|"<<"_"<<"|\n";
cout<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|\n";
cout<<"\nPosibles ataques\n";
cout<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|\n";
cout<<"|"<<"_"<<"|"<<"_"<<"|"<<"X"<<"|"<<"_"<<"|"<<"_"<<"|\n";
cout<<"|"<<"_"<<"|"<<"X"<<"|"<<"L"<<"|"<<"X"<<"|"<<"_"<<"|\n";
cout<<"|"<<"_"<<"|"<<"_"<<"|"<<"X"<<"|"<<"_"<<"|"<<"_"<<"|\n";
cout<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|\n";

cout<<"\nCaballo (C)\n";
cout<<"Posibles movimientos\n";
cout<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|\n";
cout<<"|"<<"_"<<"|"<<"_"<<"|"<<"X"<<"|"<<"_"<<"|"<<"X"<<"|"<<"_"<<"|"<<"_"<<"|\n";
cout<<"|"<<"_"<<"|"<<"X"<<"|"<<"X"<<"|"<<"_"<<"|"<<"X"<<"|"<<"X"<<"|"<<"_"<<"|\n";
cout<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|"<<"C"<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|\n";
cout<<"|"<<"_"<<"|"<<"X"<<"|"<<"X"<<"|"<<"_"<<"|"<<"X"<<"|"<<"X"<<"|"<<"_"<<"|\n";
cout<<"|"<<"_"<<"|"<<"_"<<"|"<<"X"<<"|"<<"_"<<"|"<<"X"<<"|"<<"_"<<"|"<<"_"<<"|\n";
cout<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|\n";
cout<<"\nPosibles ataques\n";
cout<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|\n";
cout<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|\n";
cout<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|"<<"X"<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|\n";
cout<<"|"<<"_"<<"|"<<"_"<<"|"<<"X"<<"|"<<"C"<<"|"<<"X"<<"|"<<"_"<<"|"<<"_"<<"|\n";
cout<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|"<<"X"<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|\n";
cout<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|\n";
cout<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|\n";

cout<<"\nArquero (A)";
cout<<"\nPosibles movimientos\n";
cout<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|\n";
cout<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|\n";
cout<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|"<<"X"<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|\n";
cout<<"|"<<"_"<<"|"<<"_"<<"|"<<"X"<<"|"<<"A"<<"|"<<"X"<<"|"<<"_"<<"|"<<"_"<<"|\n";
cout<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|"<<"X"<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|\n";
cout<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|\n";
cout<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|\n";
cout<<"\nPosibles ataques\n";
cout<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|"<<"X"<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|\n";
cout<<"|"<<"_"<<"|"<<"_"<<"|"<<"X"<<"|"<<"_"<<"|"<<"X"<<"|"<<"_"<<"|"<<"_"<<"|\n";
cout<<"|"<<"_"<<"|"<<"X"<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|"<<"X"<<"|"<<"_"<<"|\n";
cout<<"|"<<"X"<<"|"<<"_"<<"|"<<"_"<<"|"<<"A"<<"|"<<"_"<<"|"<<"_"<<"|"<<"X"<<"|\n";
cout<<"|"<<"_"<<"|"<<"X"<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|"<<"X"<<"|"<<"_"<<"|\n";
cout<<"|"<<"_"<<"|"<<"_"<<"|"<<"X"<<"|"<<"_"<<"|"<<"X"<<"|"<<"_"<<"|"<<"_"<<"|\n";
cout<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|"<<"X"<<"|"<<"_"<<"|"<<"_"<<"|"<<"_"<<"|\n";

//mostrar informacion de las unidaes (costo, posibles movimientos y atacar)
cout<<"\n \n\t ******* Teclas/letras especiales ******* \n\n";
cout<<"q\t"<<" Salir del juego y desidiendo si guarda o no.\n";
cout<<"n\t"<<" No realizar una acción.\n";
cout<<"yy\t"<<" Salir del juego y guardar partida.\n";
cout<<"yn\t"<<" Salir del juego y sin guarda partida.\n";
cout<<"i\t"<<" Información de todas las unidades (Nombre, tipo, posición.. etc).\n";
cout<<"u\t"<<" Información de una unidad especifica seleccionada por el nombre.\n";
cout<<"y\t"<<" Aceptar.\n";
cout<<"p \t mover y atacar \n\n";
}