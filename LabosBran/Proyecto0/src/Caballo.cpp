//INCLUDES

#include "../include/Caballo.h"

//DEFINES
using namespace std;

//Metodos

//Constructor
Caballo::Caballo(){
this->setHPM(25);
this->setHP(25);
this->setATK(15);
this->setDEF(5);
this->setEXP(0,'s');
this->setID(0);
this->setLevel(1);
this->setMovement(3);
this->setNAME("NULL");
this->setRange(1);
this->setType('C');
}

//Constructor
Caballo::Caballo(string name, string jugador){
this->setHPM(25);
this->setHP(25);
this->setATK(15);
this->setDEF(5);
this->setEXP(0,'s');
this->setID(0);
this->setLevel(1);
this->setMovement(3);
this->setNAME("NULL");
this->setRange(1);
this->setType('C');
this->setNAME(name);
this->setJug(jugador);
}

Caballo::~Caballo(){
    //Do nothing broh
}

int Caballo::getCost(){
    return cost;
}

bool Caballo::Move(Cell *ref, int x, int y){
    //Revisando Posibilidades de avance
    int mover = ref->getArmy()->getMovement();        // Obtengo cantidad posible de movimiento
    int posx = ref->getArmy()->getPosX();             // Obtengo posiciones (x,y) actuales del unit
    int posy = ref->getArmy()->getPosY();
    int sum = abs(posx-x)+abs(posy-y);
    cout << "Capacidad de movimiento: " << mover << endl;
    // posibles movimientos prohibidos del Caballo
    if( (sum<=mover && sum>=0) && !(x==posx || posy==y)  ){
       cout<<"Ubicacion correcta.."<<endl;
        return true;                                 // Si es una coordenada valida
    }
    else{   
        return false;
    }   
}

bool Caballo::Attack(Unit *ref, int x, int y){

    int alcance = ref->getRange();           // Obtengo rango de alcance del unit
    int posx = ref->getPosX();               // Obtengo posiciones (x,y) actuales del unit
    int posy = ref->getPosY();
    if( ( (x==posx+alcance || x==posx-alcance) &&  y==posy) ||  ( (y==posy+alcance || y==posy-alcance) &&  x==posx)){
        cout << "Rango de ataque Correcto"<< endl;
        return true;                        // Si es una coordenada valida
    }
    else{
       cout << "Rango de ataque No Adecuado."<< endl;
       return false;                       // Si no
    }
}