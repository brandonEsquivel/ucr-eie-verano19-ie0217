
//INCLUDES

#include "../include/Arquero.h"

//DEFINES
using namespace std;

//Metodos

Arquero::Arquero(){
//Constructor
this->setHPM(15);
this->setHP(15);
this->setATK(8);
this->setDEF(5);
this->setEXP(0,'n');
this->setID(0);
this->setLevel(1);
this->setMovement(1);
this->setNAME("NULL");
this->setRange(3);
this->setType('A');
}

Arquero::Arquero(string name, string jugador){
//Constructor
this->setHPM(15);
this->setHP(15);
this->setATK(8);
this->setDEF(5);
this->setEXP(0,'s');
this->setID(0);
this->setLevel(1);
this->setMovement(1);
this->setNAME("NULL");
this->setRange(3);
this->setType('A');
this->setNAME(name);
this->setJug(jugador);
}

int Arquero::getCost(){
    return cost;
}

Arquero::~Arquero(){
}

bool Arquero::Move(Cell *ref, int x, int y){
    //Revisando Posibilidades de avance
    int mover = ref->getArmy()->getMovement();        // Obtengo cantidad posible de movimiento
    int posx = ref->getArmy()->getPosX();             // Obtengo posiciones (x,y) actuales del unit
    int posy = ref->getArmy()->getPosY();
    cout << "Capacidad de movimiento: " << mover << endl;
    // posibles movimientos prohibidos del Arque
    if(     (   (x == posx + mover || x==posx-mover)    && y==posy )    || (    (y == posy+mover || y==posy-mover)   && x ==posx )  ){
       cout<<"Ubicacion correcta.."<<endl;
       return true;                                 // Si es una coordenada valida
    }
    else{   
        return false;
    }
}

bool Arquero::Attack(Unit *ref, int x, int y){
    int alcance = ref->getRange();           // Obtengo rango de alcance del unit
    int posx = ref->getPosX();               // Obtengo posiciones (x,y) actuales del unit
    int posy = ref->getPosY();
    int sum = abs(posx-x)+abs(posy-y);
    if( sum==alcance){
        cout << "Rango de ataque Correcto"<< endl;
        return true;                        // Si es una coordenada valida
    }
    else{
       cout << "Rango de ataque No Adecuado."<< endl;
       return false;                       // Si no
    }
}