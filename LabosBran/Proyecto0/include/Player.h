#pragma once

#include "Lancero.h"
#include "Arquero.h"
#include "Caballo.h"
#include "Field.h"



using namespace std;
/** @brief Implementacion/definicion de la clase Player que Representa a los jugadores
    @author Reinier Camacho, Brandon Esquivel Molina.
    @date February 2020
    */
class Player{
    private:
        
        //Unit* army;
        Field playfield;/**Referencia a field*/
        string name; /** Nombre del jugador*/
        int score; /**Puntaje acumulado del jugador*/
        bool baja; /**Indica cuando se tiene una baja para proceder a eliminar la unidad y refrescar la celda*/
        bool guardar;/**Para saber si jugador quiere guardar*/
    
    public:
        vector<Unit*> army;/**vector con Todas las unidades del jugador */
        static const int maxCost=28; /**costo maximo del ejercito del jugador*/

        /** Default constructor. 
         * @brief construye el objeto Player */
        Player();

        /** Default destructor 
         * @brief Destruye el objeto player */
        ~Player();

         /** Custom constructor. 
         * @brief construye el objeto Player indicando su nombre*/
        Player(string name);
        
        /** Crea las unidades definidas por el usuario. 
         * @brief construye objetos de tipo Unity y las agraga a un arreglo.
         * @param lancer: cantidad de lancer 
         * @param caval: Cantidad de Cavalry
         * @param arch: Cantidad de Archer. 
         * @return bool: indica si se completo con exito o no la creacion de unidades*/
        bool createArmy(int lancer ,int caval ,int arch, Field &field);

         /** Recibe referencia al field como argumento y contiene la logica de mover una unidad.
         * @brief Recibe referencia al field como argumento y contiene la logica de mover una unidad. */  
        void Mover(Field &field);

         /** Recibe referencia al field como argumento y contiene la logica de Atacar con una unidad.
         * @brief Recibe referencia al field como argumento y contiene la logica de Atacar con una unidad. */
        void Atacar(Field &field);

        /** Muestra Toda la informacion de las unidades del jugador de forma ordenada
         * @brief Muestra Toda la informacion de las unidades del jugador de forma ordenada */
        void MostrarInfoTodasUnidades(Field &field);

        /** Muestra Toda la informacion de una unidad del jugador de forma ordenada, indicada por el nombre
         * @briefMuestra Toda la informacion de una unidad del jugador de forma ordenada, indicada por el nombre.*/
        void MostrarInfoUnidad(Field &field, string name);
        
        /** Devuelve string nombre del jugador
         * @return String Nombre - Nombre del jugador */
        string getNombre();

        /** set string nombre del jugador
         * @param Nombre - Nombre del jugador */
        void setNombre(string nom);

        /** Devuelve un vector de tipo Unit 
         * @param pos: posicion (index) en el vector army de objetos Unit.
         * @brief Devuelve un objeto de tipo Unit.
         * @return objeto de tipo Unit.*/ 
        Unit* getArmy(int pos);

        /** Devuelve un vector de tipo Unit inicializado
         * @param cant: cantidad de unidades
         * @brief Devuelve un vector de tipo Unit inicializado.  */ 
        void inicializar(int cant);

        /** Devuelve baja, indicando si es necesario eimnar una unidad derrotada
         * @param baja - indicador de una baja de unidades
         * @return baja -  indicador de una baja de unidades */ 
        bool getBaja();

         /** set baja, indica si es necesario eliminar una unidad derrotada
         * @param baja - indicador de una baja de unidades*/
        void setBaja(bool b);

        /** recibe referencia al field como argumento y contiene la logica de un turno del jugador
         * @brief recibe referencia al field como argument y contiene la logica de un turno del jugador. */    
       bool play(Field &field);

        /** recibe referencia al field como argumento y contiene la logica de colocar las unidades en el campo de batalla
        * @brief recibe referencia al field como argument y la posicion index j del vector de unidades a colocar y contiene la logica de colocar las unidades en el campo de batalla. */  
       void ColocarUnit(int j, Field &field);

       void colocarUnits(Field &field);

       void setGuardar(bool guardar);
       
       vector<Unit*> getArmy2();

       bool getGuardar();

       void setVector(vector<Unit*> ref);

};