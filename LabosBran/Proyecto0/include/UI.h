#pragma once

//INCLUDES
#include "Player.h"



using namespace std;
/** @brief Implementacion/definicion de la clase UI, que mantiene la logica de impresion en consola o grafica
    @author Reinier Camacho, Brandon Esquivel Molina.
    @date February 2020*/
class UI{
    private:
        vector<Unit*> army1;/**Guarda vector de unidades de un jugador.*/
        vector<Unit*> army2; /**Guarda vector de unidades de un jugador.*/
        string nomPer1;/** Nombre del jugador 1*/
        string nomPer2; /** Nombre del jugador 2*/

    public:
        UI();
        ~UI();

         /** Guarda el juego
         * @brief Guarda el estado del juego
         * */
        void guardarJ(Player ref, Player ref2);

        /** Carga  el juego
         * @brief Carga el estado del juego
         * @return bool: retorna respuesta, a juegos guardados.
         * */
        bool cargar(Field &field, string n);

        /** Retorna un vector de Unidades de un jugador cargado desde archivo
         * @brief Retorna un vector de Unidades de un jugador cargado desde archivo
         * @return vector<Unit*>: retorna vector de unidades de un jugador
         * */
        vector<Unit*> getArmy1();//Retorna vector de un jugador

        /** Retorna un vector de Unidades de un jugador cargado desde archivo
         * @brief Retorna un vector de Unidades de un jugador cargado desde archivo
         * @return vector<Unit*>: retorna vector de unidades de un jugador
         * */
        vector<Unit*> getArmy2();//Retorna vector del otro jugador

        /** Retorna el nombre del jugador 1 
         * @brief Retorna un nombre del jugador 1 guardado.
         * @return string: retorna el nombre del jugador.
         * */
        string getPer1();

        /** Retorna el nombre del jugador 1 
         * @brief Retorna un nombre del jugador 1 guardado.
         * @return string: retorna el nombre del jugador.
         * */
        string getPer2();        


       /** Retorna si existen partidas guardadas o no 
         * @brief Retorna si existen partidas guardadas o no.
         * @return bool: retorna indicador si existen partidas guardadas o no. */
        bool ListaPartidas();

        /** Metodo de inicio llamado al menu principal
         * @brief Metodo de inicio llamado al menu principal*/
        bool MenuPrincipal(Field &field,Player &hellbrain, Player &Nostra);

        /** Metodo de inicio llamado a crear nueva partida
         * @brief Metodo de inicio llamado a crear nueva partida*/
        void NuevaPartida(Field &tablero, Player &hellbrain,Player &Nostra);

        /** Metodo de inicio del juego por turnos
         * @brief Metodo de inicio del juego por turnos*/
        void Juego(Field &tablero,Player &hellbrain, Player &Nostra);

      
        /** Imprime instrucciones del juego
         * @brief  Imprime instrucciones del juego*/
        void MostrarInfoJuego();




};