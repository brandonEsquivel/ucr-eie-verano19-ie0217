#pragma once

//INCLUDES

#include "Unit.h"
class Unit;

//DEFINES
using namespace std;

/** @brief Implementacion/definicion de la clase Cell que Representa una posicion del campo de juego
    @author Brandon Esquivel
    @date February 2020
    */
class Cell{
    private:
        bool passable;/** indica si el terreno es franqueable o no.*/
        bool ocupada;/** indica si la celda esta ocupada. */
        Unit *army; /** indica si hay una unidad en esa celda o no. Si passable es falso, este valor es nulo. cUANDO SE REFRESCA UNA CELDA, QUEDA NULO*/
        
    public:

         /** Default constructor. 
         * @brief construye el objeto Cell */
        Cell();

        /** Custom constructor. 
         * @brief construye el objeto Cell determinando passable*/
        Cell(bool ps);


        /** Default destructor 
         * @brief Destruye el objeto Cell */
         ~Cell();

        /**  metodo que imprime la informacion de la celda; si es franqueable o no y si tiene una unidad en ella, e indica el tipo
         *  @brief Metodo de impresion de informacion de la celda
         * @return info de la celda impresa en consola*/
        void printCellInfo();

         /** Metodo get del parametro passable
         * @brief Metodo get del parametro passable
        * @return passable - indica si el terreno es franqueable o no*/
        bool getPassable();
        
         /** Metodo Set del parametro passable
         * @brief Metodo Set del parametro passable
        * @param passable - indica si el terreno es franqueable o no*/
        void setPassable(bool pass);

        /** Metodo get del parametro Army
         * @brief Metodo get del parametro Army
        * @return Unit* - Puntero a la unidad que tiene la celda.*/
        Unit* getArmy();
        
         /** Metodo Set del parametro Army
         * @brief Metodo Set del parametro Army
        * @param Army - indica si hay una unidad en esa celda o no*/
        void setArmy(Unit *unidad);

        /** Metodo Set del parametro Ocupada
         * @brief Metodo Set del parametro Ocupada
        * @param ocupada- indica si hay una unidad en esa celda o no*/
        void setOcuped(bool oc);

        /** Metodo get del parametro Ocupada
         * @brief Metodo get del parametro Ocupada
        * @return ocupada - indica si hay una unidad en esa celda o no*/
        bool getOcuped();

};
