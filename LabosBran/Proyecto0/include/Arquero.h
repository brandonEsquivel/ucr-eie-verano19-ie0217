#pragma once

//INCLUDES

#include "Unit.h"

//DEFINES
using namespace std;

/** @brief Implementacion/definicion de la clase Arquero que hereda de Unit.
 *  Cada unidad debe mantener una lista de atributos, asi como un tipo, y su ubicacion en el campo
    @author Brandon Esquivel
    @date February 2020
    */
class Arquero: public Unit{
  
           

    public: 

        const static int cost = 4;/** indica el costo de la unidad.*/

         /** Constructor 
         * @brief Constructor del objeto Arquero*/
        Arquero();

        /** Custom Constructor 
         * @brief Constructor del objeto Arquero
         * @param Name - nombre de la unidad*/
        Arquero(string name, string jugador);


        /** Destructor 
         * @brief Destructor  del objeto Arquero*/
        ~Arquero();
        
       /** Metodo get del parametro static Cost 
         * @brief Metodo get del parametro Movement
        * @return static Cost - Cantidad de unidades del tipo*/
        static int getCost();

        /** Metodo Attack de las unidades 
         * @brief Metodo attack de las unidades
        * @param Attack - Posibilidad de Ataque del Unit*/
        bool Attack(Unit *ref, int x, int y);


            /** Metodo del parametro Move 
         * @brief Metodo del parametro Move
        * @param Move - Posibilidad de Movimiento del Unit*/
         bool Move(Cell *ref, int x, int y);

};