#pragma once

#include "header.h"
#include "Cell.h"


using namespace std;
/** @brief Implementacion/definicion de la clase Field que Representa el campo del juego con dimensiones 10x9 por defecto
    @author Reinier Camacho, Brandon Esquivel Molina.
    @date February 2020
    */
class Field{
    private:
        bool guardar;/**Indica si se va guardar el juego*/
        int cows=10;/**Columnas del tablero*/
        int rows=9;/**Filas del tablero.*/
        Cell* playfield[10][9];/**Matriz de punteros a celdas*/
    public:
        /** Default constructor. 
         * @brief construye el objeto Field */
        Field();
        
        /** Default destructor 
         * @brief Destruye el objeto Field */
        ~Field();
        
        /** Guarda una celda en el vector de celdas
         *  @brief Crea todas las celdas y las guarda en un vector de tipo cell.*/
        void tabInicial();
            
        /** Muestra todas las celdas
         * @brief Muestra en pantalla el tablero.*/  
        void printTab(void);

        /** Muestra el valor de cow
         * @brief Muestra en valor de cow(columnas del tablero).
         * @return int: retorna un entero de tipo int con el valor de cow*/    
        int getCow();

        /** Muestra el valor de row
         * @brief Muestra en valor de row.(columnas del tablero).
         * @return int: retorna un entero de tipo int con el valor de row */ 
        int getRow();

        /** Muestra la celda de una posicion
         * @param cow: posicion de columna.
         * @param row: posicion de fila.
         * @brief Muestra la referencia de una celda del tablero.
         * @return Cell*: retorna un puntero a una celda ubicada en la posicion de memoria del array playfield   */         
        Cell* getCell(int cow, int row); 

        
        /** Refresca la celda de una posicion luego de mover o eliminar una unidad
         * @brief Refresca la celda de una posicion luego de mover o eliminar una unidad
        * @param x: coordenada X del campo de batalla.
        * @param y: coordenada Y del campo de batalla.*/
        void RefreshCelda(Field &field,int x,int y);

        void Guardar();
        bool Cargar();

};