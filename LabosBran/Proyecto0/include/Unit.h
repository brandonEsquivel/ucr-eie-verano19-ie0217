#pragma once

//INCLUDES

#include "header.h"
#include "Cell.h"

class Cell;

//DEFINES
using namespace std;

/** @brief Implementacion/definicion de la clase Unit que representa a las unidades del juego.
 *  Cada unidad debe mantener una lista de atributos, asi como un tipo, y su ubicacion en el campo
    @author Brandon Esquivel
    @date February 2020
    */
class Unit{

    private:
            int id;/** indica el numero de unidad como ID para el jugador.*/ 
            char type;/** indica el tipo de la unidad.*/
            string name;/** indica el nombre de la unidad.*/
            string jugador;/** indica el nombre del jygador al que pertenece la unidad.*/
            int maxHitPoints;/** indica el numero maximo de HP de la unidad.*/
            double hitPoints;/** indica el numero de HP actual de la unidad.*/
            double attack;/** indica el valor de ataque de la unidad.*/
            double defense;/** indica el valor de defensa de la unidad.*/
            int range;/** indica el valor de alcance de la unidad.*/
            int level;/** indica el nivel actual de la unidad.*/
            int experience;/** indica el valor de experiencia de la unidad.*/
            int movement;/** indica la cantidad de espacios de movimiento de la unidad.*/
            int posX;/** indica la posicion en x  de la unidad en el tablero.*/
            int posY;/** indica la posicion en y  de la unidad en el tablero.*/

           
           

    public: 

        /** Default constructor. 
         * @brief construye el objeto Unit */
        Unit();

        /** Default destructor 
         * @brief Destruye el objeto Unit */
        ~Unit();


         /** Metodo get del parametro id
         * @brief Metodo get del parametro id
        * @return ID- ID de la unidad*/
        int getID();

        /** Metodo set del parametro id
         * @brief Metodo set del parametro id
        * @return id - nombre del Unit*/
        void setID(int id);

        /** Metodo get del parametro type
         * @brief Metodo get del parametro Type
        * @return type - tipo del Unit*/
        char getType();

        /** Metodo set del parametro type
         * @brief Metodo set del parametro Type
        * @return type - nombre del Unit*/
        void setType(char tipo);

        /** Metodo get del parametro name
         * @brief Metodo get del parametro name
        * @return name - nombre del Unit*/
        string getNAME();

        /** Metodo set del parametro name
         * @brief Metodo set del parametro name
        * @param name - nombre del Unit*/
        void setNAME(string name);


        /** Metodo get del parametro HP
         * @brief Metodo get del parametro HP
        * @return HP - Puntos de vida actuales del Unit*/
        int getHP();

        /** Metodo set del parametro HP
         * @brief Metodo set del parametro HP
        *  @param HP - Puntos de salud del Unit
        */
        void setHP(int hp);


        /** Metodo get del parametro HPM
         * @brief Metodo get del parametro HP max
        * @return HPM - Puntos de vida maximos del Unit*/
        int getHPM();

        /** Metodo set del parametro HPM
         * @brief Metodo set del parametro HPM
        *  @param HPM - Puntos de salud maximos del Unit
        */
        void setHPM(int hpm);

        /** Metodo get del parametro ATK
         * @brief Metodo get del parametro ATK
        * @return ATK - Puntos de ataque del Unit*/
        int getATK();

        /** Metodo set del parametro ATK
         * @brief Metodo set del parametro ATK
        * @param ATK - Puntos de ataque del Unit*/
        void setATK(int atk);


        /** Metodo set del parametro DEF 
         * @brief Metodo get del parametro DEF
        * @return DEF - Puntos de defensa del Unit*/
        int getDEF();

        /** Metodo set del parametro DEF 
         * @brief Metodo set del parametro DEF
        * @param DEF - Puntos de defensa del Unit*/
        void setDEF(int def);


        /** Metodo get del parametro Range
         * @brief Metodo get del parametro Range
        * @return Range - rango de alcance del Unit*/
        int getRange();

        /** Metodo set del parametro Range
         * @brief Metodo set del parametro Range
        * @param Range - rango de alcance del Unit*/
        void setRange(int Range);


        /** Metodo get del parametro PosX
         * @brief Metodo get del parametro PosX
        * @return PosX - coordenada x en el campo del unit*/
        int getPosX();

        /** Metodo set del parametro PosX
         * @brief Metodo set del parametro PosX
        * @param PosX - coordenada x en el campo del unit*/
        void setPosX(int PosX);

        /** Metodo get del parametro PosY
         * @brief Metodo get del parametro PosY
        * @return PosY - coordenada Y en el campo del unit*/
        int getPosY();

        /** Metodo set del parametro PosY
         * @brief Metodo set del parametro PosY
        * @param PosY - coordenada Y en el campo del unit*/
        void setPosY(int PosY);


        /** Metodo get del parametro EXP
         * @brief Metodo get del parametro EXP
        * @return EXP - Puntos de Experiencia del Unit*/
        int getEXP();

        /** Metodo set del parametro EXP 
         * @brief Metodo set del parametro EXP
        * @param EXP - Puntos de Experiencia del Unit
        * @param m - char 'm' indica sumar experiencia exp, si no se indica 'm' se setea la experiencia al valor de exp */
        void setEXP(int exp, char m);

         /** Metodo get del parametro LEVEL 
         * @brief Metodo get del parametro Level
        * @return LEVEL - Nivel del Unit*/
        int getLevel();

        /** Metodo set del parametro LEVEL 
         * @brief Metodo set del parametro Level
        * @param LEVEL - Nivel del Unit*/
        void setLevel(int lv);

        /** Metodo revision de subir de nivel 
         * @brief Metodo de revision de la experiencia para subir de nivel
        * @param LEVEL - Nivel del Unit
        * @param Exp - Experiencia del Unit*/
        bool subirNivel();
        

         /** Metodo get del parametro Movement 
         * @brief Metodo get del parametro Movement
        * @return Movement - Capacidad de Movimiento del Unit*/
        int getMovement();

        /** Metodo set del parametro Movement 
         * @brief Metodo set del parametro Movement
        * @param Movement - Capacidad de Movimiento del Unit*/
        void setMovement(int mv);

        /** Metodo set del parametro HP cuando lo atacan
         * @brief Metodo set del parametro HP (disminuir)
        * @param HP - Salud actual del Unit*/
        void lessHP(int hp);


        /** Metodo set del parametro jugador 
         * @brief Metodo set del parametro jugador.
        * @param Jugador - Indica el numero de jugador al que pertenece la Unidad*/
        void setJug(string NombreJugador);


          /** Metodo set del parametro jugador 
         * @brief Metodo set del parametro jugador.
        * @return Jugador - Indica el nombre del jugador al que pertenece la Unidad*/
        string getJug();

           /** Metodo Attack de las unidades 
         * @brief Metodo attack de las unidades
        * @param Attack - Posibilidad de Ataque del Unit*/
        virtual bool Attack(Unit *ref, int x, int y);


            /** Metodo del parametro Move 
         * @brief Metodo del parametro Move
        * @param Move - Posibilidad de Movimiento del Unit*/
        virtual bool Move(Cell *ref, int x, int y);

};
