/** 
 * @authors Brandon Esquivel, Yassir Wagon, Reinier Camacho
 * @date Febrero 2020
 * @warning UCR EIE Version BETA del Proyecto 0
 * @mainpage Proyecto 0: Juego de estrategia basada en turnos
 * @brief Implementacion del Proyecto 0 del curso IE0217 Estructuras abstractas y algoritmos para ingeniería.
 * @section intro_sec Introduccion
 * En el presente codigo se muestra la solucion implementada del proyecto 0 del curso IE0217 Estructuras abstractas y algoritmos para ingeniería, de la escuela de Ingenieria Electrica , EIE, UCR.
 * El objetivo de este proyecto es brindarle al estudiante la capacidad de modelar e implementar problemas utilizando la programacion orientada a objetos usando el lenguaje de programacion C++. 
 * @section compile_sec Compilacion y ejecucion
 * Ejecute el comando make en la carpeta raiz, luego, para correr ejecute make run o bien ./bin/main
 * @section Documentation_sec Documentacion
 * Aparte de este HTLM podra encontrar un pdf en doc/latex/refman.pdf latex con toda la informacion detallada. Si no encuentra el pdf, utilize el comando make en esa carpeta para generarlo.
 * @section about_sec Sobre el programa
 * Un motor basico para un juego de estrategia por turnos en modo texto. Algunos ejemplos de estos juegos son Final Fantasy Tactics o Fire Emblem. En este tipo de juegos, unidades de bandos opuestos se enfrentan en un campo de juego, hasta que uno de los bandos quede sin unidades. Las unidades reciben dan˜o basado en sus habilidades de defensa y ofensiva del rival hasta que sus puntos de vida se acaben.
 * Cada vez que una unidad derrota a un rival, esta gana experiencia y cuando acumula la suﬁciente, sube un nivel, restaurando sus puntos de vida y mejorando sus habilidades.

 * @section concepts_sec Resumen Teórico
 * @subsection Herencia
 * La reutilizadón de software reduce el tiempo de desarrollo de un programa.
 * a clase base directa de una clase derivada es la clase base a partir de la cual hereda la clase derivada (especificada por el nombre de la clase a la derecha de : en la primera línea de la definición de la clase). Una clase base indirecta de una clase derivada es una clase dos o más niveles arriba en la jerarquía de ciases de dicha clase derivada.
 * Con la herencia simple, una clase se deriva de otra clase base. Con la herencia múltiple, una clase se deriva de dos o más clases base directas.
 *  Una dase derivada puede incluir sus propios datos y funciones miembro, por lo que a menudo una clase derivada es más grande que una clase base.
 * Una clase derivada es más específica que una dase base y representa a un grupo más pequeño de objetos.
 * Cada objeto de una dase derivada es también un objeto de su clase base. Sin embargo, un objeto de la clase base no es un objeto de sus clases derivadas.
 * Las funciones miembro de la dase derivada pueden acceder a los miembros protegidos de la clase base de manera directa.
 * Una relación "es-un” representa a la herenda. En una rdadón "es-un”, un objeto de una clase derivada también puede tratarse como un objeto de su clase base.
 * Una reladón “tiene-un" representa a la composición. En una rdadón “tiene-un”, un objeto de la clase contiene uno o más objetos de otras clases como miembros.
 * @subsection Polimorfismo
 * Con las fundones virtual y el polimorfismo es posible diseñar e ¡mplementar sistemas que sean más fácilmente extensibles. Es posible escribir programas para procesar objetos de tipos que tal vez no existan cuando el programa se encuentre en desarrollo.
 * a programación polimórfica con fundones virtual puede eliminar la necesidad de la lógica de switch . El programador puede utilizar el mecanismo de las fundones virtuales para llevar a cabo automáticamente la lógica equivalente, evitando así los tipos de errores generalmente asodados con la lógica de sw itc h . El código cliente que hace dedsiones acerca de los tipos de objetos y las representaciones indica un pobre diseño de clases.
 * Las clases derivadas pueden proporcionar sus propias implcmcntadones de una fundón virtual de la clase base, si es necesario, pero de no ser así. se utiliza la implcmcntación de la clase base.
 * Si se llama a una fundón virtual hadendo referenda a un objeto específico por nombre y utilizando d operador punto para selecdonar miembros, la referencia se resudve en tiempo de compilación (esto se conoce como vinculación estática); la función virtual que se llama es la que está definida para la clase de ese objeto específico.
 * Hay muchas situadones en las que es útil definir clases abstractas de las cuales el programador nunca pretenderá instandar un objeto. Como éstas se utilizan sólo como clases base, nos referimos a días como clases base abstractas. No pueden crearse instandas de objetos de una clase abstracta.
 *Las clases a partir de las cuales se pueden instandar objetos se conocen como clases concretas.
 * Una dase se hace abstracta al declarar una o más de sus fundones virtuales como puras. Una fundón virtual pura contiene un inicializador de - 0 en su dcclaradón.
 * Si una clase se deriva de una clase con una función virtual pura y esa clase derivada no proporciona una dc- finidón para esa función virtual pura, entonces esa función virtual seguirá siendo pura en la clase derivada. Por consecuencia, la clase derivada se convertirá también en una clase abstracta.
 * C++ permite el polimorfismo: la habilidad de que los objetos de clases distintas reladonadas por herencia respondan de manera distinta a la llamada de la misma función miembro.
 * El polimorfismo se implementa mediante funciones virtual.
 * **/ 




//PRAGMA ONCE
#pragma once
//INCLUDES
#include <stdlib.h>
#include <iostream>
#include <string.h>
#include <vector>
#include<fstream>
#include <string>
#include <cmath>

