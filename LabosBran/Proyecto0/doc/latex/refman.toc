\contentsline {chapter}{\numberline {1}Proyecto 0\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}: Juego de estrategia basada en turnos}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Introduccion}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Compilacion y ejecucion}{1}{section.1.2}
\contentsline {section}{\numberline {1.3}Documentacion}{1}{section.1.3}
\contentsline {section}{\numberline {1.4}Sobre el programa}{2}{section.1.4}
\contentsline {section}{\numberline {1.5}Resumen Te\IeC {\'o}rico}{2}{section.1.5}
\contentsline {subsection}{\numberline {1.5.1}Herencia}{2}{subsection.1.5.1}
\contentsline {subsection}{\numberline {1.5.2}Polimorfismo}{2}{subsection.1.5.2}
\contentsline {chapter}{\numberline {2}Hierarchical Index}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Class Hierarchy}{3}{section.2.1}
\contentsline {chapter}{\numberline {3}Class Index}{5}{chapter.3}
\contentsline {section}{\numberline {3.1}Class List}{5}{section.3.1}
\contentsline {chapter}{\numberline {4}File Index}{7}{chapter.4}
\contentsline {section}{\numberline {4.1}File List}{7}{section.4.1}
\contentsline {chapter}{\numberline {5}Class Documentation}{9}{chapter.5}
\contentsline {section}{\numberline {5.1}Arquero Class Reference}{9}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Detailed Description}{12}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}Constructor \& Destructor Documentation}{12}{subsection.5.1.2}
\contentsline {subsubsection}{\numberline {5.1.2.1}Arquero()\hspace {0.1cm}{\relax \fontsize {8}{9.5}\selectfont \abovedisplayskip 6\p@ plus2\p@ minus4\p@ \abovedisplayshortskip \z@ plus\p@ \belowdisplayshortskip 3\p@ plus\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 3\p@ plus\p@ minus\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \ttfamily [1/2]}}{12}{subsubsection.5.1.2.1}
\contentsline {subsubsection}{\numberline {5.1.2.2}Arquero()\hspace {0.1cm}{\relax \fontsize {8}{9.5}\selectfont \abovedisplayskip 6\p@ plus2\p@ minus4\p@ \abovedisplayshortskip \z@ plus\p@ \belowdisplayshortskip 3\p@ plus\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 3\p@ plus\p@ minus\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \ttfamily [2/2]}}{12}{subsubsection.5.1.2.2}
\contentsline {subsubsection}{\numberline {5.1.2.3}$\sim $\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Arquero()}{13}{subsubsection.5.1.2.3}
\contentsline {subsection}{\numberline {5.1.3}Member Function Documentation}{13}{subsection.5.1.3}
\contentsline {subsubsection}{\numberline {5.1.3.1}Attack()}{13}{subsubsection.5.1.3.1}
\contentsline {subsubsection}{\numberline {5.1.3.2}get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Cost()}{13}{subsubsection.5.1.3.2}
\contentsline {subsubsection}{\numberline {5.1.3.3}Move()}{14}{subsubsection.5.1.3.3}
\contentsline {subsection}{\numberline {5.1.4}Member Data Documentation}{14}{subsection.5.1.4}
\contentsline {subsubsection}{\numberline {5.1.4.1}cost}{14}{subsubsection.5.1.4.1}
\contentsline {section}{\numberline {5.2}Caballo Class Reference}{14}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Detailed Description}{17}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Constructor \& Destructor Documentation}{17}{subsection.5.2.2}
\contentsline {subsubsection}{\numberline {5.2.2.1}Caballo()\hspace {0.1cm}{\relax \fontsize {8}{9.5}\selectfont \abovedisplayskip 6\p@ plus2\p@ minus4\p@ \abovedisplayshortskip \z@ plus\p@ \belowdisplayshortskip 3\p@ plus\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 3\p@ plus\p@ minus\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \ttfamily [1/2]}}{17}{subsubsection.5.2.2.1}
\contentsline {subsubsection}{\numberline {5.2.2.2}Caballo()\hspace {0.1cm}{\relax \fontsize {8}{9.5}\selectfont \abovedisplayskip 6\p@ plus2\p@ minus4\p@ \abovedisplayshortskip \z@ plus\p@ \belowdisplayshortskip 3\p@ plus\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 3\p@ plus\p@ minus\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \ttfamily [2/2]}}{17}{subsubsection.5.2.2.2}
\contentsline {subsubsection}{\numberline {5.2.2.3}$\sim $\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Caballo()}{18}{subsubsection.5.2.2.3}
\contentsline {subsection}{\numberline {5.2.3}Member Function Documentation}{18}{subsection.5.2.3}
\contentsline {subsubsection}{\numberline {5.2.3.1}Attack()}{18}{subsubsection.5.2.3.1}
\contentsline {subsubsection}{\numberline {5.2.3.2}get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Cost()}{18}{subsubsection.5.2.3.2}
\contentsline {subsubsection}{\numberline {5.2.3.3}Move()}{18}{subsubsection.5.2.3.3}
\contentsline {subsection}{\numberline {5.2.4}Member Data Documentation}{19}{subsection.5.2.4}
\contentsline {subsubsection}{\numberline {5.2.4.1}cost}{19}{subsubsection.5.2.4.1}
\contentsline {section}{\numberline {5.3}Cell Class Reference}{19}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Detailed Description}{20}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}Constructor \& Destructor Documentation}{20}{subsection.5.3.2}
\contentsline {subsubsection}{\numberline {5.3.2.1}Cell()\hspace {0.1cm}{\relax \fontsize {8}{9.5}\selectfont \abovedisplayskip 6\p@ plus2\p@ minus4\p@ \abovedisplayshortskip \z@ plus\p@ \belowdisplayshortskip 3\p@ plus\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 3\p@ plus\p@ minus\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \ttfamily [1/2]}}{20}{subsubsection.5.3.2.1}
\contentsline {subsubsection}{\numberline {5.3.2.2}Cell()\hspace {0.1cm}{\relax \fontsize {8}{9.5}\selectfont \abovedisplayskip 6\p@ plus2\p@ minus4\p@ \abovedisplayshortskip \z@ plus\p@ \belowdisplayshortskip 3\p@ plus\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 3\p@ plus\p@ minus\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \ttfamily [2/2]}}{21}{subsubsection.5.3.2.2}
\contentsline {subsubsection}{\numberline {5.3.2.3}$\sim $\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Cell()}{21}{subsubsection.5.3.2.3}
\contentsline {subsection}{\numberline {5.3.3}Member Function Documentation}{21}{subsection.5.3.3}
\contentsline {subsubsection}{\numberline {5.3.3.1}get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Army()}{21}{subsubsection.5.3.3.1}
\contentsline {subsubsection}{\numberline {5.3.3.2}get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Ocuped()}{21}{subsubsection.5.3.3.2}
\contentsline {subsubsection}{\numberline {5.3.3.3}get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Passable()}{22}{subsubsection.5.3.3.3}
\contentsline {subsubsection}{\numberline {5.3.3.4}print\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Cell\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Info()}{22}{subsubsection.5.3.3.4}
\contentsline {subsubsection}{\numberline {5.3.3.5}set\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Army()}{22}{subsubsection.5.3.3.5}
\contentsline {subsubsection}{\numberline {5.3.3.6}set\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Ocuped()}{22}{subsubsection.5.3.3.6}
\contentsline {subsubsection}{\numberline {5.3.3.7}set\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Passable()}{24}{subsubsection.5.3.3.7}
\contentsline {section}{\numberline {5.4}Field Class Reference}{24}{section.5.4}
\contentsline {subsection}{\numberline {5.4.1}Detailed Description}{25}{subsection.5.4.1}
\contentsline {subsection}{\numberline {5.4.2}Constructor \& Destructor Documentation}{25}{subsection.5.4.2}
\contentsline {subsubsection}{\numberline {5.4.2.1}Field()}{25}{subsubsection.5.4.2.1}
\contentsline {subsubsection}{\numberline {5.4.2.2}$\sim $\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Field()}{26}{subsubsection.5.4.2.2}
\contentsline {subsection}{\numberline {5.4.3}Member Function Documentation}{26}{subsection.5.4.3}
\contentsline {subsubsection}{\numberline {5.4.3.1}Cargar()}{26}{subsubsection.5.4.3.1}
\contentsline {subsubsection}{\numberline {5.4.3.2}get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Cell()}{26}{subsubsection.5.4.3.2}
\contentsline {subsubsection}{\numberline {5.4.3.3}get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Cow()}{26}{subsubsection.5.4.3.3}
\contentsline {subsubsection}{\numberline {5.4.3.4}get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Row()}{27}{subsubsection.5.4.3.4}
\contentsline {subsubsection}{\numberline {5.4.3.5}Guardar()}{27}{subsubsection.5.4.3.5}
\contentsline {subsubsection}{\numberline {5.4.3.6}print\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Tab()}{27}{subsubsection.5.4.3.6}
\contentsline {subsubsection}{\numberline {5.4.3.7}Refresh\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Celda()}{27}{subsubsection.5.4.3.7}
\contentsline {subsubsection}{\numberline {5.4.3.8}tab\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Inicial()}{28}{subsubsection.5.4.3.8}
\contentsline {section}{\numberline {5.5}Lancero Class Reference}{28}{section.5.5}
\contentsline {subsection}{\numberline {5.5.1}Detailed Description}{31}{subsection.5.5.1}
\contentsline {subsection}{\numberline {5.5.2}Constructor \& Destructor Documentation}{31}{subsection.5.5.2}
\contentsline {subsubsection}{\numberline {5.5.2.1}Lancero()\hspace {0.1cm}{\relax \fontsize {8}{9.5}\selectfont \abovedisplayskip 6\p@ plus2\p@ minus4\p@ \abovedisplayshortskip \z@ plus\p@ \belowdisplayshortskip 3\p@ plus\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 3\p@ plus\p@ minus\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \ttfamily [1/2]}}{31}{subsubsection.5.5.2.1}
\contentsline {subsubsection}{\numberline {5.5.2.2}Lancero()\hspace {0.1cm}{\relax \fontsize {8}{9.5}\selectfont \abovedisplayskip 6\p@ plus2\p@ minus4\p@ \abovedisplayshortskip \z@ plus\p@ \belowdisplayshortskip 3\p@ plus\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 3\p@ plus\p@ minus\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \ttfamily [2/2]}}{31}{subsubsection.5.5.2.2}
\contentsline {subsubsection}{\numberline {5.5.2.3}$\sim $\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Lancero()}{32}{subsubsection.5.5.2.3}
\contentsline {subsection}{\numberline {5.5.3}Member Function Documentation}{32}{subsection.5.5.3}
\contentsline {subsubsection}{\numberline {5.5.3.1}Attack()}{32}{subsubsection.5.5.3.1}
\contentsline {subsubsection}{\numberline {5.5.3.2}get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Cost()}{32}{subsubsection.5.5.3.2}
\contentsline {subsubsection}{\numberline {5.5.3.3}Move()}{33}{subsubsection.5.5.3.3}
\contentsline {subsection}{\numberline {5.5.4}Member Data Documentation}{33}{subsection.5.5.4}
\contentsline {subsubsection}{\numberline {5.5.4.1}cost}{33}{subsubsection.5.5.4.1}
\contentsline {section}{\numberline {5.6}Player Class Reference}{33}{section.5.6}
\contentsline {subsection}{\numberline {5.6.1}Detailed Description}{35}{subsection.5.6.1}
\contentsline {subsection}{\numberline {5.6.2}Constructor \& Destructor Documentation}{35}{subsection.5.6.2}
\contentsline {subsubsection}{\numberline {5.6.2.1}Player()\hspace {0.1cm}{\relax \fontsize {8}{9.5}\selectfont \abovedisplayskip 6\p@ plus2\p@ minus4\p@ \abovedisplayshortskip \z@ plus\p@ \belowdisplayshortskip 3\p@ plus\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 3\p@ plus\p@ minus\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \ttfamily [1/2]}}{36}{subsubsection.5.6.2.1}
\contentsline {subsubsection}{\numberline {5.6.2.2}$\sim $\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Player()}{36}{subsubsection.5.6.2.2}
\contentsline {subsubsection}{\numberline {5.6.2.3}Player()\hspace {0.1cm}{\relax \fontsize {8}{9.5}\selectfont \abovedisplayskip 6\p@ plus2\p@ minus4\p@ \abovedisplayshortskip \z@ plus\p@ \belowdisplayshortskip 3\p@ plus\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 3\p@ plus\p@ minus\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \ttfamily [2/2]}}{36}{subsubsection.5.6.2.3}
\contentsline {subsection}{\numberline {5.6.3}Member Function Documentation}{36}{subsection.5.6.3}
\contentsline {subsubsection}{\numberline {5.6.3.1}Atacar()}{36}{subsubsection.5.6.3.1}
\contentsline {subsubsection}{\numberline {5.6.3.2}Colocar\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Unit()}{36}{subsubsection.5.6.3.2}
\contentsline {subsubsection}{\numberline {5.6.3.3}colocar\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Units()}{37}{subsubsection.5.6.3.3}
\contentsline {subsubsection}{\numberline {5.6.3.4}create\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Army()}{37}{subsubsection.5.6.3.4}
\contentsline {subsubsection}{\numberline {5.6.3.5}get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Army()}{37}{subsubsection.5.6.3.5}
\contentsline {subsubsection}{\numberline {5.6.3.6}get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Army2()}{38}{subsubsection.5.6.3.6}
\contentsline {subsubsection}{\numberline {5.6.3.7}get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Baja()}{38}{subsubsection.5.6.3.7}
\contentsline {subsubsection}{\numberline {5.6.3.8}get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Guardar()}{38}{subsubsection.5.6.3.8}
\contentsline {subsubsection}{\numberline {5.6.3.9}get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Nombre()}{38}{subsubsection.5.6.3.9}
\contentsline {subsubsection}{\numberline {5.6.3.10}inicializar()}{38}{subsubsection.5.6.3.10}
\contentsline {subsubsection}{\numberline {5.6.3.11}Mostrar\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Info\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Todas\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Unidades()}{39}{subsubsection.5.6.3.11}
\contentsline {subsubsection}{\numberline {5.6.3.12}Mostrar\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Info\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Unidad()}{39}{subsubsection.5.6.3.12}
\contentsline {subsubsection}{\numberline {5.6.3.13}Mover()}{39}{subsubsection.5.6.3.13}
\contentsline {subsubsection}{\numberline {5.6.3.14}play()}{39}{subsubsection.5.6.3.14}
\contentsline {subsubsection}{\numberline {5.6.3.15}set\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Baja()}{39}{subsubsection.5.6.3.15}
\contentsline {subsubsection}{\numberline {5.6.3.16}set\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Guardar()}{40}{subsubsection.5.6.3.16}
\contentsline {subsubsection}{\numberline {5.6.3.17}set\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Nombre()}{40}{subsubsection.5.6.3.17}
\contentsline {subsubsection}{\numberline {5.6.3.18}set\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Vector()}{40}{subsubsection.5.6.3.18}
\contentsline {subsection}{\numberline {5.6.4}Member Data Documentation}{40}{subsection.5.6.4}
\contentsline {subsubsection}{\numberline {5.6.4.1}army}{40}{subsubsection.5.6.4.1}
\contentsline {subsubsection}{\numberline {5.6.4.2}max\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Cost}{41}{subsubsection.5.6.4.2}
\contentsline {section}{\numberline {5.7}UI Class Reference}{41}{section.5.7}
\contentsline {subsection}{\numberline {5.7.1}Detailed Description}{42}{subsection.5.7.1}
\contentsline {subsection}{\numberline {5.7.2}Constructor \& Destructor Documentation}{42}{subsection.5.7.2}
\contentsline {subsubsection}{\numberline {5.7.2.1}U\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}I()}{42}{subsubsection.5.7.2.1}
\contentsline {subsubsection}{\numberline {5.7.2.2}$\sim $\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}U\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}I()}{42}{subsubsection.5.7.2.2}
\contentsline {subsection}{\numberline {5.7.3}Member Function Documentation}{42}{subsection.5.7.3}
\contentsline {subsubsection}{\numberline {5.7.3.1}cargar()}{43}{subsubsection.5.7.3.1}
\contentsline {subsubsection}{\numberline {5.7.3.2}get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Army1()}{43}{subsubsection.5.7.3.2}
\contentsline {subsubsection}{\numberline {5.7.3.3}get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Army2()}{43}{subsubsection.5.7.3.3}
\contentsline {subsubsection}{\numberline {5.7.3.4}get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Per1()}{43}{subsubsection.5.7.3.4}
\contentsline {subsubsection}{\numberline {5.7.3.5}get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Per2()}{44}{subsubsection.5.7.3.5}
\contentsline {subsubsection}{\numberline {5.7.3.6}guardar\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}J()}{44}{subsubsection.5.7.3.6}
\contentsline {subsubsection}{\numberline {5.7.3.7}Juego()}{44}{subsubsection.5.7.3.7}
\contentsline {subsubsection}{\numberline {5.7.3.8}Lista\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Partidas()}{44}{subsubsection.5.7.3.8}
\contentsline {subsubsection}{\numberline {5.7.3.9}Menu\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Principal()}{45}{subsubsection.5.7.3.9}
\contentsline {subsubsection}{\numberline {5.7.3.10}Mostrar\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Info\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Juego()}{45}{subsubsection.5.7.3.10}
\contentsline {subsubsection}{\numberline {5.7.3.11}Nueva\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Partida()}{45}{subsubsection.5.7.3.11}
\contentsline {section}{\numberline {5.8}Unit Class Reference}{45}{section.5.8}
\contentsline {subsection}{\numberline {5.8.1}Detailed Description}{49}{subsection.5.8.1}
\contentsline {subsection}{\numberline {5.8.2}Constructor \& Destructor Documentation}{49}{subsection.5.8.2}
\contentsline {subsubsection}{\numberline {5.8.2.1}Unit()}{49}{subsubsection.5.8.2.1}
\contentsline {subsubsection}{\numberline {5.8.2.2}$\sim $\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Unit()}{49}{subsubsection.5.8.2.2}
\contentsline {subsection}{\numberline {5.8.3}Member Function Documentation}{49}{subsection.5.8.3}
\contentsline {subsubsection}{\numberline {5.8.3.1}Attack()}{49}{subsubsection.5.8.3.1}
\contentsline {subsubsection}{\numberline {5.8.3.2}get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}A\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}T\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}K()}{50}{subsubsection.5.8.3.2}
\contentsline {subsubsection}{\numberline {5.8.3.3}get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}D\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}E\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}F()}{50}{subsubsection.5.8.3.3}
\contentsline {subsubsection}{\numberline {5.8.3.4}get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}E\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}X\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}P()}{50}{subsubsection.5.8.3.4}
\contentsline {subsubsection}{\numberline {5.8.3.5}get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}H\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}P()}{51}{subsubsection.5.8.3.5}
\contentsline {subsubsection}{\numberline {5.8.3.6}get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}H\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}P\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}M()}{51}{subsubsection.5.8.3.6}
\contentsline {subsubsection}{\numberline {5.8.3.7}get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}I\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}D()}{51}{subsubsection.5.8.3.7}
\contentsline {subsubsection}{\numberline {5.8.3.8}get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Jug()}{51}{subsubsection.5.8.3.8}
\contentsline {subsubsection}{\numberline {5.8.3.9}get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Level()}{52}{subsubsection.5.8.3.9}
\contentsline {subsubsection}{\numberline {5.8.3.10}get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Movement()}{52}{subsubsection.5.8.3.10}
\contentsline {subsubsection}{\numberline {5.8.3.11}get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}N\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}A\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}M\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}E()}{52}{subsubsection.5.8.3.11}
\contentsline {subsubsection}{\numberline {5.8.3.12}get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Pos\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}X()}{52}{subsubsection.5.8.3.12}
\contentsline {subsubsection}{\numberline {5.8.3.13}get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Pos\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Y()}{53}{subsubsection.5.8.3.13}
\contentsline {subsubsection}{\numberline {5.8.3.14}get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Range()}{53}{subsubsection.5.8.3.14}
\contentsline {subsubsection}{\numberline {5.8.3.15}get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Type()}{53}{subsubsection.5.8.3.15}
\contentsline {subsubsection}{\numberline {5.8.3.16}less\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}H\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}P()}{53}{subsubsection.5.8.3.16}
\contentsline {subsubsection}{\numberline {5.8.3.17}Move()}{54}{subsubsection.5.8.3.17}
\contentsline {subsubsection}{\numberline {5.8.3.18}set\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}A\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}T\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}K()}{54}{subsubsection.5.8.3.18}
\contentsline {subsubsection}{\numberline {5.8.3.19}set\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}D\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}E\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}F()}{54}{subsubsection.5.8.3.19}
\contentsline {subsubsection}{\numberline {5.8.3.20}set\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}E\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}X\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}P()}{55}{subsubsection.5.8.3.20}
\contentsline {subsubsection}{\numberline {5.8.3.21}set\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}H\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}P()}{55}{subsubsection.5.8.3.21}
\contentsline {subsubsection}{\numberline {5.8.3.22}set\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}H\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}P\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}M()}{55}{subsubsection.5.8.3.22}
\contentsline {subsubsection}{\numberline {5.8.3.23}set\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}I\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}D()}{56}{subsubsection.5.8.3.23}
\contentsline {subsubsection}{\numberline {5.8.3.24}set\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Jug()}{56}{subsubsection.5.8.3.24}
\contentsline {subsubsection}{\numberline {5.8.3.25}set\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Level()}{56}{subsubsection.5.8.3.25}
\contentsline {subsubsection}{\numberline {5.8.3.26}set\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Movement()}{57}{subsubsection.5.8.3.26}
\contentsline {subsubsection}{\numberline {5.8.3.27}set\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}N\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}A\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}M\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}E()}{57}{subsubsection.5.8.3.27}
\contentsline {subsubsection}{\numberline {5.8.3.28}set\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Pos\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}X()}{57}{subsubsection.5.8.3.28}
\contentsline {subsubsection}{\numberline {5.8.3.29}set\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Pos\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Y()}{58}{subsubsection.5.8.3.29}
\contentsline {subsubsection}{\numberline {5.8.3.30}set\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Range()}{58}{subsubsection.5.8.3.30}
\contentsline {subsubsection}{\numberline {5.8.3.31}set\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Type()}{58}{subsubsection.5.8.3.31}
\contentsline {subsubsection}{\numberline {5.8.3.32}subir\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Nivel()}{58}{subsubsection.5.8.3.32}
\contentsline {chapter}{\numberline {6}File Documentation}{61}{chapter.6}
\contentsline {section}{\numberline {6.1}Arquero.\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}h File Reference}{61}{section.6.1}
\contentsline {section}{\numberline {6.2}Caballo.\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}h File Reference}{62}{section.6.2}
\contentsline {section}{\numberline {6.3}Cell.\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}h File Reference}{63}{section.6.3}
\contentsline {section}{\numberline {6.4}Field.\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}h File Reference}{64}{section.6.4}
\contentsline {section}{\numberline {6.5}header.\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}h File Reference}{65}{section.6.5}
\contentsline {section}{\numberline {6.6}Lancero.\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}h File Reference}{66}{section.6.6}
\contentsline {section}{\numberline {6.7}Player.\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}h File Reference}{67}{section.6.7}
\contentsline {section}{\numberline {6.8}U\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}I.\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}h File Reference}{68}{section.6.8}
\contentsline {section}{\numberline {6.9}Unit.\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}h File Reference}{69}{section.6.9}
