var searchData=
[
  ['setarmy',['setArmy',['../classCell.html#a464009f5ff2ebd200b11cf6cc7167114',1,'Cell']]],
  ['setatk',['setATK',['../classUnit.html#a8bf707b5276114f075f3b021e549d131',1,'Unit']]],
  ['setbaja',['setBaja',['../classPlayer.html#a8fdabd6cf7eda8ec9b5e91b96dfe00b0',1,'Player']]],
  ['setdef',['setDEF',['../classUnit.html#aaab2457c20bd1d63ec8366b574f17f1d',1,'Unit']]],
  ['setexp',['setEXP',['../classUnit.html#aad09abdfa6752876f58ed770dec3f912',1,'Unit']]],
  ['setguardar',['setGuardar',['../classPlayer.html#af79bc6c5e76ff2da23e06ff2f837b84c',1,'Player']]],
  ['sethp',['setHP',['../classUnit.html#a2c8df7e954b50902a054e6ccfc7bdfe0',1,'Unit']]],
  ['sethpm',['setHPM',['../classUnit.html#a90e706153fec90515e285037761d72e3',1,'Unit']]],
  ['setid',['setID',['../classUnit.html#a41252f75f39f198f20edb83cc2b11f42',1,'Unit']]],
  ['setjug',['setJug',['../classUnit.html#aabd5a5c2d0057e634f04303af3a96b97',1,'Unit']]],
  ['setlevel',['setLevel',['../classUnit.html#ae0da8782d33440e2d270fcdbb287e488',1,'Unit']]],
  ['setmovement',['setMovement',['../classUnit.html#a33c610b71fd95aa0a2951515a1bc3c8f',1,'Unit']]],
  ['setname',['setNAME',['../classUnit.html#aec66b575c34b7a045ceddbb8fb846dbf',1,'Unit']]],
  ['setnombre',['setNombre',['../classPlayer.html#a6edd818c005bbfc1fc3ea0c8f4b1e7f2',1,'Player']]],
  ['setocuped',['setOcuped',['../classCell.html#ac00d6d2b83d0dbd306aed50276a1c825',1,'Cell']]],
  ['setpassable',['setPassable',['../classCell.html#a0c067ef99032f0e0fc0c63c0090d8195',1,'Cell']]],
  ['setposx',['setPosX',['../classUnit.html#a22b4afa88216969ad2dc564c0aafd27e',1,'Unit']]],
  ['setposy',['setPosY',['../classUnit.html#a346b96df9cd9001f4d48c8c7907c02e1',1,'Unit']]],
  ['setrange',['setRange',['../classUnit.html#a9ab1aa89d13826495af9c79d2ae9f9f8',1,'Unit']]],
  ['settype',['setType',['../classUnit.html#af04dac030da3e579c77f83b3bc3abf05',1,'Unit']]],
  ['setvector',['setVector',['../classPlayer.html#acf5c848bf07db76f276c36221591d86c',1,'Player']]],
  ['subirnivel',['subirNivel',['../classUnit.html#a7152aebc583e95ace27678ffbee975fd',1,'Unit']]]
];
