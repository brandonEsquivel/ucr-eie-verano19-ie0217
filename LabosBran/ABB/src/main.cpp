//INCLUDES

#include "../include/Nodo.h"

#include<iostream>
using namespace std;

int main(){
   
    Arbol<int> arbol;


    //1er comprovacion. Eliminar arbol vacio
    

  //    FUNCIONA------------------------------
    cout<<"\n \t-> Ingresando nodo raiz<-"<<endl;
    
    arbol.insert(10); 

    cout<<"\n \tBuscando datos";
    arbol.find(10,arbol.getArbol());
    arbol.find(5,arbol.getArbol());

    cout<<"\n\tMostrando árbol\n";
    arbol.inOrden(arbol.getArbol()); 

    cout<<"\n\n \tBorrando Dato"<<endl;
    arbol.remove1(5);

    arbol.borrarArbol(arbol.getArbol());
    //---------------------------------------------------

    cout<<"Ingresando datos para el arbol\n"<<endl;
    arbol.insert(10);
   
    arbol.insert(7);

    arbol.insert(4);
    arbol.insert(3);
    arbol.insert(5);
    arbol.insert(8);
    arbol.insert(9);
    arbol.insert(12);
    arbol.insert(11);
    arbol.insert(17);

    arbol.insert(14);
    arbol.insert(13);
    arbol.insert(15);

    arbol.insert(20);
    arbol.insert(21);
    arbol.insert(22);
    
    
    cout<<"\nInOrden"<<endl;
    arbol.inOrden(arbol.getArbol());
    
    cout<<"\n PreOrden"<<endl;
    arbol.preOrden(arbol.getArbol());

    cout<<"\nPostOrden"<<endl;
    arbol.postOrden(arbol.getArbol());
    cout<<"\n";

    cout<<"\n \nDato mas grande de los pequeños"<<endl;
    arbol.findLargestToTheLeft(arbol.getArbol());

    cout<<"\n Dato mas pequeño de los grandes"<<endl;
    arbol.findSmallestToTheRight (arbol.getArbol());
 //FUNCUINA ------------------------------111111111111111

    cout<<"\n Borrando HOJAS: 3 y 22\n"<<endl;
    cout<<"Arbol antes de APODAR"<<endl;
    arbol.preOrden(arbol.getArbol());
    arbol.remove1(3);
    arbol.remove1(22);
    cout<<"\n PreOrden"<<endl;
    arbol.preOrden(arbol.getArbol());  

    cout<<"\n \n\t ++Borrando Arbol++";
    arbol.borrarArbol(arbol.getArbol());
    //FUNCUINA ------------------------------111111111111111


    cout<<"\n Nuevo Arbol\n";
    arbol.insert(30);
    arbol.insert(28);
    arbol.insert(29);
    arbol.insert(42);
    arbol.insert(38);
    arbol.insert(39);
    arbol.insert(35);
    arbol.insert(33);
    arbol.insert(45);
    arbol.insert(43);
    arbol.insert(49);

    
    
    cout<<"\n Borrando Nodo con un solo hijo: 28 y 35\n"<<endl;
    
    cout<<"\n PreOrden"<<endl;
    arbol.preOrden(arbol.getArbol());  
    arbol.remove1(28);
    arbol.remove1(35);
    cout<<"\n PreOrden"<<endl;
    arbol.preOrden(arbol.getArbol());


    
    cout<<"\n\n\t Borrando Nodo con dos hijos: 42\n"<<endl;
    cout<<"\n PreOrden"<<endl;
    arbol.preOrden(arbol.getArbol());
    arbol.remove1(42);
    cout<<"\n PreOrden"<<endl;
    arbol.preOrden(arbol.getArbol());

    cout<<"\n \n \t Borrando Nodo raiz: 30\n"<<endl;
    cout<<"\n PreOrden"<<endl;
    arbol.preOrden(arbol.getArbol());
    arbol.remove1(30);
    cout<<"\n PreOrden"<<endl;
    arbol.preOrden(arbol.getArbol());

    

    cout<<"\nELIMINANDO ARBOL\n";
    arbol.borrarArbol(arbol.getArbol());


   

    return 0;
}