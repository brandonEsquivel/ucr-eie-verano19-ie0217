#include<iostream>
using namespace std;
/** 
 * @authors Brandon Esquivel, Yassir Wagon, Reinier Camacho
 * @date Febrero 2020
 * @warning UCR EIE Version BETA del Proyecto 0
 * @mainpage Proyecto 1- Parte 1: Árboles BST
 * @brief Implementacion de la primera parte del Proyecto 1 del curso IE0217 Estructuras abstractas y algoritmos para ingeniería.
 * @section intro_sec Introduccion
 * En el presente codigo se muestra la solucion implementada del proyecto 11- parte 1 del curso IE0217 Estructuras abstractas y algoritmos para ingeniería, de la escuela de Ingenieria Electrica , EIE, UCR.
 * El objetivo de este proyecto es implementar la estructura de datos abstractos Tree utilizando un Árbol Binario de Búsqueda haciendo uso de objetos y usando el lenguaje de programacion C++. 
 * @section compile_sec Compilacion y ejecucion
 * Ejecute el comando make en la carpeta raiz, luego, para correr ejecute make run o bien ./bin/main
 * @section Documentation_sec Documentacion
 * Aparte de este HTLM podra encontrar un pdf en doc/latex/refman.pdf latex con toda la informacion detallada. Si no encuentra el pdf, utilize el comando make en esa carpeta para generarlo.
 * **/ 

//*

template<typename T>
class Arbol;


/** @brief Implementacion/definicion de la clase Nodo que Representa un nodo del BST.
    @author Reinier Camacho Quirós
    @date February 2020
    */
template<typename T>
class Nodo{
    private:
        T dato; /** Dato del nodo*/
        Nodo<T> *izq;/** Puntero al hijo izquierdo del nodo*/
        Nodo<T> *der;/** Puntero al hijo derecho del nodo*/
        Nodo<T> *ances;/** Puntero al ancestro del nodo*/
        friend class Arbol<T>;
    public:
             
        /** Default constructor. 
         * @brief construye el objeto Nodo */
        Nodo();
        //Nodo(int dato);

        /** Default destructor 
         * @brief Destruye el objeto Nodo */
        ~Nodo();

};

template <typename T>
Nodo<T>::Nodo(){
    this->izq=NULL;
    this->der=NULL;
    this->ances=NULL;
    this->dato=-1;
}


template <typename T>
Nodo<T>::~Nodo(){}


/** @brief Implementacion/definicion de la clase Arbol que Representa el contenedor para el BST.
    @author Reinier Camacho Quirós
    @date February 2020
    */
template <typename T>
class Arbol
{
private:
   
    Nodo<T> *arbol;/**Puntero de tipo nodo. Apunta al inicio del arbol.*/
public:
        /** Default constructor. 
         * @brief construye el objeto Árbol */
    Arbol();
        /** Default destructor. 
         * @brief Destruye el objeto Árbol */
    ~Arbol();
    
    /** Metodo para insertar un nodo.
    * @brief Metodo para insertar nodo cuando existe raiz
    * @param n - dato a ingresar
    * @param ar- puntero a un nodo en particular */
    void insert1(T n, Nodo<T> *ar);

    /** Metodo para insertar un nodo.
    * @brief Metodo para insertar nodo raiz
    * @param n - dato a ingresar
    */
    void insert(T n);

    /** Metodo para eliminar un nodo.
    * @brief Metodo para un nodo.
    * @param d - dato a ingresar
    * @param ar- puntero a un nodo en particular */
    void remove(T d,Nodo<T> *ar);

    /** Metodo para eliminar  un nodo.
    * @brief Metodo para eliminar nodo, verifica si nodo es null
    * @param d - dato a ingresar
    */
    void remove1(T d);

    /** Metodo para encontrar un nodo.
    * @brief Metodo para encontrar un nodo 
    * @param d - dato a buscar
    * @param ar- puntero a un nodo en particular 
    * @return retorna true o false según lo a encontrado o no*/
    bool find(T d,Nodo<T> *ar);

    /** Metodo para encontrar un nodo.
    * @brief Metodo para encontrar el nodo mas grande de los pequeños
    * @param n- puntero a un nodo en particular */
    void findLargestToTheLeft(Nodo<T>* n);

    /** Metodo para encontrar un nodo.
    * @brief Metodo para encontrar el nodo mas pequeño de los grandes
    * @param n- puntero a un nodo en particular 
    */
    void findSmallestToTheRight(Nodo<T>* n);

    /** Tipo de recorrido para mostrar el árbol.
    * @brief Metodo para mostrar todos los nodos en forma preOrden
    * @param ar- puntero a un nodo en particular */
    void preOrden(Nodo<T> *ar);

    /** Tipo de recorrido para mostrar el árbol.
    * @brief Metodo para mostrar todos los nodos en forma inOrden
    * @param ar- puntero a un nodo en particular */
    void inOrden(Nodo<T> *ar); 

    /** Tipo de recorrido para mostrar el árbol.
    * @brief Metodo para mostrar todos los nodos en forma postOrden
    * @param ar- puntero a un nodo en particular */
    void postOrden(Nodo<T> *ar);

    /** Llama a la funcion inOrden.
    * @brief Metodo para llamar al método inOrden*/
    void inOrden1();

    /** Borra todo el árbol.
    * @brief Metodo para borrar todo del árbol
    * @param ar- puntero a un nodo en particular */
    void borrarArbol(Nodo<T>* ar); 

    /** Metodo que retorna el inicio del arbol
    * @brief Metodo para retornar un pntero al inicio del árbol
    * @return puntero al inicio del árbol*/
    Nodo<T>* getArbol(); 
    
    /** Metodo para encontrar el nodo más grande a la derecha de un nodo en particular.
    * @brief Metodo para encontrar el nodo mas grande a la derecha de un nodo en particular
    * @param n- puntero a un nodo en particular
    * @return retorna el nodo mas grande a la derecha.*/
    Nodo<T>* masGrandeDerecha(Nodo<T>* n);//retorna el nodo mas grande a la derecha.


};
template <typename T>
Arbol<T>::Arbol(){
    this->arbol=NULL;
    
}

/*template <typename T>
Arbol<T>::Arbol(Nodo<T> *n){
    this->arbol=n;
}*/
template <typename T>
Arbol<T>::~Arbol(){}

template <typename T>
Nodo<T>* Arbol<T>::getArbol(){
    //cout<<"Direccion de la raiz del arbol: "<<this->arbol<<endl;
    return this->arbol;
}


template <typename T>
bool Arbol<T>::find(T d,Nodo<T> *ar){    
    if(ar==NULL){
        cout<<"\n Dato "<<d<<" No encontrado"<<endl;
        return false;
    }
    if (ar->dato==d){
        cout<<"\n Dato "<<d<<" Encontrado "<<endl;
        return true;
    }
     
    else
    {
        if(d<ar->dato){
            return find(d,ar->izq);
        }
        else
        {
            return find(d,ar->der);
        }
        
    }    
    
}
template <typename T>
void Arbol<T>::insert(T d){
    if (this->arbol==NULL){
       
        Nodo<T> *nuevo_nodo=new(Nodo<T>);
        nuevo_nodo->dato=d; 
        nuevo_nodo->ances=NULL;
        nuevo_nodo->der=NULL;
        nuevo_nodo->izq=NULL;
        this->arbol=nuevo_nodo; 
        cout<<"Se ingresó: "<<this->arbol->dato<<". NODO RAIZ."<<endl;
        //cout<<"Direccion del arbol con el primer nodo: "<<this->arbol<<endl; 
    }else
    {
        //cout<<" arbol no vacio"<<endl;
        Nodo<T>* aux=this->arbol;
        insert1(d,aux);
    }
    
}


template <typename T>
void Arbol<T>::insert1(T n,Nodo<T> *ar){
    //Falta crear apuntador al ancestro   
    if(ar==NULL){
        cout<<"No debe ingresar aqui"<<endl;
        /*
        cout<<"Primer NODO. La RAIZ"<<endl;
        Nodo<T> *nuevo_nodo=new(Nodo<T>);
        nuevo_nodo->dato=n;
        //nuevo_nodo->ances=ar; 
        ar=nuevo_nodo;  
        */ 
    }
    else
    {
        //cout<<"Arbol NO vacio"<<endl;
        //cout<<"dato del nodo "<<ar->dato<<endl;
        if(n<ar->dato){
            //cout<<"dato es menor"<<endl;
            if(ar->izq==NULL){/*Saber de antemano si hijo izquierdo es nullo, */  
            Nodo<T> *nuevo_nodo=new(Nodo<T>);
            nuevo_nodo->dato=n;
            nuevo_nodo->ances=ar; 
            ar->izq=nuevo_nodo;
            cout<<"Se ingresó: "<<ar->izq->dato<<endl;
            //cout<<"DATO A INGRESAR "<<n<<" DATO INGRESADO EN HIJO "<<ar->izq->dato<<" DATO ANCESTRO "<<ar->izq->ances->dato<<endl;
            //cout<<"Direccion de arbol: "<<this->arbol<<" "<<ar<<endl;
            }else
            {
                insert1(n,ar->izq);
            }
            
            
        }
        else
        {
            //cout<<"Dato es mayor"<<endl;
            if(ar->der==NULL){/*Saber de antemano si hijo derecho es nullo, */  
            Nodo<T> *nuevo_nodo=new(Nodo<T>);
            nuevo_nodo->dato=n;
            nuevo_nodo->ances=ar; 
            ar->der=nuevo_nodo;
            cout<<"Se ingresó: "<<ar->der->dato<<endl;
            //cout<<"DATO A INGRESAR "<<n<<" DATO INGRESADO EN HIJO "<<ar->der->dato<<" DATO ANCESTRO "<<ar->der->ances->dato<<endl;
            //cout<<"Direccion de arbol: "<<this->arbol<<" "<<ar<<endl;
            }else
            {
                insert1(n,ar->der);
            }
                        
        }        
    }    
}
template <typename T>
void Arbol<T>::preOrden(Nodo<T> *ar){
    if(ar!=NULL){
        //cout<<ar->dato<<" ";
        preOrden(ar->izq);
        cout<<ar->dato<<" ";
        preOrden(ar->der);
    }//else{cout<<endl;}
}

template <typename T>
void Arbol<T>::inOrden1(){
    inOrden(this->arbol);
}
template <typename T>
void Arbol<T>::inOrden(Nodo<T> *ar){
    if(ar!=NULL){
        cout<<ar->dato<<" ";
        inOrden(ar->izq);
        //cout<<ar->dato<<" ";        
        inOrden(ar->der);
    }//else{cout<<endl;}
}
template <typename T>
void Arbol<T>::postOrden(Nodo<T> *ar){
    if(ar!=NULL){
        postOrden(ar->der);
        cout<<ar->dato<<" ";
        postOrden(ar->izq);
        //cout<<ar->dato<<" ";           
    }//else{cout<<endl;}
}

template <typename T>
void Arbol<T>::remove1(T d){
    if(this->arbol==NULL){
        cout<<"****El árbol está vacio, no se puede borrar****"<<endl;
    }
    else
    {
       if(find(d,getArbol())){
           remove(d,getArbol());
       }else
       {
           cout<<"No se pudo borrar\n"<<endl;
       }
       
       
    }
    
}
template <typename T>
void Arbol<T>::remove(T d,Nodo<T> *ar){

    
    if(d<ar->dato){
        remove(d,ar->izq);
    }   
    else if(d>ar->dato)
    {
         remove(d,ar->der);
    }
    else if(ar->dato==d && ar->izq==NULL && ar->der==NULL &&ar->ances!=NULL){//Primer caso si es hoja.
        if(ar->dato<ar->ances->dato){ //Si dato de nodo es menor que dato de ancestro, es hijo izquierdo.
            ar->ances->izq=NULL;
            delete ar;
            cout<<"Eliminado."<<endl;
        }
        else //si no, es hijo derecho.
        {
            ar->ances->der=NULL;
            delete ar;
            cout<<"Eliminado."<<endl;
        }       

    }else if(ar->dato==d && (ar->izq==NULL || ar->der==NULL )){//Si es nodo con un solo hijo y no es la raiz
        if(ar->ances==NULL){//ES LA RAIZ
            if(ar->der==NULL){//Solo hijo izquierdo
                Nodo<T>*aux=ar;
                ar->izq->ances=NULL;
                this->arbol=ar->izq;
                delete aux;
                cout<<"Eliminado."<<endl;
            }
            else{ //Solo hijo derecho
                Nodo<T>*aux=ar;
                ar->der->ances=NULL;
                this->arbol=ar->der;
                delete aux;
                cout<<"Eliminado."<<endl;

            }
        }
        else if(ar->izq==NULL){//SOLO HIJO DERECHO
            if(ar->dato<ar->ances->dato){//Es hijo izquierdo de  ancestro
                ar->der->ances=ar->ances;//El puntero de mi hijo apunta a mi padre.
                ar->ances->izq=ar->der;
                delete ar;
                cout<<"Eliminado."<<endl;
            }else{//Es hijo derecho de  ancestro
                ar->der->ances=ar->ances;//El puntero de mi hijo apunta a mi padre.
                ar->ances->der=ar->der;
                delete ar;
                cout<<"Eliminado."<<endl;

            }
            
        
        }else{//SOLO HIJO IZQUIERDO
            if(ar->dato<ar->ances->dato){//Es hijo izquierdo de  ancestro
                ar->izq->ances=ar->ances;//El puntero de mi hijo apunta a mi padre.
                ar->ances->izq=ar->izq;
                delete ar;
                cout<<"Eliminado."<<endl;
            }else{//Es hijo derecho de  ancestro
                ar->izq->ances=ar->ances;//El puntero de mi hijo apunta a mi padre.
                ar->ances->der=ar->izq;
                delete ar;
                cout<<"Eliminado."<<endl;

            }

        }
    }else if(ar->dato==d && (ar->izq!=NULL && ar->der!=NULL )){//si nodo tiene dos hijos.
        if(ar->ances==NULL){ //raiz
            Nodo<T> *aux =masGrandeDerecha(ar->izq);//Nodo mas a la derecha del menor hijo a subir
            ar->izq->ances=NULL;
            aux->der=ar->der;//agrego rama derecha del nodo a borrar al nodo mas grande del subarbol del hijo izquierdo del nodo a borrar.
            this->arbol=ar->izq;
            delete ar;
            
            cout<<"Eliminado"<<endl;

        }
        else{
            Nodo<T> *aux =masGrandeDerecha(ar->izq);//Nodo mas a la derecha del menor hijo a subir
            //Nodo<T> *aux2=ar;
            if(ar->dato>ar->ances->dato){ ///Este hijo es derecho
                ar->ances->der=ar->izq; //Conexion abuelo con nieto
                ar->izq->ances=ar->ances;//conexion nieto con abuelo
                aux->der=ar->der;//agrego rama derecha del nodo a borrar al nodo mas grande del subarbol del hijo izquierdo del nodo a borrar.
                delete ar;
                cout<<"Eliminado"<<endl;

            }
            else{
                ar->ances->izq=ar->izq; //Conexion abuelo con nieto
                ar->izq->ances=ar->ances;//conexion nieto con abuelo
                aux->der=ar->der;//agrego rama derecha del nodo a borrar al nodo mas grande del subarbol del hijo izquierdo del nodo a borrar.
                delete ar;
                cout<<"Eliminado"<<endl;
                

            }

        }
        
        
    }

   
            
     

}
template <typename T>
void Arbol<T>::findLargestToTheLeft(Nodo<T>* n){
    if(n->ances==NULL){
        findLargestToTheLeft(n->izq);
    }
    else{
        if(n->der==NULL){
            cout<<"Dato más grande: "<<n->dato<<endl;
        }else{
             findLargestToTheLeft(n->der);
        }
    }
   
}
template <typename T>
void Arbol<T>::findSmallestToTheRight(Nodo<T>* n){
    if(n->ances==NULL){
        findSmallestToTheRight(n->der);
    }
    else{
        if(n->izq==NULL){
            cout<<"Dato más pequeño: "<<n->dato<<endl;
        }else{
             findSmallestToTheRight(n->izq);
        }
    }
}
template <typename T>
Nodo<T>* Arbol<T>::masGrandeDerecha(Nodo<T>* n){
    if(n->der==NULL){return n;}
    else
    {
       return masGrandeDerecha(n->der);
    }    
}

template <typename T>
void Arbol<T>::borrarArbol(Nodo<T>* ar){
    if(ar!=NULL){
        borrarArbol(ar->izq);
        borrarArbol(ar->der);
        cout<<"\n DATO A BORRAR: "<<ar->dato<<endl;
        if(ar->ances==NULL){
            cout<<"Borrando raiz"<<endl;
            if(ar==this->arbol){
                cout<<"\n *EXITO*"<<endl;
                delete ar;
                this->arbol=NULL;
            }
            
        }
        else if(ar->dato<ar->ances->dato){//dato a borrar es hijo izquierdo.
            delete ar;
            ar->ances->izq=NULL;
            //cout<<"DATO BORRADO\n"<<endl;
        }
        else //hijo derecho
        {
            delete ar;
            ar->ances->der=NULL;
            //cout<<"DATO BORRADO"<<endl;
        }
        
        inOrden(getArbol());
        //cout<<"Direccion de ar; "<<ar<<"Direccion de ARBOL: "<<this->arbol<<endl;
    }else{cout<<"  "<<endl;}
}