
//INCLUDES

#include "../include/Electric.h"


//DEFINES
using namespace std;

Electric::Electric(){

}
Electric::~Electric(){
    //Do nothing
}

string Electric::Type(){
    return "Electric";
}

string Electric::weakVs(){

    return "Fire";
}

string Electric::strongVs(){
    return "Water";
}

bool Electric::isWeakVS(string vs){
    bool weak = false;
    if(vs==weakVs()){
        weak = true;
        return weak;
    }
    return weak;
}

bool Electric::isStrongVS(string vs){
    bool strong = false;
    if(vs==strongVs()){
        strong = true;
        return strong;
    }

    return strong;
}