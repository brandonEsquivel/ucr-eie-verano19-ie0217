

//INCLUDES

#include "../include/Zapdos.h"
#include "../include/Moltress.h"
#include "../include/Arcticuno.h"


//DEFINES

using namespace std;



int main(){

// Creando pokemons
Zapdos zapdos;
Arcticuno arcticuno;
Moltress moltress;

char eleccion;
char tecla;


inicio_menu:
    cout << "\t*************** Inicio de Batalla legendaria!**********\n";
    cout << "Seleccione a su pokemon preferido: \nA. Moltress\nB. Arcticuno\nC. Zapdos\n";
    cin >> eleccion;
    if(eleccion != 'A' && eleccion != 'B' && eleccion != 'C'){
        cout << " No eligio una opcion correcta! Seleccione A, B o C\n";
        goto inicio_menu;
    }
    switch (eleccion)
    {
    case 'A':
                                segundo:
            cout << "Que desea hacer con Moltress?\nA. Mostrar informacion\nB. Batallar\nC. SALIR";
            cin >> eleccion;
            if(eleccion != 'A' && eleccion != 'B' && eleccion != 'C'){
            cout << " No eligio una opcion correcta! Seleccione A, B o C\n";
            goto inicio_menu;
            }
            switch (eleccion)
            {
                case 'A':
                        moltress.printInfo();
                        goto segundo;
    
                case 'B':
                        cout << "Batalla contra Arcticuno!!\n";
                        moltress.printInfo();
                        cout<< endl<<"Tecla para continuar."<<endl;
                        cin >> tecla;
                        arcticuno.printInfo();
                        cout<< endl<<"Tecla para continuar."<<endl;
                        cin >> tecla;
                        cout << "INICIA!\n"; 
                        arcticuno.ATK1(moltress);
                        cout<< endl<<"Tecla para continuar."<<endl;
                        cin >> tecla;
                        moltress.ATK1(arcticuno);
                        cout<< endl<<"Tecla para continuar."<<endl;
                        cin >> tecla;
                        arcticuno.ATK2(moltress);
                        cout<< endl<<"Tecla para continuar."<<endl;
                        cin >> tecla;
                        moltress.printInfo();
                        cout<< endl<<"Tecla para continuar."<<endl;
    cin >> tecla;
                        moltress.ATK2(arcticuno);
                        cout<< endl<<"Tecla para continuar."<<endl;
    cin >> tecla;            
                        arcticuno.printInfo();
                        cout<< endl<<"Tecla para continuar."<<endl;
    cin >> tecla;
                        arcticuno.ATK4(moltress);
                        cout<< endl<<"Tecla para continuar."<<endl;
    cin >> tecla;
                        moltress.ATK4(arcticuno);
                        cout<< endl<<"Tecla para continuar."<<endl;
    cin >> tecla;
                        cout << "Arcticuno ha Huido. Fin de la batalla\n";
                        moltress.print();
                        goto segundo;
                case 'C':
                        cout << "saliendo...\n";
                        goto salir;
             }

    
    case 'B':
                                segundob:
            cout << "Que desea hacer con Arcticuno?\nA. Mostrar informacion\nB. Batallar\nC. SALIR";
            cin >> eleccion;
            if(eleccion != 'A' && eleccion != 'B' && eleccion != 'C'){
            cout << " No eligio una opcion correcta! Seleccione A, B o C\n";
            goto inicio_menu;
            }
            switch (eleccion)
            {
                case 'A':
                        arcticuno.printInfo();
                        goto segundob;
    
                case 'B':
                        cout << "Batalla contra zapdos!!\n";
                        arcticuno.printInfo();
                        cout<< endl<<"Tecla para continuar."<<endl;
    cin >> tecla;
                        zapdos.printInfo();
                        cout<< endl<<"Tecla para continuar."<<endl;
    cin >> tecla;
                        cout << "INICIA!\n"; 
                        zapdos.ATK1(arcticuno);
                        cout<< endl<<"Tecla para continuar."<<endl;
    cin >> tecla;
                        arcticuno.ATK1(zapdos);
                        cout<< endl<<"Tecla para continuar."<<endl;
    cin >> tecla;
                        zapdos.ATK2(arcticuno);
                        cout<< endl<<"Tecla para continuar."<<endl;
    cin >> tecla;
                        arcticuno.printInfo();
                        cout<< endl<<"Tecla para continuar."<<endl;
    cin >> tecla;
                        arcticuno.ATK2(zapdos);
                        cout<< endl<<"Tecla para continuar."<<endl;
    cin >> tecla;            
                        zapdos.printInfo();
                        cout<< endl<<"Tecla para continuar."<<endl;
    cin >> tecla;
                        zapdos.ATK4(arcticuno);
                        cout<< endl<<"Tecla para continuar."<<endl;
    cin >> tecla;
                        arcticuno.ATK4(zapdos);
                        cout<< endl<<"Tecla para continuar."<<endl;
    cin >> tecla;
                        cout << "zapdos ha Huido. Fin de la batalla\n";
                        arcticuno.print();
                        goto segundob;
                case 'C':
                    cout << "saliendo...\n";
                        goto salir;
            }
    
    case 'C':
                                segundoc:
            cout << "Que desea hacer con Zapdos?\nA. Mostrar informacion\nB. Batallar\nC. SALIR";
            cin >> eleccion;
            if(eleccion != 'A' && eleccion != 'B' && eleccion != 'C'){
            cout << " No eligio una opcion correcta! Seleccione A, B o C\n";
            goto inicio_menu;
            }
            switch (eleccion)
            {
                case 'A':
                        cout << "\n";
                        zapdos.printInfo();
                        cout << "\n";
                        goto segundoc;
    
                case 'B':
                        cout << "*******************Batalla contra Moltress!!**********\n";
                        zapdos.printInfo();
                        cout<< endl<<"Tecla para continuar."<<endl;
    cin >> tecla;
                        moltress.printInfo();
                        cout<< endl<<"Tecla para continuar."<<endl;
    cin >> tecla;
                        cout << "INICIA!\n"; 
                        moltress.ATK1(zapdos);
                        cout<< endl<<"Tecla para continuar."<<endl;
    cin >> tecla;
                        zapdos.ATK1(moltress);
                        cout<< endl<<"Tecla para continuar."<<endl;
    cin >> tecla;
                        moltress.ATK2(zapdos);
                        cout<< endl<<"Tecla para continuar."<<endl;
    cin >> tecla;
                        zapdos.printInfo();
                        cout<< endl<<"Tecla para continuar."<<endl;
    cin >> tecla;
                        zapdos.ATK2(moltress);
                        cout<< endl<<"Tecla para continuar."<<endl;
    cin >> tecla;            
                        moltress.printInfo();
                        cout<< endl<<"Tecla para continuar."<<endl;
    cin >> tecla;
                        moltress.ATK3(zapdos);
                        cout<< endl<<"Tecla para continuar."<<endl;
    cin >> tecla;
                        zapdos.ATK3(moltress);
                        cout<< endl<<"Tecla para continuar."<<endl;
    cin >> tecla;
                        cout << "moltress ha Huido. Fin de la batalla\n";
                        zapdos.print();
                        goto segundoc;
                case 'C':
                        cout << "saliendo...\n";
                        goto salir;

            }
    
    
       break;

default:
cout << "\nDebe elegir una opcion correcta. Saliendo... \n";
    break;
}




    




salir:
return 0;
}




