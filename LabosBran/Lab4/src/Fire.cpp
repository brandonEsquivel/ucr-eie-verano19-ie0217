
//INCLUDES

#include "../include/Fire.h"


//DEFINES
using namespace std;

Fire::Fire(){

}
Fire::~Fire(){
    //Do nothing
}

string Fire::Type(){
    return "Fire";
}

string Fire::weakVs(){

    return "Water";
}

string Fire::strongVs(){
    return "Electric";
}

bool Fire::isWeakVS(string vs){
    bool weak = false;
    if(vs==weakVs()){
        weak = true;
        return weak;
    }
    return weak;
}

bool Fire::isStrongVS(string vs){
    bool strong = false;
    if(vs==strongVs()){
        strong = true;
        return strong;
    }

    return strong;
}