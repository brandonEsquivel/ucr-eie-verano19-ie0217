
//INCLUDES

#include "../include/Moltress.h"


//DEFINES
using namespace std;


// Constructor
Moltress::Moltress(){
    this->setNAME("Moltress");
    this->setATK(65);
    this->setCALL("MOLLLD MOOOLTRESS");
    this->setDEF(25);
    this->setEXP(0);
    this->setsATK(140);
    this->setsDEF(90);
    this->setSPD(70);
    this->setType("Fire");
    this->setLevel(0);
}


Moltress::~Moltress(){
    //Do nothing
}


void Moltress::print(){
    this->printInfo();
}

void Moltress::ATK1(Pokemon &other){
    
    string nombreATK = "Flamas Volcanicas!";
    
    cout <<"\n"<< this->getNAME() << " ha usado " << nombreATK << " contra " << other.getNAME() << endl;

    cout << "  FIRE!          // \n ";
    cout << "                /*/ \n";
    cout << "               /**/___  \n";
    cout << "            ****/    FIRE! \n";
    cout << "             /**/ \n ";
    cout << "          /****/ \n";
    cout << " FIRE!!   /*/_ \n";
    cout << "          /*/\n";
    cout << "          |*/ \n ";
    cout << "          | / \n";
    cout << "           *  *  !!!!!!\n";
    cout << "    FIRE!!!! \n";
    string tipo = other.getType();

    if(Fire::isStrongVS(tipo) == true){
        cout << "Es muy eficaz!" << endl;
        other.setHP((this->getATK()*1.40) - other.getDEF(),'m'); 
    }
    else if(Fire::isStrongVS(tipo) == true){
        cout << "NO es muy eficaz... " << endl;
        other.setHP((this->getATK()*0.70) - other.getDEF(),'m');

    }
    else{
        other.setHP(this->getATK() - other.getDEF(),'m');

    }

   cout << "fin de ataque" << endl;
}


void Moltress::ATK2(Pokemon &other){
string nombreATK = "lANZALLAMAS !";
    
    cout <<"\n"<< this->getNAME() << " ha usado " << nombreATK << " contra " << other.getNAME() << endl;

    cout << "      /****/ \n ";
    cout << "     /****/          FIREEEE!                  /\n     "; 
    cout << "    /****/____________________________________/ \n";
    cout << "    ****************************************** \n";
    cout << "          ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ";


    string tipo = other.getType();

    if(Fire::isStrongVS(tipo) == true){
        cout << "Es muy eficaz!" << endl;
        other.setHP((this->getATK()*1.40) - other.getDEF(),'m'); 
    }
    else if(Fire::isStrongVS(tipo) == true){
        cout << "NO es muy eficaz... " << endl;
        other.setHP((this->getATK()*0.70) - other.getDEF(),'m');

    }
    else{
        other.setHP(this->getATK() - other.getDEF(),'m');

    }

   cout << "\nfin de ataque" << endl;
}


void Moltress::ATK3(Pokemon &other){
string nombreATK = "Furia de dragon!";
    cout <<"\n"<< this->getNAME() << " ATACAA!\n" << endl;
    cout <<"\n"<< this->getNAME() << " ha usado " << nombreATK << " contra " << other.getNAME() << endl;
    cout << "                       ueeeeeu..\n";
cout<<"                        ur d$$$$$$$$$$$$$$Nu\n ";
cout<<"                    d$$$  $$$$$$$$$$$$$$$$$$e.\n";
cout<<"                  uuuc        .^**$$$$$$$$$$$$$b \n";
cout<<"                z$$u...           /!?$$$$$$$$$$$$$N.\n";
cout<<"              .$P                    uR$$$$$$$$$$$$$b\n";
cout<<"             x$F                 **$b. ..R).$$$$$$$$$$\n";
cout<<"            J^                            $$$$$$$$$$$$.\n";
cout<<"           z$e                      ..      .**$$$$$$$$$\n";
cout<<"         :$P           .        .$$$$$b.    ..  .  .$$$$\n";
cout<<"         $$            L          ^*$$$$b    .      4$$$$L\n";
cout<<"         4$$            ^u    .e$$$$e..*$$$N.       @$$$$$\n";
cout<<"                $$E            d$$$$$$$$$$$$$$L .$$$$$  mu $$$$$$F\n";
cout<<"                $$&            $$$$$$$$$$$$$$$$N    .* * ?$$$$$$$N\n";
cout<<"                $$F            .$$$$$$$$$$$$$$$$$bec...z$ $$$$$$$$\n";
cout<<"                $$F             .$$$$$$$$$$$$$$$$$$$$$$$$ .$$$$E $\n";
cout<<"                $$                  ^"""""".       ^.*$$$& 9$$$$N\n";
cout<<"                k  u$                                   $$.  $$P r\n";
cout<<"                4$$$$L                                    $. eeeR\n";
cout<<"                 $$$$$k                                   .$e. .@\n";
cout<<"                 3$$$$$b                                   .$$$$\n";
cout<<"                  $$$$$$                                    3$$\n";
cout<<"                   $$$$$  dc                                4$F\n";
cout<<"                    RF** <$$                                J\n";
cout<<"                     .bue$$$LJ$$$Nc.                        .\n";
cout<<"                      ^$$$$$$$$$$$$$r\n";
cout<<"                        *$$$$$$$$$\n";
cout<<"                $. .$ $~$ $~$ ~$~  $  $    $ $ $~$ $. .$ $~$  $  ~$~\n";
cout<<"                $$ $$ $ $ $ $  $  $.$ $    $$  $ $ $$ $$ $.$ $.$  $\n";
cout<<"                $$$ $ $ $~k  $  $~$ $    $$  $ $ $$$ $ $ $~$  $\n";
cout<<"                $ $ $ $o$ $ $  $  $ $ $oo  $ $ $o$ $ $ $ $o$ $ $  $ \n";





    string tipo = other.getType();

    if(Fire::isStrongVS(tipo) == true){
        cout << "Es muy eficaz!" << endl;
        other.setHP((this->getATK()*1.40) - other.getDEF(),'m'); 
    }
    else if(Fire::isStrongVS(tipo) == true){
        cout << "NO es muy eficaz... " << endl;
        other.setHP((this->getATK()*0.70) - other.getDEF(),'m');

    }
    else{
        other.setHP(this->getATK() - other.getDEF(),'m');

    }

   cout << "\nfin de ataque" << endl;
}


void Moltress::ATK4(Pokemon &other){
string nombreATK = "Descanso!";


    
    cout <<"\n"<< this->getNAME() << " ha usado " << nombreATK << "\nSe ha Posado en un volcan!\nRestaurando salud... "<< endl;
    cout << "      ******* \n ";
    cout << "     **     ** \n";
    cout << "    **       ** \n";
    cout << "   **   FIRE   ** \n";
    cout << "  **           ** \n ";
    cout << "   **         ** \n";
    cout << "    **       ** \n";
    cout << "     ********* \n";

    cout << this->getNAME() << " ha resturado un poco de su salud\n";
    this->setHP(20,'p');
    cout << "\nfin de ataque" << endl;
}