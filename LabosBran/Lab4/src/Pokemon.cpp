
//INCLUDES

#include "../include/Pokemon.h"

//DEFINES
using namespace std;


int Pokemon::getATK(){
    return this->ATK;
}
void Pokemon::setATK(int atk){
    this->ATK = atk;
}


int Pokemon::getDEF(){
    return this->DEF;
}
void Pokemon::setDEF(int def){
    this->DEF = def;
}

int Pokemon::getHP(){
    return this->HP;
}
void Pokemon::setHP(int hp, char p){
    if(p=='p'){
         this->HP = (this->HP) + hp;
    }
    else{
        this->HP = (this->HP) - hp;
    }

}

int Pokemon::getsATK(){
    return this->sATK;
}
void Pokemon::setsATK(int Satk){
    this->sATK = Satk;
}

int Pokemon::getsDEF(){
    return this->sDEF;
}
void Pokemon::setsDEF(int sdef){
    this->sDEF = sdef;
}


int Pokemon::getSPD(){
    return this->SPD;
}
void Pokemon::setSPD(int spd){
    this->SPD = spd;
}

string Pokemon::getCALL(){
    return this->call;
}
void Pokemon::setCALL(string grito){
    this->call = grito;
}

int Pokemon::getEXP(){
    return this->EXP;
}
void Pokemon::setEXP(int exp){
    this->EXP =+ exp;
}

string Pokemon::getNAME(){
    return this->name;
}
void Pokemon::setNAME(string nombre){
    this->name = nombre;
}

string Pokemon::getType(){
    return this->type;
}
void Pokemon::setType(string tipo){
    this->type = tipo;
}

void Pokemon::setLevel(int lv){
    this->LEVEL = lv;

}

void Pokemon::printInfo(){
    cout <<"\n\t***********INFO DEL POKEMON **************\n" << endl;
    cout << "Nombre: " << this->getNAME()              << "\t\tTipo: " << this->getType() << endl;
    cout << this->getCALL() << "!!!!"                << "\t\tSalud: " << this->getHP() << endl;
    cout << "Ataque: " << this->getATK()               << "\t\t\tDefensa: " << this->getDEF() << endl;
    cout << "Ataque Especial: " << this->getsATK()     << "\t\tDefensa Especial: " << this->getsDEF() << endl;
    cout << "Velocidad: " << this->getSPD()            << "\t\t\tNivel: "       << this->LEVEL << endl;
    cout <<"********************************************************************************************"<<endl;

}