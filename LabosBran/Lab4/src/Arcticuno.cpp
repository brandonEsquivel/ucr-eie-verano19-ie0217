
//INCLUDES

#include "../include/Arcticuno.h"



//DEFINES
using namespace std;


// Constructor
Arcticuno::Arcticuno(){
    this->setNAME("Arcticuno");
    this->setATK(45);
    this->setCALL("ARTICC ARCTICUUUN");
    this->setDEF(45);
    this->setEXP(0);
    this->setsATK(100);
    this->setsDEF(125);
    this->setSPD(40);
    this->setType("Water");
    this->setLevel(0);
}


Arcticuno::~Arcticuno(){
    //Do nothing
}


void Arcticuno::print(){
    this->printInfo();
}

void Arcticuno::ATK1(Pokemon &other){
    
    string nombreATK = " TSUNAMI !";
    
    cout <<"\n"<< this->getNAME() << " ha usado " << nombreATK << " contra " << other.getNAME() << endl;

    cout << " ¸,ø¤º°`°º¤ø,¸¸,ø¤º°¸,ø¤º°`°º¤ø,¸¸,ø¤º°¸,ø¤º°`°º¤ø,¸¸,ø¤º°¸,ø¤º°`°º¤ø,¸ \n ";
    cout << "  ¸  ,ø¤º°¸,ø¤º°`°º¤ø,¸¸,ø¤º°°º¤ø,¸¸,ø¤º°`°º¤ø,¸\n";
    cout << "                                ¸,ø¤º°¸,ø¤º°`°º¤ø,¸¸,ø¤º°°º¤ø,¸¸,ø¤º°`°º¤ø,¸            /**/___  \n";
    cout << " ¸,ø¤º°¸,ø¤º°`°º¤ø,¸¸,ø¤º°°º¤ø,¸¸,ø¤º°`°º¤ø,¸ \n";
    cout << "   ¸,ø¤º°¸,ø¤º°`°º¤ø,¸¸,ø¤º°°º¤ø,¸¸,ø¤º°`°º¤ø,¸¸,ø¤º°¸,ø¤º°`°º¤ø,¸¸,ø¤º°°º¤ø,¸¸,ø¤º°`°º¤ø,¸¸,ø¤º°¸,ø¤º°`°º¤ø,¸¸,ø¤º°°º¤ø,¸¸,ø¤º°`°º¤ø,¸  \n ";
    cout << "          /****/ \n";
    cout << " ¸,ø¤º°¸,ø¤º°`°º¤ø,¸¸,ø¤º°°º¤ø,¸¸,ø¤º°`°º¤ø,¸   /*/_                   TSUNAMIIII!! \n";

    string tipo = other.getType();

    if(Water::isStrongVS(tipo) == true){
        cout << "Es muy eficaz!" << endl;
        other.setHP((this->getATK()*1.40) - other.getDEF(),'m'); 
    }
    else if(Water::isStrongVS(tipo) == true){
        cout << "NO es muy eficaz... " << endl;
        other.setHP((this->getATK()*0.70) - other.getDEF(),'m');

    }
    else{
        other.setHP(this->getATK() - other.getDEF(),'m');

    }

   cout << "fin de ataque" << endl;
}


void Arcticuno::ATK2(Pokemon &other){
string nombreATK = "RAYO DE HIELO !";
    
    cout <<"\n"<< this->getNAME() << " ha usado " << nombreATK << " contra " << other.getNAME() << endl;

    cout << "      ____________________________________________________.....\n ";
      cout << "          ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~....... ";
    cout << "      °l||l°°l||l°°l||l°°l||l°°l||l°°l||l°°l||l°°l||l°°l||l°°l....... \n"; 
    cout << "       ,ø¤º°¸,ø¤º°`°º¤ø,¸¸,ø¤º°°º¤ø,¸¸,ø¤º°`°º¤ø,¸ø¤º°`°º¤ø,¸.... \n";
    cout << "             ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~.... ";
    cout << "       ___________________________________________________ ..\n"; 
  

    string tipo = other.getType();

    if(Water::isStrongVS(tipo) == true){
        cout << "Es muy eficaz!" << endl;
        other.setHP((this->getATK()*1.40) - other.getDEF(),'m'); 
    }
    else if(Water::isStrongVS(tipo) == true){
        cout << "NO es muy eficaz... " << endl;
        other.setHP((this->getATK()*0.70) - other.getDEF(),'m');

    }
    else{
        other.setHP(this->getATK() - other.getDEF(),'m');

    }

   cout << "\nfin de ataque" << endl;
}


void Arcticuno::ATK3(Pokemon &other){
string nombreATK = "Dragon de Nieve!";
    cout <<"\n"<< this->getNAME() << " ATACAA!\n" << endl;
    cout <<"\n"<< this->getNAME() << " ha usado " << nombreATK << " contra " << other.getNAME() << endl;
    cout << "                       ueeeeeu..\n";
cout<<"                        ur d$$$$$$$$$$$$$$Nu\n ";
cout<<"                    d$$$  $$$$$$$$$$$$$$$$$$e.\n";
cout<<"                  uuuc        .^**$$$$$$$$$$$$$b \n";
cout<<"                z$$u...           /!?$$$$$$$$$$$$$N.\n";
cout<<"              .$P                    uR$$$$$$$$$$$$$b\n";
cout<<"             x$F                 **$b. ..R).$$$$$$$$$$\n";
cout<<"            J^                            $$$$$$$$$$$$.\n";
cout<<"           z$e                      ..      .**$$$$$$$$$\n";
cout<<"         :$P           .        .$$$$$b.    ..  .  .$$$$\n";
cout<<"         $$            L          ^*$$$$b    .      4$$$$L\n";
cout<<"         4$$            ^u    .e$$$$e..*$$$N.       @$$$$$\n";
cout<<"                $$E            d$$$$$$$$$$$$$$L .$$$$$  mu $$$$$$F\n";
cout<<"                $$&            $$$$$$$$$$$$$$$$N    .* * ?_)-,.-~*¨¯¨*·~-.¸_)-,.-~*¨¯¨*·~-.¸_)-,.-~*¨¯¨*·~-.¸_)$$N\n";
cout<<"                $$F            .$$$$$$$$$$$$$$$$$bec...z$_)-,.-~*¨¯¨*·~-.¸_)-,.-~*¨¯¨*·~-.¸_)-,.-~*¨¯¨*$$$$$$\n";
cout<<"                $$F             .$$$$$$$$$$$$$$$$$$$$$$$$ _)-,.-~*¨¯¨*·~-.¸_)-,.-~*¨¯¨*·~-.¸_)-,.-~*¨¯¨*·~-.¸.$$$$E $\n";
cout<<"                $$                  ^"""""".       ^.*$$$& 9$$$$N\n";
cout<<"                k  u$                                   $$.  $$P r\n";
cout<<"                4$$$$L                                    $. eeeR\n";
cout<<"                 $$$$$k                                   .$e. .@\n";
cout<<"                 3$$$$$b                                   .$$$$\n";
cout<<"                  $$$$$$                                    3$$\n";
cout<<"                   $$$$$  dc                                4$F\n";
cout<<"                    RF** <$$                                J\n";
cout<<"                     .bue$$$LJ$$$Nc.                        .\n";
cout<<"                      ^$$$$$$$$$$$$$r\n";
cout<<"                        *$$$$$$$$$\n";
cout<<"                _)-,.-~*¨¯¨*·~-.¸_)-,.-~*¨¯¨*·~-.¸_)-,.-~*¨¯¨*·~-.¸_)-,.-~*¨¯¨*·~-.¸_)-,.-~*¨¯¨*·~-.¸_)-,.-~*¨¯¨*·~-.¸_)-,.-~*¨¯¨*·~-.¸~\n";
cout<<"                _)-,.-~*¨¯¨*·~-.¸_)-,.-~*¨¯¨*·~-.¸_)-,.-~*¨¯¨*·~-.¸_)-,.-~*¨¯¨*·~-.¸_)-,.-~*¨¯¨*·~-.¸_)-,.-~*¨¯¨*·~-.¸_)-,.-~*¨¯¨*·~-.¸ $\n";






    string tipo = other.getType();

    if(Water::isStrongVS(tipo) == true){
        cout << "Es muy eficaz!" << endl;
        other.setHP((this->getATK()*1.40) - other.getDEF(),'m'); 
    }
    else if(Water::isStrongVS(tipo) == true){
        cout << "NO es muy eficaz... " << endl;
        other.setHP((this->getATK()*0.70) - other.getDEF(),'m');

    }
    else{
        other.setHP(this->getATK() - other.getDEF(),'m');

    }

   cout << "\nfin de ataque" << endl;
}


void Arcticuno::ATK4(Pokemon &other){
string nombreATK = "Conjelamiento!";


    
    cout <<"\n"<< this->getNAME() << " ha usado " << nombreATK << "\nSe ha Conjelado!\nRestaurando salud... "<< endl;
    cout << "      ******* \n ";
    cout << "     **     ** \n";
    cout << "    **       ** \n";
    cout << "   **   ICE   ** \n";
    cout << "  **           ** \n ";
    cout << "   **         ** \n";
    cout << "    **       ** \n";
    cout << "     ********* \n";

    cout << this->getNAME() << " ha resturado un poco de su salud\n";
    this->setHP(20,'p');
    cout << "\nfin de ataque" << endl;
}