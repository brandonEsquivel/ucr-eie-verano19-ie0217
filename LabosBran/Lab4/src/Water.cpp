
//INCLUDES

#include "../include/Water.h"


//DEFINES
using namespace std;

Water::Water(){

}
Water::~Water(){
    //Do nothing
}

string Water::Type(){
    return "Water";
}

string Water::weakVs(){

    return "Electric";
}

string Water::strongVs(){
    return "Fire";
}

bool Water::isWeakVS(string vs){
    bool weak = false;
    if(vs==weakVs()){
        weak = true;
        return weak;
    }
    return weak;
}

bool Water::isStrongVS(string vs){
    bool strong = false;
    if(vs==strongVs()){
        strong = true;
        return strong;
    }

    return strong;
}