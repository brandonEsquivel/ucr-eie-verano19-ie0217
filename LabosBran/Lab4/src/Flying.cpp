
//INCLUDES

#include "../include/Flying.h"


//DEFINES
using namespace std;

Flying::Flying(){
}

Flying::~Flying(){
    //Do nothing
}

string Flying::Type(){
    return "Flying";
}

string Flying::weakVs(){

    return "Electric";
}

string Flying::strongVs(){
    return "Bug";
}

bool Flying::isWeakVS(string vs){
    bool weak = false;
    if(vs==weakVs()){
        weak = true;
        return weak;
    }
    return weak;
}

bool Flying::isStrongVS(string vs){
    bool strong = false;
    if(vs==strongVs()){
        strong = true;
        return strong;
    }

    return strong;
}