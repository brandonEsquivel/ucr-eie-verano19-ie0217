
//INCLUDES

#include "../include/Zapdos.h"


//DEFINES
using namespace std;


// Constructor
Zapdos::Zapdos(){
    this->setNAME("Zapdos");
    this->setATK(50);
    this->setCALL("ZAAAPDAAAS");
    this->setDEF(30);
    this->setEXP(1);
    this->setsATK(120);
    this->setsDEF(100);
    this->setSPD(50);
    this->setType("Electric");
    this->setLevel(0);
}


Zapdos::~Zapdos(){
    //Do nothing
}


void Zapdos::print(){
    this->printInfo();
}

void Zapdos::ATK1(Pokemon &other){
    
    string nombreATK = "Trueno Destellante!";
    
    cout <<"\n"<< this->getNAME() << " ha usado " << nombreATK << " contra " << other.getNAME() << endl;

    cout << "      /****/ \n ";
    cout << "    /****/ \n";
    cout << "    /****/___  \n";
    cout << "    ********/ \n";
    cout << "      /****/ \n ";
    cout << "    /****/ \n";
    cout << "    /__  /_ \n";
    cout << "      /***/\n";
    cout << "     |**/ \n ";
    cout << " *  | / \n";
    cout << "  *  *  !!!!!!\n";
    cout << "    ******\n";
    string tipo = other.getType();

    if(Electric::isStrongVS(tipo) == true){
        cout << "Es muy eficaz!" << endl;
        other.setHP((this->getATK()*1.40) - other.getDEF(),'m'); 
    }
    else if(Electric::isWeakVS(tipo)== true){
        cout << "NO es muy eficaz... " << endl;
        other.setHP((this->getATK()*0.70) - other.getDEF(),'m');

    }
    else{
        other.setHP(this->getATK() - other.getDEF(),'m');

    }

   cout << "fin de ataque" << endl;
}


void Zapdos::ATK2(Pokemon &other){
string nombreATK = "Tornado de Centellas!";
    
    cout <<"\n"<< this->getNAME() << " ha usado " << nombreATK << " contra " << other.getNAME() << endl;

    cout << "      /****/ \n ";
    cout << "     /****/ \n";
    cout << "    /****/___  \n";
    cout << "    ********/ \n";
    cout << "      /****/ \n ";
    cout << "     /****/ \n";
    cout << "    /__  /_ \n";
    cout << "      /***/\n";
    cout << "      |**/ \n ";
    cout << "      | / \n";
    cout << "   *  |/ *  !!!!!!\n";
    cout << "    ******\n";

    string tipo = other.getType();

    if(Electric::isStrongVS(tipo) == true){
        cout << "Es muy eficaz!" << endl;
        other.setHP((this->getATK()*1.40) - other.getDEF(),'m'); 
    }
    else if(Electric::isWeakVS(tipo)== true){
        cout << "NO es muy eficaz... " << endl;
        other.setHP((this->getATK()*0.70) - other.getDEF(),'m');

    }
    else{
        other.setHP(this->getATK() - other.getDEF(),'m');

    }

   cout << "\nfin de ataque" << endl;
}


void Zapdos::ATK3(Pokemon &other){
string nombreATK = "Sobrecarga Electromagnetica!";
    cout <<"\n"<< this->getNAME() << " ATACAA!\n" << endl;
    cout <<"\n"<< this->getNAME() << " ha usado " << nombreATK << " contra " << other.getNAME() << endl;
    cout << "      /****/ \n ";
    cout << "     /****/ \n";
    cout << "    /****/___  \n";
    cout << "    ********/ \n";
    cout << "      /****/ \n ";
    cout << "     /****/ \n";
    cout << "    /__  /_ \n";
    cout << "      /***/\n";
    cout << "      |**/ \n ";
    cout << "      | / \n";
    cout << "   *  |/ *  !!!!!!\n";
    cout << "    ******\n";





    string tipo = other.getType();

    if(Electric::isStrongVS(tipo) == true){
        cout << "Es muy eficaz!" << endl;
        other.setHP((this->getATK()*1.40) - other.getDEF(),'m'); 
    }
    else if(Electric::isWeakVS(tipo)== true){
        cout << "NO es muy eficaz... " << endl;
        other.setHP((this->getATK()*0.70) - other.getDEF(),'m');

    }
    else{
        other.setHP(this->getATK() - other.getDEF(),'m');

    }

   cout << "\nfin de ataque" << endl;
}


void Zapdos::ATK4(Pokemon &other){
string nombreATK = "Recarga!";


    
    cout <<"\n"<< this->getNAME() << " ha usado " << nombreATK << "\nSe ha acercado a una fuente de corriente alterna!\nRestaurando salud... "<< endl;
    cout << "      ******* \n ";
    cout << "     **     ** \n";
    cout << "    **       ** \n";
    cout << "   **   A C   ** \n";
    cout << "  **           ** \n ";
    cout << "   **         ** \n";
    cout << "    **       ** \n";
    cout << "     ********* \n";

    cout << this->getNAME() << " ha resturado un poco de su salud\n";
    this->setHP(20,'p');
    cout << "\nfin de ataque" << endl;
}