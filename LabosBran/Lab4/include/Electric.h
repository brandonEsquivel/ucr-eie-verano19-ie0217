#pragma once

//INCLUDES

#include "Pokemon.h"

//DEFINES
using namespace std;



/** @brief Implementacion/definicion de la clase tipo de pokemon Electric, hereda virtual de la clase Pokemon hereda a la clases concretas    
    @author Brandon Esquivel
    @date January 2020 
    */
class Electric: virtual public Pokemon{

    public:
        Electric();
        ~Electric();

        /** Get type name
            @return type name */
        static string Type();

        /** metodo static get de las debilidades del pokemon
            @return type weakness*/
        static string weakVs(); 

        /** metodo static get de las fortalezas del pokemon
            @return type strengths */
        static string strongVs();


        /** revisa si es debil contra otro pokemon  
            @param typeVS - string tipo de otro Pokemon
            @return debil o no
            */
       static bool isWeakVS(string typeVS);

        /** Revisa si el tipo es fuerte contra otro tipo  
            @param typeVS - string tipo de otro Pokemon
            @return Fuerte o no (bool)
            */
        static bool isStrongVS(string typeVS); 
       
        
        




};