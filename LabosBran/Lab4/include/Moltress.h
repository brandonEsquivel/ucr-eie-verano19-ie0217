#pragma once

//INCLUDES

#include "Fire.h"
#include "Flying.h"


//DEFINES
using namespace std;



/** @brief Implementacion/definicion de la clase Moltress, hereda virtual de la clase Pokemon y Flying. Hereda a la clases concretas    
    @author Brandon Esquivel
    @date January 2020 
    */
class Moltress : public Fire, virtual public Flying{

    private:
        string strongVs;/**<Type strengths*/
        string weakVs;/**<Type weaknesses*/

    public:

        /** Default constructor. 
         * @brief construye el objeto pokemon */
        Moltress();
        
       /** Default destructor 
        @brief Destruye el objeto pokemon */
         ~Moltress();

        /**Imprime informacion actual del pokemon*/
        void print();

        /** Metodos virtuales de ataques - Ataquee 1
         * @brief metodos virtuales de ataques - Ataquee 1*/
        void ATK1(Pokemon &other);

        /** Metodos virtuales de ataques - Ataque 2
         * @brief metodos virtuales de ataques - Ataque 2*/
        void ATK2(Pokemon &other);

        /** Metodos virtuales de ataques - Ataquee 1
         * @brief metodos virtuales de ataques - Ataquee 1*/
        void ATK3(Pokemon &other);

        /** Metodos virtuales de ataques - Ataquee 1
         * @brief metodos virtuales de ataques - Ataquee 1*/
        void ATK4(Pokemon &other);



};