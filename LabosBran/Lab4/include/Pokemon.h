#pragma once

//INCLUDES

#include "header.h"

//DEFINES
using namespace std;

/** @brief Implementacion/definicion de la clase abstracta base Pokemon que se encarga de crear los elementos generales para herencia a clases concretas
    @author Brandon Esquivel
    @date January 2020
    */
class Pokemon{
    private:
        string name;/**Nombre del pokemon a instanciar*/
        string call;/**Sonido, grito del pokemon!*/
        string type;/**Tipo del pokemon*/
        int HP=200;/**valor de puntos de vida del pokemon*/
        int ATK;/**valor de ataque del pokemon*/
        int DEF;/**valor de defensa del pokemon*/
        int sATK;/**valor de ataque especial del pokemon*/
        int sDEF;/**valor de defencia especial del pokemon*/
        int SPD;/**valor de velocidad del pokemon*/
        int EXP=0;/**valor de puntos de experiencia del pokemon*/
        int LEVEL=1;/**Valor del nivel actual del pokemon*/
    public:

        /** Metodos virtuales de ataques - Ataquee 1
         * @brief metodos virtuales de ataques - Ataquee 1*/
        virtual void ATK1(Pokemon &other) = 0;

        /** Metodos virtuales de ataques - Ataquee 2
         * @brief metodos virtuales de ataques - Ataquee 2*/
        virtual void ATK2(Pokemon &other) = 0;

        /** Metodos virtuales de ataques - Ataquee 3*
         * @brief metodos virtuales de ataques - Ataquee 3*/
        virtual void ATK3(Pokemon &other) = 0;

        /** Metodos virtuales de ataques - Ataquee 4
         * @brief metodos virtuales de ataques - Ataquee 4*/
        virtual void ATK4(Pokemon &other) = 0;

        /** Metodo get del parametro type
         * @brief Metodo get del parametro Type
        * @return type - nombre del pokemon*/
        string getType();

        /** Metodo set del parametro type
         * @brief Metodo set del parametro Type
        * @return type - nombre del pokemon*/
        void setType(string tipo);

        /** Metodo get del parametro name
         * @brief Metodo get del parametro name
        * @return name - nombre del pokemon*/
        string getNAME();

        /** Metodo set del parametro name
         * @brief Metodo set del parametro name
        * @param name - nombre del pokemon*/
        void setNAME(string name);


        /** Metodo get del parametro HP
         * @brief Metodo get del parametro HP
        * @return HP - Puntos de vida actuales del pokemon*/
        int getHP();

        /** Metodo get del parametro HP
         * @brief Metodo get del parametro ATK
        *  @param HP - Puntos de salud del pokemon
        *  @param p - si se indica p, se esta restaurando la salud, si se induca cualquier otro caracter, se resta value
        */
        void setHP(int value, char p);


        /** Metodo get del parametro ATK
         * @brief Metodo get del parametro ATK
        * @return ATK - Puntos de ataque del pokemon*/
        int getATK();

        /** Metodo set del parametro ATK
         * @brief Metodo set del parametro ATK
        * @param ATK - Puntos de ataque del pokemon*/
        void setATK(int atk);


        /** Metodo set del parametro DEF 
         * @brief Metodo get del parametro DEF
        * @return DEF - Puntos de defensa del pokemon*/
        int getDEF();

        /** Metodo set del parametro DEF 
         * @brief Metodo set del parametro DEF
        * @param DEF - Puntos de defensa del pokemon*/
        void setDEF(int def);


        /** Metodo get del parametro sATK
         * @brief Metodo get del parametro sATK
        * @return sATK - Puntos de ataque especial del pokemon*/
        int getsATK();

        /** Metodo set del parametro sATK
         * @brief Metodo set del parametro sATK
        * @param sATK - Puntos de ataque especial del pokemon*/
        void setsATK(int satk);


        /** Metodo get del parametro sDEF
         * @brief Metodo get del parametro sDEF
        * @return sDEF - Puntos de Defensa especial del pokemon*/
        int getsDEF();

        /** Metodo set del parametro sDEF
         * @brief Metodo set del parametro sDEF
        * @param sDEF - Puntos de Defensa especial del pokemon*/
        void setsDEF(int sdef);


        /** Metodo get del parametro SPD
         * @brief Metodo get del parametro SPD
        * @return SPD - Puntos de velocidad del pokemon*/
        int getSPD();

        /** Metodo set del parametro SPD
         * @brief Metodo set del parametro SPD
        * @param SPD - Puntos de velocidad del pokemon*/
        void setSPD(int spd);


        /** Metodo get del parametro CALL
         * @brief Metodo get del parametro CALL
        * @return call - Grito o sonido del pokemon*/
        string getCALL();

        /** Metodo set del parametro CALL
         * @brief Metodo set del parametro CALL
        * @param call - Grito o sonido del pokemon*/
        void setCALL(string grito);
        

        /** Metodo get del parametro EXP
         * @brief Metodo get del parametro EXP
        * @return EXP - Puntos de Experiencia del pokemon*/
        int getEXP();

        /** Metodo set del parametro EXP 
         * @brief Metodo set del parametro EXP
        * @param EXP - Puntos de Experiencia del pokemon*/
        void setEXP(int exp);

        /** Metodo set del parametro LEVEL 
         * @brief Metodo set del parametro Level
        * @param LEVEL - Nivel del pokemon*/
        void setLevel(int lv);

        /** Metodo de impresion de informacion del pokemon
         *  @brief Metodo de impresion de informacion del pokemon
         * @return info del pokemon impresa en consola*/
        void printInfo();

};
