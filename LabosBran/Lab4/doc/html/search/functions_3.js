var searchData=
[
  ['getatk',['getATK',['../classPokemon.html#ac31d559cd56c1455d5bd097b51042455',1,'Pokemon']]],
  ['getcall',['getCALL',['../classPokemon.html#a22c2a4f6272e44f932e5ebd65fe6d514',1,'Pokemon']]],
  ['getdef',['getDEF',['../classPokemon.html#a5efb172aa2761816205f1e1ed59640ff',1,'Pokemon']]],
  ['getexp',['getEXP',['../classPokemon.html#a53731fb189f7733208627872b5e4d56b',1,'Pokemon']]],
  ['gethp',['getHP',['../classPokemon.html#ad91531adc33486fee630bec3e5bfa388',1,'Pokemon']]],
  ['getname',['getNAME',['../classPokemon.html#a301c199291bfd26c8d0deb4aa1d3b7fe',1,'Pokemon']]],
  ['getsatk',['getsATK',['../classPokemon.html#a9eaec0bafdef0e1b943dba2e40ef9863',1,'Pokemon']]],
  ['getsdef',['getsDEF',['../classPokemon.html#ab564539f0dcc83af17c80bc4a20e41e7',1,'Pokemon']]],
  ['getspd',['getSPD',['../classPokemon.html#a583c6ee0d07c4c7d3cec0bfac03ddd87',1,'Pokemon']]],
  ['gettype',['getType',['../classPokemon.html#a3283976f82a48c37434a64118679e62c',1,'Pokemon']]]
];
