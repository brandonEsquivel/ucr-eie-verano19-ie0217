var searchData=
[
  ['setatk',['setATK',['../classPokemon.html#aa78f812952386a5741653e772188dbd0',1,'Pokemon']]],
  ['setcall',['setCALL',['../classPokemon.html#a9fe584f4fb977a1dd2dba1c99aee419b',1,'Pokemon']]],
  ['setdef',['setDEF',['../classPokemon.html#a9263ffabc19d67b54457b2d07ed8ea6d',1,'Pokemon']]],
  ['setexp',['setEXP',['../classPokemon.html#a6353c2f24ed461ee902d6bdd25824ad7',1,'Pokemon']]],
  ['sethp',['setHP',['../classPokemon.html#af1ecbd992b981ecb40934eb94b23a1ae',1,'Pokemon']]],
  ['setlevel',['setLevel',['../classPokemon.html#acde77e23620a3e51d4eb98f8eb8c187c',1,'Pokemon']]],
  ['setname',['setNAME',['../classPokemon.html#a431bfacf7fe66228b7533b193cde5622',1,'Pokemon']]],
  ['setsatk',['setsATK',['../classPokemon.html#af6894367b62c1d2a88b87d70f3c3df35',1,'Pokemon']]],
  ['setsdef',['setsDEF',['../classPokemon.html#a4804fa305353f3fd4ef82a9f0e8a0946',1,'Pokemon']]],
  ['setspd',['setSPD',['../classPokemon.html#aa44e190656e7cf1ab56a2b4b26c02cfe',1,'Pokemon']]],
  ['settype',['setType',['../classPokemon.html#ae326c94f1f13cf915c0a46e54c6e9ed8',1,'Pokemon']]],
  ['strongvs',['strongVs',['../classElectric.html#a11944de076f7c682d473ed3516e193df',1,'Electric::strongVs()'],['../classFire.html#a9ec157338fd4d4b97cabefe72a1ad2c8',1,'Fire::strongVs()'],['../classWater.html#a1d1557230f3c52cce78bfa1fd4768497',1,'Water::strongVs()']]]
];
