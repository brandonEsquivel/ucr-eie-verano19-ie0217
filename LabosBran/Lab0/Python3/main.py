# Se importa el modulo sys para el ingreso de datos en terminal
import sys

# Se define la cadena de entrada y se imprime 
cadena_entrada = sys.argv[1]    
print ("El codigo genetico de entrada es: ",cadena_entrada)

largo_cadena = len(cadena_entrada)  # Se obtiene largo de la cadena

prueba_multiplo = largo_cadena % 3  # Se obtiene el modulo de la cadena
num_grupos = largo_cadena // 3      # Se obtiene el numero de grupos

cadena_salida = []                  # Se define la cadena de salida

# Se verifica si la cadena es multiplo de tres
if (prueba_multiplo != 0):
    print("La cadena de entrada no es multiplo de 3")
    print("Ingrese cadena que sea multiplo de 3")

else:                               # En caso de que sea multiplo de tres
    # Se verifica si hay caracter invalidos en la cadena,
    # por medio de la bandera llamada codones_validos
    for i in range(0,largo_cadena):
        if (str(cadena_entrada[i]) in "GUCA"):
            codones_validos = True
        else:
            codones_validos = False
            break

    # En caso de que hayan caracter invalidos se imprime dicha informacion
    if (codones_validos == False):
        print("Hay codones invalidos en la cadena de entrada")
        print("Verifique los codones")

    else:                           # En caso de que los caracteres sean validos
        # Se va recorriendo y dividiendo en subgrupos la cadena
        for k in range(0, num_grupos):
            subgrupo = cadena_entrada[k*3:(k*3)+3]

            # Se procede con el analisis de cada subgrupo, tomando 
            # los dos primeros codones y a partir de ahí se revisa el tercero
            if (str(subgrupo[0:2]) == "UU"):
                if (str(subgrupo[2]) in "UC"):
                    cadena_salida.append("F")
                if(str(subgrupo[2]) in "AG"):
                     cadena_salida.append("L")
                     
            
            if (str(subgrupo[0:2]) == "UC"):
                    cadena_salida.append("S")


            if (str(subgrupo[0:2]) == "UA"):
                if (str(subgrupo[2]) in "UC"):
                    cadena_salida.append("Y")
                if(str(subgrupo[2]) in "AG"):
                     pass
                     
                     
            if (str(subgrupo[0:2]) == "UG"):
                if (str(subgrupo[2]) in "UC"):
                    cadena_salida.append("C")
                if(str(subgrupo[2]) == "G"):
                     cadena_salida.append("W")
                if(str(subgrupo[2]) == "A"):
                     pass

            if (str(subgrupo[0:2]) == "CU"):
                    cadena_salida.append("L")
                    
            if (str(subgrupo[0:2]) == "CC"):
                    cadena_salida.append("P")
                  
                     
            if (str(subgrupo[0:2]) == "CA"):
                if (str(subgrupo[2]) in "UC"):
                    cadena_salida.append("H")
                if(str(subgrupo[2]) in "AG"):
                     cadena_salida.append("Q")
                     


            if (str(subgrupo[0:2]) == "CG"):
                cadena_salida.append("R")
                     
                     
            if (str(subgrupo[0:2]) == "AU"):
                if (str(subgrupo[2]) in "UCA"):
                    cadena_salida.append("I")
                if(str(subgrupo[2]) == "G"):
                     cadena_salida.append("M")
                     
            
            if (str(subgrupo[0:2]) == "AC"):
                cadena_salida.append("T")
                
            
            if (str(subgrupo[0:2]) == "AA"):
                if (str(subgrupo[2]) in "UC"):
                    cadena_salida.append("N")
                if(str(subgrupo[2]) in "AG"):
                     cadena_salida.append("K")           


            if (str(subgrupo[0:2]) == "AG"):
                if (str(subgrupo[2]) in "UC"):
                    cadena_salida.append("S")
                if(str(subgrupo[2]) in "AG"):
                     cadena_salida.append("R") 
                     
            if (str(subgrupo[0:2]) == "GU"):
                cadena_salida.append("V")  
                

            if (str(subgrupo[0:2]) == "GC"):
                cadena_salida.append("A")  
                
                
            if (str(subgrupo[0:2]) == "GG"):
                cadena_salida.append("G") 
                
            if (str(subgrupo[0:2]) == "GA"):
                if (str(subgrupo[2]) in "UC"):
                    cadena_salida.append("D")
                if(str(subgrupo[2]) in "AG"):
                     cadena_salida.append("E") 
                     
        # Se imprime el aminoacido generado
        print ("El amonoacido es: ","".join(cadena_salida)) 