//INCLUDES 

#include "polinomio.h"

// IMPLEMENTACION DE METODOS


//DEFAULT CONSTRUCTOR
Polinomio::Polinomio(){
	this->a=0;
	this->b=0;
	this->c=0;
	this->d=0;
	this->e=0;

}
//CUSTOM CONSTRUCTOR
Polinomio::Polinomio(float a,float b, float c){
	this->a=a;
	this->b=b;
	this->c=c;
	this->d=0;
	this->e=0;
}

//SOBRECARGA DEL OPERADOR + PARA POLINOMIOS DE GRADO DOS
Polinomio Polinomio::operator + (Polinomio const &obj){
	Polinomio result;
	result.a=(this->a + obj.a);
	result.b=(this->b + obj.b);
	result.c=(this->c + obj.c);
	result.d=0;
	result.e=0;
	return result;
}

//SOBRECARGA DEL OPERADOR - PARA POLINOMIOS DE GRADO DOS
Polinomio Polinomio::operator - (Polinomio const &obj){
	Polinomio result;
	result.a=(this->a - obj.a);
	result.b=(this->b - obj.b);
	result.c=(this->c - obj.c);
	result.d=0;
	result.e=0;
	return result;
}

//SOBRECARGA DEL OPERADOR * PARA POLINOMIO DE GRADO DOS RESULTANDO GRADO CUATRO
Polinomio Polinomio::operator * (Polinomio const &obj){
	Polinomio result;
	result.e=this->a *obj.a;
	result.d=this->a*obj.b+this->b*obj.a;
	result.a=this->a*obj.c + this->b*obj.b + this->c*obj.a;
	result.b=this->b*obj.c + this->c*obj.b;
	result.c=this->c*obj.c;

	return result;
}


Polinomio Polinomio::operator / (Polinomio const &obj){
	Polinomio result;
	float d[3]={this->a,this->b,this->c};
	float di[3]={obj.a,obj.b,obj.c};
	float r[3]={0,0,0};

	if(this->a==0 && obj.a!=0){
	result.a=0.0000001;
	result.b=0.0000001;
	result.c=0.0000001;
	result.d=0.0000001;
	result.e=0.0000001;
	}	//NO SE PUEDE HACER DIVISION
	
	else{//Se puede hacer la division
	
	if(obj.a!=0){//Si se ingresa divisor de grado 2
		c=d[0]/di[0];
		result.c=c;
		for(int j=0;j<3;j++){
			r[j]=c*di[j];
			r[j]=d[j]-r[j];
		}
		//result.b=0;
		//result.e=0;
		//result.d=0;
		//result.a=0;
	}
	else{
		c=d[0]/di[1];
		result.b=c;
		for(int j=0;j<3;j++){
			r[j]=c*di[j];
			r[j]=d[j]-r[j];
		}
		c=r[1]/di[1];//
		result.c=c;
		for(int j=1;j<3;j++){
			r[j]=c*di[j];
			r[j]=d[j]-r[j];
		}

		//result.b=0;
		//result.e=0;
		//result.d=0;
		//result.a=0;

	}
	}
	return result;
}

//Polinomio::operator float() const{
//	return this->num;
//}



ostream& operator <<(ostream& os, Polinomio const &obj){
	string x[5]={"x⁴","x³","x²","x",""};
	float res[5]={obj.e,obj.d,obj.a,obj.b,obj.c};	//Division
	int bandera=0;//Para saber cual x es la primera.
	for(int j=0;j<5;j++){

		if(res[j]!=0){//Si coeficiente es 0 no lo imprima.
			
			if(res[j]<0){
				os <<res[j]<<x[j];
			}else{
				
				if(bandera==0){
					os<<res[j]<<x[j];
					bandera=1;
				
				}
				else{
					os <<" + "<< res[j]<<x[j];
					
				
				}
			}
			
		}
	}
	os<<endl;
	return os;	
}



istream& operator >> (istream& in, Polinomio &obj){
	cout<<"Ingrese a: ";
	in>>obj.a;
	cout<<"Ingrese b: ";
	in>>obj.b;
	cout<<"Ingrese c: ";
	in>>obj.c;
	return in;	
}

