#include <iostream>
#include "fraccion.h"

#include "polinomio.h"

using namespace std;

int main(){
	int c=0;
	bool salir =false;
	while(salir!=true){
		cout<<endl<<"***********"<<endl;
		cout<<" Opcion:"<<endl;///
		cout<<"1 Fracción"<<endl;
		cout<<"2 Polinomio"<<endl;
		cout<<"3 Salir"<<endl;
		cin >>c;
	switch(c){
		case 1:
			{
			Fraccion f1;
			cin >>f1; 
			cout<<endl<<" La fraccion 1 que ingresó es: "<<f1<<endl;

			Fraccion f2;
			cin >>f2; 

			cout<<endl<<" La fraccion 2 que ingresó es: "<<f2<<endl;
	
			Fraccion f3=f1 + f2;
			Fraccion f4=f1 - f2;

			Fraccion f5=f1 * f2;
			Fraccion f6=f1 / f2;
	
			cout<<endl<<"La suma es:"<<endl;
			cout<<f3<<endl;
			cout<<endl<<"La resta es: "<<endl;
			cout<<f4<<endl;
	
			cout<<endl<<"La multiplicación: "<<endl;
			cout<<f5<<endl;
			cout<<endl<<"La division:"<<endl;
			cout<<f6<<endl;
			}
	
			break;
		case 2:
	
			{


			cout<<endl<<" ********Polinomio*******"<<endl;	
			Polinomio p1;
			cin >>p1; 
			cout<<endl<<" El polinomio 1 que ingresó es: "<<p1<<endl;
	
			Polinomio p2;
			cin >>p2; 
			cout<<endl<<" El polinomio 2 que ingresó es: "<<p2<<endl;	
			Polinomio p3=p1 + p2;
			Polinomio p4=p1 - p2;

			Polinomio p5=p1 * p2;
			Polinomio p6=p1 / p2;
	
			cout<<endl<<"La suma es:"<<endl;
			cout<<p3<<endl;
			cout<<endl<<"La resta es: "<<endl;
			cout<<p4<<endl;
	
			cout<<endl<<"La multiplicación: "<<endl;
			cout<<p5<<endl;
			cout<<endl<<"La division:"<<endl;
			cout<<p6<<endl;
			}
			break;
		case 3: 
			salir =true;
	}
	}
	return 0;
}//
