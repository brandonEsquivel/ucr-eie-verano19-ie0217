#include "fraccion.h"
Fraccion::Fraccion(){
	this->num=0;
	this->den=1;
}

Fraccion::Fraccion(float num,float den){
	this->num=num;
	this->den=den;
}

Fraccion Fraccion::operator + (Fraccion const &obj){
	Fraccion result;
	result.num=(this->num * obj.den) + (this->den * obj.num);
	result.den=(this->den * obj.den);
	
	return result;
}

Fraccion Fraccion::operator - (Fraccion const &obj){
	Fraccion result;
	result.num=(this->num * obj.den) - (this->den * obj.num);
	result.den=(this->den * obj.den);
	
	return result;
}


Fraccion Fraccion::operator * (Fraccion const &obj){
	Fraccion result;
	result.num=(this->num * obj.num);
	result.den=(this->den * obj.den);
	
	return result;
}


Fraccion Fraccion::operator / (Fraccion const &obj){
	Fraccion result;
	result.num=(this->num / obj.num);
	result.den=(this->den / obj.den);
	
	return result;
}

Fraccion::operator float() const{
	return this->num;
}

ostream& operator <<(ostream& os, Fraccion const &obj){
	os << obj.num <<"/"<<obj.den;
	return os;	
}

istream& operator >> (istream& in, Fraccion &obj){
	cout<<"Ingrese numerador: ";
	in>>obj.num;
	cout<<"Ingrese denominador: ";
	in>>obj.den;

	return in;	
}

