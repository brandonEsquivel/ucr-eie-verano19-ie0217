#include <iostream>
using namespace std;

class Polinomio{
	private:
/* tres constantes para ax²+bx+c y las otras para polinomio de grado 4*/
		float a;
		float b;
		float c;
		float d;
		float e;
	public:
		Polinomio();

/*Costructor, lo inicializa el usuario
 * @param a: coeficiente  que acompaña a la x²
 * @param b: coeficiente  que acompaña a la x
 * @param c: coeficiente solitario
 */Polinomio(float a, float b, float c );

/*Realiza la suma de dos fracciones, meadiante el operador +
 * @param Fraccion const @obj: resive la referencia de un objeto de tipo Fraccion
 * @return result: retorna un objeto de tipo fraccion que contiene el resultado de la suma.
 */Polinomio operator + (Polinomio const &obj);

/*Realiza la resta  de dos fracciones, meadiante el operador -
 * @param Fraccion const @obj: resive la referencia de un objeto de tipo Fraccion
 * @return result: retorna un objeto de tipo fraccion que contiene el resultado de la resta.
 */Polinomio operator - (Polinomio const &obj);
		
/*Realiza la multiplicación de dos fracciones, meadiante el operador *
 * @param Fraccion const @obj: resive la referencia de un objeto de tipo Fraccion
 * @return result: retorna un objeto de tipo fraccion que contiene el resultado de la multiplicación.
 */Polinomio operator * (Polinomio const &obj);

/*Realiza la divición de dos fracciones, meadiante el operador /
 * @param Fraccion const @obj: resive la referencia de un objeto de tipo Fraccion
 * @return result: retorna un objeto de tipo fraccion que contiene el resultado de la divición.
 */Polinomio operator / (Polinomio const &obj);

		//operator float() const;

/*Función que sobrecarga operador de salida o de imprimir
 */ friend ostream& operator <<(ostream& os, Polinomio const &obj);

/*Función que sobrecarga operador de entrada 
 */ friend istream& operator >> (istream& in, Polinomio  &obj);
};
