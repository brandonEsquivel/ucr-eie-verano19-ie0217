#include <iostream>
using namespace std;

class Fraccion{
	private:
		float num;
		float den;
	public:
		Fraccion();
/*Costructor, lo inicializa el usuario
 * pasandole los datos de los atributos
 */Fraccion(float num, float den);

/*Realiza la suma de dos fracciones, meadiante el operador +
 * @param Fraccion const @obj: resive la referencia de un objeto de tipo Fraccion
 * @return result: retorna un objeto de tipo fraccion que contiene el resultado de la suma.
 */Fraccion operator + (Fraccion const &obj);

/*Realiza la resta  de dos fracciones, meadiante el operador -
 * @param Fraccion const @obj: resive la referencia de un objeto de tipo Fraccion
 * @return result: retorna un objeto de tipo fraccion que contiene el resultado de la resta.
 */Fraccion operator - (Fraccion const &obj);

/*Realiza la multiplicación de dos fracciones, meadiante el operador *
 * @param Fraccion const @obj: resive la referencia de un objeto de tipo Fraccion
 * @return result: retorna un objeto de tipo fraccion que contiene el resultado de la multiplicación.
 */Fraccion operator * (Fraccion const &obj);

/*Realiza la divición de dos fracciones, meadiante el operador /
 * @param Fraccion const @obj: resive la referencia de un objeto de tipo Fraccion
 * @return result: retorna un objeto de tipo fraccion que contiene el resultado de la divición.
 */Fraccion operator / (Fraccion const &obj);

		operator float() const;

/*Función que sobrecarga operador de salida o de imprimir
 */ friend ostream& operator <<(ostream& os, Fraccion const &obj);

/*Función que sobrecarga operador de entrada 
 */ friend istream& operator >> (istream& in, Fraccion  &obj);
 
};
