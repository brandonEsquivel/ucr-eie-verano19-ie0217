var searchData=
[
  ['operator_20float',['operator float',['../classFraccion.html#ad947df0721f2f1502ef99cfac9993e2b',1,'Fraccion']]],
  ['operator_2a',['operator*',['../classFraccion.html#a6f05a00fc1d573b95957ece1d35978e2',1,'Fraccion::operator*()'],['../classPolinomio.html#a8de5de3e1bc5394886f5e5460f348c2c',1,'Polinomio::operator*()']]],
  ['operator_2b',['operator+',['../classFraccion.html#ab6b2dd0b2464b92e45d101ed99f7c965',1,'Fraccion::operator+()'],['../classPolinomio.html#adb1eeb2c5e5fdb5842b109f3d3b05574',1,'Polinomio::operator+()']]],
  ['operator_2d',['operator-',['../classFraccion.html#ad1f90db6df1ba8493ee98911c2a04a8b',1,'Fraccion::operator-()'],['../classPolinomio.html#a4c57cbda8b003eb339ccee7290118beb',1,'Polinomio::operator-()']]],
  ['operator_2f',['operator/',['../classFraccion.html#a901ef7e569d380379926ace5dc3296d7',1,'Fraccion::operator/()'],['../classPolinomio.html#a9daa7a9d0ad9e88fb91831413755003b',1,'Polinomio::operator/()']]]
];
