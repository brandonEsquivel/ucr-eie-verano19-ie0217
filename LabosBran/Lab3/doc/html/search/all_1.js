var searchData=
[
  ['operator_20float',['operator float',['../classFraccion.html#ad947df0721f2f1502ef99cfac9993e2b',1,'Fraccion']]],
  ['operator_2a',['operator*',['../classFraccion.html#a6f05a00fc1d573b95957ece1d35978e2',1,'Fraccion::operator*()'],['../classPolinomio.html#a8de5de3e1bc5394886f5e5460f348c2c',1,'Polinomio::operator*()']]],
  ['operator_2b',['operator+',['../classFraccion.html#ab6b2dd0b2464b92e45d101ed99f7c965',1,'Fraccion::operator+()'],['../classPolinomio.html#adb1eeb2c5e5fdb5842b109f3d3b05574',1,'Polinomio::operator+()']]],
  ['operator_2d',['operator-',['../classFraccion.html#ad1f90db6df1ba8493ee98911c2a04a8b',1,'Fraccion::operator-()'],['../classPolinomio.html#a4c57cbda8b003eb339ccee7290118beb',1,'Polinomio::operator-()']]],
  ['operator_2f',['operator/',['../classFraccion.html#a901ef7e569d380379926ace5dc3296d7',1,'Fraccion::operator/()'],['../classPolinomio.html#a9daa7a9d0ad9e88fb91831413755003b',1,'Polinomio::operator/()']]],
  ['operator_3c_3c',['operator&lt;&lt;',['../classFraccion.html#ac0c66a21cb203a456a6ae7ae55cc85d1',1,'Fraccion::operator&lt;&lt;()'],['../classPolinomio.html#af1cc0dbf6f17803a8274e75efb99e371',1,'Polinomio::operator&lt;&lt;()']]],
  ['operator_3e_3e',['operator&gt;&gt;',['../classFraccion.html#a80960de05aa544cef802eeaa448c15d6',1,'Fraccion::operator&gt;&gt;()'],['../classPolinomio.html#a9b042a6bc0e65d53f8b092f761281c9a',1,'Polinomio::operator&gt;&gt;()']]]
];
